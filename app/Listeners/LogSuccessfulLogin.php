<?php

namespace App\Listeners;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Login;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;
use Stevebauman\Location\Location;

class LogSuccessfulLogin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param Login $event
     * @param Request $request
     * @return void
     */
    public function handle(Login $event)
    {

        $event->user->last_login_at = Carbon::now()->toDateTimeString();
        $event->user->last_login_ip = request()->getClientIp();
        $event->user->save();
    }
}
