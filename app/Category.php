<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $table = 'categories';

    protected $id = 'user_id';

    protected $primaryKey = 'id';

    protected $fillable = [
        'name',
    ];

}
