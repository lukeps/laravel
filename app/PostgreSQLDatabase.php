<?php

namespace App;


use App\Interfaces\DatabaseInterface;
use Illuminate\Http\Request;

class PostgreSQLDatabase
{

    private $db;
    private $request;

    public function __construct(Request $request, DatabaseInterface $db)
    {

        $this->db = $db;
        $this->request = $request;

    }

    public function getDBData()
    {
        if ($this->request->server("SERVER_NAME") == "obscure-taiga-62467.herokuapp.com") {
            $result = $this->db->getDBSize($this->request);
            return response()->json(['dbSize' => $result]);
        }
    }
}

