<?php
namespace App\Interfaces;

use Illuminate\Http\Request;

interface DatabaseInterface{


    public function getDBSize(Request $request);

}
