<?php

namespace App;
use App\Interfaces\DatabaseInterface;
use Illuminate\Http\Request;

class MySQLDatabase
{

    private $db;
    private $request;

    public function __construct(Request $request,DatabaseInterface $db){

        $this->db = $db;
        $this->request = $request;

    }

    public function getDBData(){
        if($this->request->server('SERVER_NAME') == "laravel.test") {

            $result = $this->db->getDBSize($this->request);

            return response()->json(['dbSize' => $result]);
        }

    }
}
