<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class TotalViews extends Model
{


    protected $table = 'total_page_views';

    protected $primaryKey = 'id';

    protected $fillable = [
        "id", "user_id", "username","email","nationality","session_id","ip_address","created_at","updated_at"
    ];


}
