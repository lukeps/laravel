<?php

namespace App\Console;

use App\Console\Commands\UserActivityStatus;
use App\Traits\CronMessagesTrait;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{

    use CronMessagesTrait;

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \Laravel\Tinker\Console\TinkerCommand::class,
        UserActivityStatus::class,

    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('user:activity_status')->dailyAt('12:00')->appendOutputTo(storage_path().'/logs/test.log');
        $schedule->call(function(){
            $this->sendCronMessagesToCloudServer();
        })->dailyAt('12:05');
    }

    /**
     * Register the commands for the application.
     * @return void
     *
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
