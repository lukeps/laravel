<?php

namespace App\Console\Commands;

use App\Http\Controllers\UsersController;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class UserActivityStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:activity_status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'User activity status ';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    private $setUserActivity;

    public function __construct(UsersController $setUserActivity)
    {
        $this->setUserActivity = $setUserActivity;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixedN
     */
    public function handle()
    {
        $this->setUserActivity->setUserActivityStatus(); //calling a task for cron
        $this->line('User activity status was checked and set. ' . ' ' . Carbon::now());
    }
}
