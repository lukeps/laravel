<?php

namespace App\Exceptions;

use App\User;
use Exception;
use http\Env\Response;
use Illuminate\Http\Request;

class UserPermissionsException extends Exception
{

    public function render(Request $request, $id)
    {
        $input = $request->all();
        $user = User::findOrFail($id);

            if (isset($input["permissionsCheckbox"])) {

                if ($user->hasRole('admin') && $user->id !== 1) {

                    $user->removeRole('admin');
                    $user->assignRole('writer');

                }

                else if($user->hasRole('writer') && $user->id !==1){

                     throw new UserPermissionsException("Ten użytkownik nie posiada praw administratora!");
                }

                else {

                    throw new UserPermissionsException("Nie możesz odebrać praw administratora głównemu użytkownikowi !");
                }
            }

    }
}

