<?php

namespace App\Traits;

use Intervention\Image\ImageManagerStatic;


trait UploadAvatarTrait
{
    public function handleUploadedAvatar($image)
    {

        $imageToResize = ImageManagerStatic::make($image);

           return $imageToResize->resize(100,100)->encode($image->getClientOriginalExtension()); //get raw image file content

    }
}
