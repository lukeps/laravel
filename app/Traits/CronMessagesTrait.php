<?php

namespace App\Traits;


use Illuminate\Support\Facades\Storage;

trait CronMessagesTrait
{
    public function sendCronMessagesToCloudServer()
    {

        $filename = 'cron logs/logs';
        Storage::disk('s3')->put($filename,(file_get_contents(storage_path().'/logs/test.log')));

    }
}
