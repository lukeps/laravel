<?php

namespace App\Providers;

use App\Http\Controllers\MYSQLDataController;
use App\Interfaces\DatabaseInterface;
use App\MySQLDatabase;
use App\PostgreSQLDatabase;
use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\PostgreSQLDataController;

class DatabaseServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register() //bind created Interface to framework env
    {

        $this->app->when(MySQLDatabase::class) //when this class is invoked, it needs implementation of DatabaseInterface
            ->needs(DatabaseInterface::class) //which is MYSQLDataController, so return this instance, and get proper data from it
            ->give(function () {
                return new MYSQLDataController();
            });

        $this->app->when(PostgreSQLDatabase::class)
            ->needs(DatabaseInterface::class)
            ->give(function () {
                return new PostgreSQLDataController();
            });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
