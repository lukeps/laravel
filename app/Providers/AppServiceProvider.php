<?php

namespace App\Providers;
use App\Http\Controllers\NotificationsController;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;


class AppServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {


    }

    /**
     * Bootstrap any application services.
     *
     * @param Request $request
     * @param Closure $next
     * @return void
     */
    public function boot()
    {
        View::composer('GUI_Components.topbar', function ($view) {

            if (Auth::check()) {

                $user = Auth::user();

                $timeAgo = [];
                $notify = new NotificationsController();
                $greetingMessage = $notify->sendGreetingNotification(new Request());
                $notify->updateTimeOfReceivedNotification();
                $sumOfUnreadNotifications = DB::table('notifications')->where('notifiable_id', $user->id)->whereNull('read_at')->count();
                Auth::user()->notifications = $user->notifications()->get();

                foreach (Auth::user()->notifications as $notification) {
                    $timeAgo[] = $notify->calculateTimeSinceGotNotification($notification->time_ago);
                }

                return $view->with([
                    'greetingNotification' => $notification->data,
                    'sumOfUnreadNotifications' => $sumOfUnreadNotifications,
                    'timeSinceGotNotification' => $timeAgo,
                    'notificationAlert' => $greetingMessage,
                    ]);
            }
        });
    }
}





