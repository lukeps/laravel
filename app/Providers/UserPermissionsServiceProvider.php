<?php

namespace App\Providers;

use App\Exceptions\UserPermissionsException;
use Illuminate\Support\ServiceProvider;

class UserPermissionsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->bind(UserPermissionsException::class, function ($app) {

            return new UserPermissionsException();

        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
