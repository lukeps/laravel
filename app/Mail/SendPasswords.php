<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendPasswords extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $randomPassword;
    public $checkbox;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $randomPassword,$checkbox)
    {
        $this->user = $user;
        $this->randomPassword = $randomPassword;
        $this->checkbox = $checkbox;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        if ($this->checkbox['newPassword'] == 1) {

            return $this
                ->subject('Laravel - Newly generated temporary password')
                ->markdown('emails.users.send_password');
        } else {

            return $this
                ->subject('Laravel -  Initial temporary password')
                ->markdown('emails.users.send_password');
        }
    }
}
