<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use App\Event;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;


class CalendarEventController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() // url protection against unauthorized user
    {

    }

    /**
     * Show the application dashboard.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {

    }

    public function store(Request $request)
    {

        $input = $request->all();
        $content = "";

        $validator = Validator::make($input, [
            'eventTitle' => 'required|max:45',
            'startEvent' => 'required|date_format:Y-m-d\TH:i|before_or_equal:endEvent',
            'endEvent' => 'required|date_format:Y-m-d\TH:i|after_or_equal:startEvent',
            'members' => 'array',
            'events' => 'array',
            'location' => 'string'

        ], [
            'eventTitle.required' => 'Event title is demanded !',
            'eventTitle.max' => 'Title cannot include more than 45 characters !',
            'startEvent.required' => 'You must set correct start date !',
            'startEvent.before_or_equal' => 'The start event must be less than or equal ' . $input["endEvent"],
            'endEvent.after_or_equal' => 'The start event must be less than or equal ' . $input["startEvent"],
            'endEvent.required' => 'You must set correct start date !',
            'members.array' => 'Something went wrong, set "Add member" field again !',
            'events.string' => 'Something went wrong, set "Event type" field again !',
            'location.string' => 'Wrong location !'

        ]);

        if ($validator->fails()) {

            return response()->json($validator->getMessageBag(), 422);  //return specified validation errors
        }

        $event = new Event();
        $user = User::find(Auth::user()->id);
        $event->id = DB::table('events')->max('id') + 1; //auto incrementing id column for PGSQL
        $event->title = $input['eventTitle'];
        $arrayLength = count($input['members']);
        for ($i = 0; $i < $arrayLength; $i++) {
            if ($arrayLength - 1 == $i) {
                $content .= $input['members'][$i];
                break;
            }
            $content .= $input['members'][$i] . ',';
        }
        $event->member = $content;
        $event->event_type = $input['events'][0];
        $event->location = $input['location'];
        $event->start_event = $input['startEvent'];
        $event->end_event = $input['endEvent'];
        $event->save();
        $user->events()->attach($event->id); // many-to-many relation, relation between specific users and theirs events in events_users table
        $userEvents = self::getUserEvents();
        return response()->json(['userEvents' => $userEvents]);
    }



    public function edit($id)
    {

        $categories = Category::orderBy('name')->get();
        $article = Article::findOrFail($id);
        return view('articles.edit', [
            'article' => $article,
            'categories' => $categories
        ]);

    }

    public function update(Request $request, $id)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required|max:255',
            'short_text' => 'required|max:255',
            'text' => 'required',
            'category_id' => 'integer'
        ], [
            'name.required' => 'Wymagane podanie tytułu !',
            'name.max' => 'Tytuł nie może zawierać więcej niż 255 znaków !',
            'short_text.required' => 'Wymagane podanie opisu artykułu !',
            'short_text.max' => 'Krótki tekst nie może zawierać więcej niż 255 znaków !',
            'text.required' => 'Wymagane podanie szczegółowego opisu !',
            'category_id.integer' => 'Wybierz kategorię !'

        ]);

        if ($validator->fails()) {

            return response()->json($validator->getMessageBag(), 422);
        }

        $article = Article::findOrFail($id);
        $article->name = $input['name'];
        $article->short_text = $input['short_text'];
        $article->text = $input['text'];
        $article->alias = Str::slug($input['name'], '-');
        $article->author_id = Auth::user()->id;
        $article->category_id = $input['category_id'];
        $article->save();

        return response()->json($article);

    }

    public function destroy($id)
    {
        $event = Event::findOrFail($id);
        $event->delete();
        $userEvents = self::getUserEvents();
        return response()->json(['events' => $userEvents]);

    }

    public static function getEvents()
    {
        return DB::table('events_types')->select('type')->get();
    }

    public static function getUserEvents()
    {
        $user = User::find(Auth::user()->id);
        return $user->events;
    }

    public function getUserEventsJson()
    {
        $user = User::find(Auth::user()->id);
        $events = $user->events;
        return response()->json(['events' => $events]);
    }

}





