<?php

namespace App\Http\Controllers;

use App\Exceptions\UserPermissionsException;
use App\Mail\sendPasswords;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use App\Traits\UploadAvatarTrait;


class UsersController extends Controller
{

    use UploadAvatarTrait;

    private $userPermissions;

    /**
     * Create a new controller instance.
     *
     * @param UserPermissionsException $userPermissions
     */
    public function __construct(UserPermissionsException $userPermissions) // dependency injection
    {
        $this->userPermissions = $userPermissions;
    }


    /**
     * Show the application dashboard.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {

        if (!$request->ajax()) {
            $users = DB::table('users')
                ->orderBy('created_at', 'DESC')
                ->paginate(10);

            $userRoles = DB::table('roles')->select('name')->get();
            return view('users.index', [
                'users' => $users,
                'usersSum' => $this->getNumberOfUsers(),
                'activeUsersSum' => $this->getSumOfActiveUsers(),
                'userRoleSum' => $this->getUserRolesSum(),
                'userRoles' => $userRoles,
                'bannedUsersSum' => $this->getBannedUsersSum(),
            ]);

        } else if ($request->has('totalUsers')) {
            return $this->getSumOfUsersByTime($request->get('totalUsers'));
        } else if ($request->has('userActivity')) {

            if ($request->get('userActivity') == 'inactiveUsers')
                return $this->getSumOfInactiveUsersToJson();
            if ($request->get('userActivity') == 'activeUsers')
                return $this->getSumOfActiveUsersToJson();
        } else if ($request->has('userRole'))
            return $this->getUserSumByRoleType($request->get('userRole'));
    }

    public function create()
    {
        return view('users.create');
    }

    public function store(Request $request)

    {

        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required|max:25',
            'email' => 'required|email|max:35|unique:users,email', // szukanie identycznych emaili w bazie

        ], [
            'name.required' => 'Wymagane podanie imienia !',
            'name.max' => 'Imię nie powinno zawierać więcej niż 25 znaków!',
            'email.required' => 'Wymagane podanie adresu e-mail !',
            'email.max' => 'Adres e-mail nie powinien zawierać więcej niż 35 znaków !',
            'email.unique' => 'Adres e-mail jest zajęty, spróbuj ponownie.'

        ]);

        if ($validator->fails()) {

            return response()->json($validator->getMessageBag(), 422);

        }

        $user = new User();
        $randomPassword = Str::random(15);
        $user->id = DB::table('users')->max('id') + 1; //auto incrementing id column for PGSQL
        $user->name = $input['name'];
        $user->email = $input['email'];
        $user->password = Hash::make($randomPassword);
        $user->assignRole('writer');
        $user->save();
        Mail::to($input['email'])->send(new SendPasswords($user, $randomPassword, null));// wysylanie wiadomosci na maila z inputa

        return response()->json($user);

    }

    public function edit($id)
    {

        $users = User::findOrFail($id);
        return view('users.edit', [
            'users' => $users
        ]);

    }

    public function update(Request $request, $id)
    {

        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required|max:20',
            'email' => 'required|email|max:25',

        ], [
            'name.required' => 'Wymagane podanie imienia !',
            'name.max' => ' Imię nie powinno zawierać więcej niż 25znaków!',
            'email.required' => 'Wymagane podanie adresu e-mail !',
            'email.max' => 'Adres e-mail nie powinien zawierać więcej niż 35 znaków !',

        ]);

        if ($validator->fails()) {

            return response()->json($validator->getMessageBag(), 422);

        }
        $user = User::findOrFail($id);
        $user->name = $input['name'];
        $user->email = $input['email'];
        $user->save();

        if (isset($input['newPassword'])) {

            $randomPassword = Str::random(15);
            $user->password = Hash::make($randomPassword);
            Mail::to($input['email'])->send(new SendPasswords($user, $randomPassword, $input)); // wysylanie wiadomosci na maila z inputa

        }

        return response()->json($user);
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return response()->json($user);

    }

    public function jsonTable(Request $request)
    {
        $input = $request->all();

        $columns = [

            0 => 'users.id',
            1 => 'users.avatar',
            2 => 'users.name',
            3 => 'users.email'

        ];

        $users = DB::table('users')
            ->select([

                'users.id as u_id',
                'users.avatar as u_avatar',
                'users.name as u_name',
                'users.email as u_email'

            ]);


        foreach ($input['columns'] as $key => $column) {

            if (isset($column['search']['value']) && $column['search']['value'] !== null)
                $users = $users->where($columns[$key], 'like', $column['search']['value'] . '%');

        }

        foreach ($input['order'] as $key => $column) {

            if (isset ($column['column']))
                $users = $users->orderBy($columns[(intval($column['column']))], $column['dir']);
        }


        $totalData = $users->get();
        $usersSum = $totalData->count();
        $usersPerPage = $users->skip($input['start'])->take($input['length'])->get(); //rows per page
        $data['data'] = $usersPerPage;
        $data['recordsTotal'] = $usersSum;
        $data['recordsFiltered'] = $usersSum;

        return response()->json($data);

    }

    public function givePermissions(Request $request, $id)
    {

        $input = $request->all();
        $user = User::findOrFail($id);

        if (isset($input["permissionsCheckbox"])) {

            if ($user->hasRole('writer')) {
                $user->removeRole('writer');
            }
            $user->assignRole('admin');
            $user->save();

        }
        return response()->json($user);

    }

    public function revokePermissions(Request $request, $id)
    {
        try {

            $this->userPermissions->render($request, $id);

        } catch (UserPermissionsException $exception) {

            return response()->json([
                'error' => $exception->getMessage(),
            ], 422);
        }
    }

    public function banOrUnbanUser(Request $request)
    {
        $input = $request->all();
        $keys = $request->keys();

        if ($keys[1] === "ban")
            DB::table('users')->where('id', "=", $input['ban'])->update(["banned" => true]);

        elseif ($keys[1] === "unban") {
            DB::table('users')->where('id', "=", $input['unban'])->update(["banned" => false]);
        }
        return \redirect()->back();
    }

    public function getNumberOfUsers()
    {

        $usersSum = DB::table('users')->count();

        return $usersSum;

    }

    public function getSumOfUsersByTime($calculateUsersByTime)
    {

        $arr = ['dropdown-item1' => "0 second",
            'dropdown-item2' => '1 week',
            'dropdown-item3' => '1 month',
            'dropdown-item4' => '1 year'];
        foreach ($arr as $key => $value) {

            if ($key == $calculateUsersByTime) {
                $midnight = strtotime('today midnight' . ' ' . '-' . ' ' . $arr[$key]);
                $secondBeforeNextDay = strtotime('today midnight + 1 day - 1 second');
                $midnightToDate = Carbon::createFromTimestamp($midnight)->toDateTimeString('second');
                $secondBeforeNextDayToDate = Carbon::createFromTimestamp($secondBeforeNextDay)->toDateTimeString('second');
                $usersSum = DB::table('users')->whereBetween('created_at', [$midnightToDate, $secondBeforeNextDayToDate])->count();
                break;
            }
        }
        return response()->json(['usersSum' => $usersSum]);
    }

    public function setUserActivityStatus() // cron function for setting user activity periodically every day at midnight
    {

        $currentTimestampToDate = Carbon::now()->toDateTimeString('second');
        $monthAgo = strtotime($currentTimestampToDate . " " . "-" . " " . "1 month");
        $monthAgoToDate = Carbon::createFromTimestamp($monthAgo)->toDateTimeString('second');
        DB::table('users')->whereBetween('last_login_at', [$monthAgoToDate, $currentTimestampToDate])->update(['active_status' => true]); // active users
        DB::table('users')->whereNotBetween('last_login_at', [$monthAgoToDate, $currentTimestampToDate])->update(['active_status' => false]); //inactive users


    }

    public function getSumOfActiveUsers()
    {
        $activeUsersSum = DB::table('users')->where('active_status', '=', true)->count();

        return $activeUsersSum;
    }

    public function getSumOfActiveUsersToJson() //used in ajax requests
    {
        $activeUsersSum = DB::table('users')->where('active_status', '=', true)->count();

        return response()->json(["activeUsers" => $activeUsersSum]);
    }

    public function getSumOfInactiveUsersToJson()
    {

        $inactiveUsersSum = DB::table('users')->where('active_status', '=', false)->count();
        return response()->json(["inactiveUsers" => $inactiveUsersSum]);
    }

    public function getUserRolesSum()
    {
        $userRolesSum = DB::table('roles')->count();

        return $userRolesSum;
    }

    public function getUserSumByRoleType($calculateUsersByRole)
    {

        if ($calculateUsersByRole === "admin") {

            $usersSum = DB::table('model_has_roles')->where('role_id', "=", 1)->count();
        } else if ($calculateUsersByRole === "writer") {

            $usersSum = DB::table('model_has_roles')->where('role_id', "=", 2)->count();

        }

        return response()->json(["userRole" => $usersSum]);

    }

    public function getBannedUsersSum()
    {

        $bannedUsersSum = DB::table('users')->where('banned', "=", true)->count();
        return $bannedUsersSum;

    }

    public static function getUsersNames()
    {

        return DB::table('users')->select('name', 'id')->get();
    }


}





