<?php

namespace App\Http\Controllers;

use App\Category;
use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class CategoriesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() // url protection against unauthorized user
    {
        if (Auth::check() && Auth::id() == 1) {

            $this->middleware('auth');

        }

        $this->middleware('role:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $categories = DB::table('categories')
            ->orderBy('created_at', 'DESC')
            ->paginate(10);


        return view('categories.index', [
            'categories' => $categories,

        ]);

    }

    public function create()
    {
        return view('categories.create');
    }

    public function store(Request $request)
    {

        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required|max:50'

        ], [
            'name.required' => 'Wymagane podanie tytułu !',
            'name.max' => 'Tytuł nie może zawierać więcej niż 50 znaków !',

        ]);

        if ($validator->fails()) {
            return response()->json($validator->getMessageBag(), 422);
        }

        $category = new Category();
        $category->id = DB::table('categories')->max('id') + 1; //auto incrementing id column for PGSQL
        $category->name = $input['name'];
        $category->save();

        return response()->json($category);

    }

    public function edit($id)
    {

        $category = Category::findOrFail($id);
        return view('categories.edit', [
            'category' => $category
        ]);

    }

    public function update(Request $request, $id)
    {

        $input = $request->all(); //zwrocenie tablicy z danymi wypelnionymi w formularzu

        $validator = Validator::make($input, [
            'name' => 'required|max:255',

        ], [
            'name.required' => 'Wymagane podanie tytułu !',
            'name.max' => 'Tytuł nie może zawierać więcej niż 255 znaków !',
        ]);

        if ($validator->fails()) {

            return response()->json($validator->getMessageBag(), 422);
        }

        $category = Category::findOrFail($id);
        $category->name = $input['name'];
        $category->save();

        return response()->json($category);

    }

    public function destroy($id)
    {

        $category = Category::findOrFail($id);
        $category->delete();

        return response()->json($category);

    }

    public function jsonTable(Request $request)
    {
        $input = $request->all();

        $columns = [
            0 => 'categories.id',
            1 => 'categories.name',
        ];

        $categories = DB::table('categories')
            ->select([
                'categories.id as c_id',
                'categories.name as c_name',
            ]);

        foreach ($input['columns'] as $key => $column) {

            if (isset($column['search']['value']) && $column['search']['value'] !== null) {

                $categories = $categories->where($columns[$key], 'like', $column['search']['value'] . '%');
            }
        }

        foreach ($input['order'] as $key => $column) {

            if (isset ($column['column']))

                $categories = $categories->orderBy($columns[(intval($column['column']))], $column['dir']);
        }


        $totalData = $categories->get();
        $categoriesSum = $totalData->count();
        $categoriesPerPage = $categories->skip($input['start'])->take($input['length'])->get(); //rows per page
        $data['data'] = $categoriesPerPage;
        $data['recordsTotal'] = $categoriesSum;
        $data['recordsFiltered'] = $categoriesSum;

        return response()->json($data);

    }


}
