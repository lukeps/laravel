<?php

namespace App\Http\Controllers;

use App\Notifications\GreetingNotification;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use View;

class NotificationsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     * Show the application dashboard.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function sendGreetingNotification(Request $request)
    {

        $user = Auth::user();
        if (!Auth::user()->firstLogin && !$request->ajax()) { // greeting notification

            $systemMessage = 'Hello' . ' ' . '<b>' . ' ' . $user->name .  ' ' . '</b>' . ' ' . ' ' . 'welcome aboard !';
            $user->notify(new GreetingNotification($systemMessage));
            $user->firstLogin = true;
            $user->save();

            return 'You have got a message!';

        } else if ($request['notificationID'] && $request->ajax()) {

            $currentTime = (Carbon::now()->toDateTimeString('second'));
            $unreadNotificationID = $request['notificationID'];
            DB::table('notifications')->where('id', $unreadNotificationID)->whereNull('read_at')->update(['read_at' => $currentTime]);
            $sumOfUnreadNotifications = DB::table('notifications')->where('notifiable_id', '=', $user->id)->whereNull('read_at')->count();
            return response()->json(['SumOfUnreadNotifications' => $sumOfUnreadNotifications]);
        }

    }

    public function calculateTimeSinceGotNotification($timeToCalculateInMillis)
    {
        $message = '';
        $unitsInMillis = ["second" => 1000,
            "minute" => 60000,
            "hour" => 3600000,
            "day" => 86400000];
        $keys = array_keys($unitsInMillis);
        $currentDayOfMonth=date('t');
        for ($i = 0; $i < count($keys) - 1; $i++) {
            $lastIndex = count($keys) - 1;
            if ($timeToCalculateInMillis < $unitsInMillis [$keys[$i+1]]) {
                if ($timeToCalculateInMillis < $unitsInMillis [$keys[$i]] * 1.5) {
                    $message = number_format($timeToCalculateInMillis / $unitsInMillis [$keys[$i]], 0) . ' ' . $keys[$i] . ' ' . 'ago';
                    break;
                }
                else {
                    $message = number_format($timeToCalculateInMillis / $unitsInMillis [$keys[$i]], 0) . ' ' . $keys[$i] . 's' . ' ' . 'ago';
                    break;
                }
            } elseif ($i == 2 && $timeToCalculateInMillis < $unitsInMillis[$keys[$lastIndex]] * 1.5) { //statements for days
                $message = number_format($timeToCalculateInMillis / $unitsInMillis [$keys[$lastIndex]], 0) . ' ' . $keys[$lastIndex] . ' ' . 'ago';
            } elseif ($i == 2 && $timeToCalculateInMillis > $unitsInMillis[$keys[$lastIndex]] * 1.5) {
                $totalDays = $timeToCalculateInMillis / $unitsInMillis[$keys[$lastIndex]];
                if($totalDays<7) {
                    $message = number_format($timeToCalculateInMillis / $unitsInMillis [$keys[$lastIndex]], 0) . ' ' . $keys[$lastIndex]  . 's' . ' ' . 'ago';
                }
                elseif($totalDays >= 7 && $totalDays <14 ) { //intval prevents weeks against rounding their values //days to weeks
                    $message = intval($totalDays / 7) . ' ' . 'week' . ' ' . 'ago';
                }
                elseif($totalDays>=14 && $totalDays<=$currentDayOfMonth) {
                    $message = intval($totalDays / 7) . ' ' . 'weeks' . ' ' . 'ago';
                }
                else{
                    if($totalDays>$currentDayOfMonth && $totalDays<$currentDayOfMonth*1.5) //days to months
                        $message = number_format($totalDays / $currentDayOfMonth,0) . ' ' . 'month' . ' ' . 'ago';
                    else
                            $message = number_format($totalDays/$currentDayOfMonth,0) . ' ' . 'months' . ' ' . 'ago';
                    }
                }
            }
        return $message;
    }


    public function updateTimeOfReceivedNotification()
    {

        foreach (Auth::user()->notifications as $notification) {//setting  time since got a last notification
            $currentTimestamp = Carbon::now()->timestamp;
            DB::table('notifications')->where('id', $notification->id)->update(['current_timestamp' => $currentTimestamp]);
            $timeAgo = ($currentTimestamp - strtotime($notification->created_at)) * 1000;
            DB::table('notifications')->where('id', $notification->id)->update(['time_ago' => $timeAgo]);
        }

    }

    public function deleteNotification(Request $request)
    {
        $notificationToDeleteID = $request->get('id');

        foreach (Auth::user()->notifications as $notification) {

            $notificationID = $notification->id;

            if ($notificationToDeleteID == $notificationID)
                $notification->delete();

        }
        $sumOfUnreadNotifications = DB::table('notifications')->where('notifiable_id',Auth::user()->id)->whereNull('read_at')->count();


        return response()->json(['deletedNotification' => $notificationToDeleteID,
                                'sumOfUnreadNotifications' => $sumOfUnreadNotifications]);

    }

}
