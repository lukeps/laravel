<?php

namespace App\Http\Controllers;

use App\TotalViews;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;


class TotalViewsController extends Controller
{

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {

    }

    public function createPageView(Request $request)
    {

        $pageView = new TotalViews();
        $pageView->user_id = Auth::user()->id;
        $pageView->username = Auth::user()->name;
        $pageView->email = Auth::user()->email;
        $pageView->nationality = Auth::user()->nationality;
        $pageView->session_id = Session::getId();
        $pageView->ip_address = (Auth::user() !== null) ? Auth::user()->last_login_ip : $request->ip();
        $pageView->save();

    }

    public function setPageView(Request $request)
    {

        $ipAddressColumn = DB::table('total_page_views')->select('ip_address')->get();
        if (!$ipAddressColumn->contains('ip_address', Auth::user()->last_login_ip)) {

            $this->createPageView($request);

        }

        $totalViewsTable = DB::table('total_page_views')->get();
        $currentTimeInMillis = Carbon::now()->timestamp;

        foreach ($totalViewsTable as $value) {

            //if current activity is at least 24h after the last timestamp from db then create another page view

            if ($value->ip_address === $request->ip()) {
                //find latest user activity in db
                $latestActivity = DB::table('total_page_views')->where('ip_address', '=', $request->ip())->orderBy('created_at', 'desc')->limit(1)->get();
                $createdAt = $latestActivity[0]->created_at;
                $date = Carbon::parse($createdAt);
                $nextDay = $date->addDay();
                $nextDayTimestamp = $nextDay->getTimestamp();

                if ($currentTimeInMillis > $nextDayTimestamp) {

                    $this->createPageView($request);
                    break;
                }

            }
        }
    }

    public function getPageView(){

        $pageViews = DB::table('total_page_views')->count();

        return response()->json(['pageViews' => $pageViews]);
    }
}
