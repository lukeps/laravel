<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Interfaces\DatabaseInterface;

class MYSQLDataController implements DatabaseInterface
{


    public function __construct()
    {

    }

    public function getDBSize(Request $request)
    {

        $dbSchema = DB::select(DB::raw('select table_schema AS "Database", SUM(data_length + index_length) / 1024 / 1024 as "size" FROM information_schema.TABLES GROUP BY table_schema'));
        $mysqlDbSize = $dbSchema[0]->size;
        return $mysqlDbSize;
    }


}
