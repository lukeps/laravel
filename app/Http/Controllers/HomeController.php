<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Traits\CronMessagesTrait;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{

    use CronMessagesTrait;
    /**
     * Create a new controller instance.
     *
     * @param Request $request
     */
    public function __construct() // url protection against unauthorized user
    {
        $this->middleware('role:admin');

    }

    /**
     * Show the application dashboard.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request){

        $pageView = new TotalViewsController();

        $pageView->setPageView($request);

        if($request->ajax()){
            if($request->has('pageViews'))
            return $pageView->getPageView();
        }

        $getDistinctYears = User::select(DB::raw('EXTRACT(YEAR FROM created_at) as year'))->distinct()->get()->pluck('year');
        $lastAddedArticles = DB::table('articles')
            ->select('name', 'created_at')
            ->orderBy('created_at', 'DESC')
            ->limit(5)
            ->get();

        $lastEditedArticles = DB::table('articles')
            ->select('name', 'updated_at')
            ->orderBy('updated_at', 'DESC')
            ->limit(5)
            ->get();

        //$this->sendCronMessagesToCloudServer(); -< testing
        return view('home', [
            'lastAddedArticles' => $lastAddedArticles,
            'lastEditedArticles' => $lastEditedArticles,
            'userRoles' => Auth::user()->getRoleNames(),
            'userEmail' => Auth::user()->email,
            'registeredDate' => Auth::user()->created_at,
            'userNationality' => Auth::user()->nationality,
            'registeredDateYears' => $getDistinctYears,
            'users' => UsersController::getUsersNames(),
            'events' => CalendarEventController::getEvents(),
            'userEvents' => CalendarEventController::getUserEvents(),
        ]);

    }

    public function getRegisteredUsersSum()
    {
        $registeredUsersPerMonth = [];

        for ($idx = 0; $idx < 12; $idx++)
            $registeredUsersPerMonth[] = DB::table('users')->whereMonth('created_at', '=', $idx)->count();

        return \response()->json(['registeredUsersData' => $registeredUsersPerMonth]);

    }

    public function parseCronLogsJson()
    {

        $parsedData = [];
        $message = null;
        $time = null;
        $date = null;

        $text = Storage::disk('s3')->get("cron logs/logs");

        $explodedTextContent = array_filter(explode("\n", $text));

        foreach ($explodedTextContent as $singleLog) {

            $obj = new \stdClass();
            $filteredStr = trim(preg_replace('/[^[:alpha:]]/', ' ', $singleLog));
            $dateToFormat = preg_replace('/[^0-9]/', '', $singleLog);
            $splitDate = str_split($dateToFormat, 8);
            $date = Carbon::parse($splitDate[0])->format('jS F Y');
            $time = Carbon::parse($splitDate[1])->format('H:m:s');
            $obj->message = $filteredStr;
            $obj->date = $date;
            $obj->time = $time;
            $parsedData[] = $obj;
        }

        return \response()->json(['data' => $parsedData]);
    }


    public function usersPerDayInSingleMonth(Request $request)
    {

        $year = $request->get('year');
        $month = Carbon::parse($request->get('month'));

        $numberOfMonth = $month->format('m');
        $monthName = $month->format('M');
        $daysInMonth = $month->daysInMonth;
        $usersSum = [];
        $days = [];
        for ($i = 1; $i <= $daysInMonth; $i++) {
            $date = $year . "-" . $numberOfMonth . "-" . 0 . $i; //if i<10
            $usersSum[] = DB::table('users')->whereDate('created_at', '=', $date)->count();
            $days[] = $i;
        }

        return response()->json(['usersSum' => $usersSum,
            'monthDays' => $days,
            'monthName' => $monthName]);

    }

    public function setWeeksPeriod($startDate, $endDate)
    {
        $arr = [];
        $arr2 = [];
        $startWeek = null;
        $endWeek = null;
        $startPeriod = CarbonImmutable::create($startDate[0], $startDate[1], $startDate[2]);
        $endPeriod = CarbonImmutable::create($endDate[0], $endDate[1], $endDate[2]);
        $sumOfMonths = $startPeriod->monthsUntil($endPeriod)->count();

        for ($i = 0; $i < $sumOfMonths; $i++) {

            $start = $startPeriod->startOfMonth();
            $end = $startPeriod->endOfMonth();
            $sumOfWeeks = $start->weeksUntil($end)->count();

            if ($i > 0) {
                $start = $startPeriod->addMonth()->startOfMonth();
                $end = $startPeriod->addMonth()->endOfMonth();
                $sumOfWeeks = $end->weekNumberInMonth; //it seems weeksUntil function doesnt work properly for March case
            }

            for ($j = 0; $j < $sumOfWeeks; $j++) {
                $data = new \stdClass();
                $startDayWeek = $start->startOfWeek($start->dayOfWeek);
                $endDayWeek = $startDayWeek->endOfWeek();
                if ($j === 0) {
                    $data->startWeek = $startDayWeek->format('Y-m-d');
                    $data->endWeek = $endDayWeek->format('Y-m-d');
                    $arr[] = $data;

                } elseif ($j > 0 && $j < $sumOfWeeks - 1) {
                    $start = $endDayWeek->addSecond();
                    $startDayWeek = $start->startOfWeek($start->dayOfWeek);
                    $startPeriod = $startDayWeek;
                    $endDayWeek = $startDayWeek->endOfWeek();
                    $data->startWeek = $startDayWeek->format('Y-m-d');
                    $data->endWeek = $endDayWeek->format('Y-m-d');
                    $arr[] = $data;
                } else {
                    $startEndWeek = $endDayWeek->addSecond();
                    $lastMonthDay = $end->dayOfWeek;
                    $endDayWeek = $startEndWeek->endOfWeek($lastMonthDay);
                    $data->startWeek = $startEndWeek->format('Y-m-d');
                    $data->endWeek = $endDayWeek->format('Y-m-d');
                    $arr[] = $data;

                }
            }
            $arr2[] = $arr;
            $arr = [];
        }

        return $arr2;

    }


    public function getUsersSumFromDatesPeriod(Request $request)
    {

        $request = $request->get('quarterPeriod');
        $startDate = $request['start'];
        $endDate = $request['end'];
        $slicedStartDate = explode('-', $startDate);
        $slicedEndDate = explode('-', $endDate);

        $dates = $this->setWeeksPeriod($slicedStartDate, $slicedEndDate);

        $usersSum = $this->getUsersByYearQuarter($dates);

        return response()->json(['data' => $usersSum]);


    }

    public function getUsersByYearQuarter($dates)
    {

        $usersPerWeekInYearQuarter = [];
        foreach ($dates as $date) {
            foreach ($date as $key => $value) {
                $obj = new \stdClass();
                $usersSum = DB::table('users')->whereBetween('created_at', [$value->startWeek, $value->endWeek])->count();
                $obj->startWeek = $value->startWeek;
                $obj->endWeek = $value->endWeek;
                $obj->usersSum = $usersSum;
                $usersPerWeekInYearQuarter[] = $obj;
            }
        }

        return $usersPerWeekInYearQuarter;
    }

    public function getUsersByNationalities()
    {

        $arr = [];
        $nationalities = DB::table('users_nationalities')->get();

        foreach ($nationalities as $nationality) {

            $dataObj = new \stdClass();
            $code = $nationality->code;
            $usersSum = DB::table('users')->where('nationality', '=', $nationality->code)->count();
            $dataObj->id = $code;
            $dataObj->value = $usersSum;
            $arr[] = $dataObj;
        }

        return response()->json($arr);

    }
}


