<?php

namespace App\Http\Controllers;

use App\Interfaces\DatabaseInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostgreSQLDataController implements DatabaseInterface{

    public function __construct(){

    }

    public function getDBSize(Request $request)
    {

        $dbSize =DB::select(DB::raw('SELECT pg_database_size(current_database())'));
        $sizeInBytes = $dbSize[0]->pg_database_size;
        $multiplier = 1.0*pow(10,-6); //it converts bytes to megabytes
        return $convertedDBSizeToMB = round($sizeInBytes * $multiplier,2);


    }
}
