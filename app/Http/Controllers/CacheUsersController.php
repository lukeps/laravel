<?php

namespace App\Http\Controllers;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Throwable;

class CacheUsersController
{

    public function trackUsers() { //store in framework/cache/data info about every "logged-in" user in separate file

        try {
            $name = Auth::user()->name;
            $keys = [];

            if (Cache::get($name)) {
                $storage = Cache::getStore();
                $filesystem = new Filesystem();
                foreach ($filesystem->allFiles($storage->getDirectory()) as $file1) {
                    $fileContent = $file1->getContents();
                    preg_match('`"([^"]+)"`', $fileContent,$username);
                    preg_match('`[0-9]*`', $fileContent,$timestamp);
                    $currentTimestamp = now()->timestamp;

                    $obj = new \stdClass();
                    if (isset($username[1])) {
                        if ($currentTimestamp > $timestamp[0] && $username[1] === $name) { //delete cache element if is stale for users who makes requests
                            Cache::forget($username[1]);
                            $username[1] = null;

                        }
                        if ($currentTimestamp <= $timestamp[0] && $username[1] === $name) { //update for requested user
                            Cache::forget($username[1]);
                            Cache::store('file')->put($name, Auth::user()->name, 600);
                        }

                        if ($currentTimestamp > $timestamp[0] && $username[1] !== $name) { //delete cache element if is stale for other users
                            Cache::forget($username[1]);
                            $username[1] = null;

                        }
                        if ($currentTimestamp <= $timestamp[0] && $username[1] !== $name) { //update for other users in cache memory

                            Cache::forget($username[1]);
                            Cache::store('file')->put($username[1], $username[1], 600);
                        }
                        if ($username[1] !== null) {
                            $obj->cachedName = $username[1];
                            $keys[] = $obj;
                        }

                    }

                }

            } else {

                Cache::store('file')->put($name, Auth::user()->name, 600);

            }

            return $keys;

        }

        catch(Throwable $e){ //bug, sometimes throwing an error - why ?
        }
    }

    public function sendJsonCachedData($result)
    {

        return response()->json(['cachedID' => $result]);

    }


}
