<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Stevebauman\Location\Location;


class ArticlesController extends NotificationsController
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() // url protection against unauthorized user
    {


    }

    /**
     * Show the application dashboard.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {

        if (!$request->ajax()) {

            $this->setUserNationality();
            $input = $request->all();
            $columns = [
                'a_id',
                'a_name',
                'a_created_at',
                'c_name',
                'u_name'

            ];
            $dirs = ['asc', 'desc'];

            $articles = DB::table('articles')
                ->select([
                    'articles.id as a_id',
                    'articles.name as a_name',
                    'articles.created_at as a_created_at',

                    'categories.name as c_name',

                    'users.name as u_name'
                ])
                ->leftJoin('categories', 'articles.category_id', '=', 'categories.id')
                ->leftJoin('users', 'articles.author_id', '=', 'users.id');


            if (isset($input['sort']) && !in_array($input['sort'], $columns) || isset($input['dir']) && !in_array($input['dir'], $dirs)) {

                abort(404);
            }

            $articles = $articles->get();

            return view('articles.index', [
                'articles' => $articles,
                'users' => $articles,
                'usersNames' => UsersController::getUsersNames(),
                'articlesSum' => $this->getSumOfArticles(),
                'activeUsersSum' => $this->getSumOfActiveUsers(),
                'userRoleSum' => $this->getSumOfArticles(),
                'articlesCategories' => $this->getCategories(),
                'bannedUsersSum' => $this->getBannedUsersSum(),
                'usersNationalities' => $this->getUsersNationalities(),

            ]);
        } else if ($request->has('totalArticles')) {
            return $this->getSumOfArticlesByTime($request->get('totalArticles'));
        } else if ($request->has('userActivity')) {

            if ($request->get('userActivity') == 'inactiveUsers')
                return $this->getSumOfInactiveUsersToJson();
            if ($request->get('userActivity') == 'activeUsers')
                return $this->getSumOfActiveUsersToJson();
        } else if ($request->has('articleByCategory'))
            return $this->getArticlesByCategoriesSum($request->get('articleByCategory'));

    }

    public function create()
    {
        $categories = Category::orderBy('name')->get();
        return view('articles.create', [
            'categories' => $categories,

        ]);
    }

    public function store(Request $request)
    {

        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required|max:255',
            'short_text' => 'required|max:255',
            'text' => 'required',
            'categoryGroup' => 'integer'

        ], [
            'name.required' => 'Wymagane podanie tytułu !',
            'name.max' => 'Tytuł nie może zawierać więcej niż 255 znaków !',
            'short_text.required' => 'Wymagane podanie opisu artykułu !',
            'short_text.max' => 'Krótki tekst nie może zawierać więcej niż 255 znaków !',
            'text.required' => 'Wymagane podanie szczegółowego opisu !',
            'categoryGroup.integer' => 'Wybierz kategorię !'

        ]);

        if ($validator->fails()) {

            return response()->json($validator->getMessageBag(), 422);  //return specified validation errors
        }

        $article = new Article();
        $article->id = DB::table('articles')->max('id') + 1; //auto incrementing id column for PGSQL
        $article->name = $input['name'];
        $article->short_text = $input['short_text'];
        $article->text = $input['text'];
        $article->alias = Str::slug($input['name'], '-');
        $article->author_id = Auth::user()->id;
        $article->category_id = $input['categoryGroup'];
        $article->save();

        return response()->json($article);

    }

    public function show($id)
    {
        $article = Article::findOrFail($id);
        return view('articles.show', [
            'article' => $article,
        ]);
    }

    public function edit($id)
    {

        $categories = Category::orderBy('name')->get();
        $article = Article::findOrFail($id);
        return view('articles.edit', [
            'article' => $article,
            'categories' => $categories
        ]);

    }

    public function update(Request $request, $id)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required|max:255',
            'short_text' => 'required|max:255',
            'text' => 'required',
            'category_id' => 'integer'
        ], [
            'name.required' => 'Wymagane podanie tytułu !',
            'name.max' => 'Tytuł nie może zawierać więcej niż 255 znaków !',
            'short_text.required' => 'Wymagane podanie opisu artykułu !',
            'short_text.max' => 'Krótki tekst nie może zawierać więcej niż 255 znaków !',
            'text.required' => 'Wymagane podanie szczegółowego opisu !',
            'category_id.integer' => 'Wybierz kategorię !'

        ]);

        if ($validator->fails()) {

            return response()->json($validator->getMessageBag(), 422);
        }

        $article = Article::findOrFail($id);
        $article->name = $input['name'];
        $article->short_text = $input['short_text'];
        $article->text = $input['text'];
        $article->alias = Str::slug($input['name'], '-');
        $article->author_id = Auth::user()->id;
        $article->category_id = $input['category_id'];
        $article->save();

        return response()->json($article);

    }

    public function destroy($id)
    {
        $article = Article::findOrFail($id);
        $article->delete();

        return response()->json($article);

    }

    public function jsonTable(Request $request)
    {
        $input = $request->all();

        $columns = [
            0 => 'articles.id',
            1 => 'articles.name',
            2 => 'articles.created_at',
            3 => 'categories.name',
            4 => 'users.name',
        ];

        $articles = DB::table('articles')
            ->select([
                'articles.id as a_id',
                'articles.name as a_name',
                'articles.created_at as a_created_at',

                'categories.name as c_name',

                'users.name as u_name'
            ])
            ->whereNotNull('categories.name')
            ->leftJoin('categories', 'articles.category_id', '=', 'categories.id')
            ->leftJoin('users', 'articles.author_id', '=', 'users.id');

        if (isset($input['columns'])) { //might be useless line of code

            foreach ($input['columns'] as $key => $column) { // dla kazdego pola input ustaw klucz

//                $columns[$key] <--- zwraca konkretna kolumne z bazy danych
                if (isset($column['search']['value']) && $column['search']['value'] !== null) {//jezeli zostalo wpisane cos w konkretnego inputa i nie jest nullem
                    $articles = $articles->where($columns[$key], 'like', $column['search']['value'] . '%');
                }
            }
        }

        if (isset($input['order'])) { //might be useless line of code

            foreach ($input['order'] as $key => $column) {

                if (isset ($column['column'])) //jezeli uruchomione wyszukiwanie, to posortuj rosnaco lub malejaco wzgledem konkretnego indeksu kolumny

                    $articles = $articles->orderBy($columns[(intval($column['column']))], $column['dir']);
            }
        }

        $totalData = $articles->get();
        $articlesSum = $totalData->count();
        $articlesPerPage = $articles->skip($input['start'])->take($input['length'])->get(); //rows per page
        $data['data'] = $articlesPerPage;
        $data['recordsTotal'] = $articlesSum;
        $data['recordsFiltered'] = $articlesSum;
        return response()->json($data);

    }

    public function getSumOfArticles()
    {

        $articlesSum = DB::table('articles')->count();

        return $articlesSum;

    }

    public function getSumOfArticlesByTime($calculateArticlesByTime)
    {

        $arr = ['dropdown-item1' => "0 second",
            'dropdown-item2' => '1 week',
            'dropdown-item3' => '1 month',
            'dropdown-item4' => '1 year'];
        foreach ($arr as $key => $value) {

            if ($key == $calculateArticlesByTime) {
                $midnight = strtotime('today midnight' . ' ' . '-' . ' ' . $arr[$key]);
                $secondBeforeNextDay = strtotime('today midnight + 1 day - 1 second');
                $midnightToDate = Carbon::createFromTimestamp($midnight)->toDateTimeString('second');
                $secondBeforeNextDayToDate = Carbon::createFromTimestamp($secondBeforeNextDay)->toDateTimeString('second');
                $articlesSum = DB::table('articles')->whereBetween('created_at', [$midnightToDate, $secondBeforeNextDayToDate])->count();
                break;
            }
        }
        return response()->json(['articlesSum' => $articlesSum]);
    }

    public function setUserActivityStatus() // cron function for setting user activity periodically every hour
    {

        $currentTimestampToDate = Carbon::now()->toDateTimeString('second');
        $monthAgo = strtotime($currentTimestampToDate . " " . "-" . " " . "1 month");
        $monthAgoToDate = Carbon::createFromTimestamp($monthAgo)->toDateTimeString('second');
        DB::table('users')->whereBetween('last_login_at', [$monthAgoToDate, $currentTimestampToDate])->update(['active_status' => true]); // active users
        DB::table('users')->whereNotBetween('last_login_at', [$monthAgoToDate, $currentTimestampToDate])->update(['active_status' => false]); //inactive users

    }

    public function getSumOfActiveUsers()
    {
        $activeUsersSum = DB::table('users')->where('active_status', '=', true)->count();

        return $activeUsersSum;
    }

    public function getSumOfActiveUsersToJson() //used in ajax requests
    {
        $activeUsersSum = DB::table('users')->where('active_status', '=', true)->count();

        return response()->json(["activeUsers" => $activeUsersSum]);
    }

    public function getSumOfInactiveUsersToJson()
    {

        $inactiveUsersSum = DB::table('users')->where('active_status', '=', false)->count();
        return response()->json(["inactiveUsers" => $inactiveUsersSum]);
    }

    public function getArticlesByCategoriesSum($category)
    {
        $articlesByCategoriesSum = DB::table('articles')->select(['articles.name as a_name', 'articles.author_id as a_id', 'articles.category_id as art_cat_id', 'categories.id as cat_id', 'categories.name as c_name'])
            ->where('categories.name', '=', $category)->leftJoin('categories', 'articles.category_id', '=', 'categories.id')->count();
        return response()->json(['articlesByCategorySum' => $articlesByCategoriesSum]);
    }

    public function getCategories()
    {

        return DB::table('categories')->select('name')->get();
    }

    public function getBannedUsersSum()
    {

        $bannedUsersSum = DB::table('users')->where('banned', "=", true)->count();
        return $bannedUsersSum;

    }

    public function getArticlesByUser(Request $request)
    {

        $userID = $request->get('id');


        $articlesSumByUser = DB::table('articles')->where('author_id', '=', $userID)->count();

        return response()->json(['articlesSum' => $articlesSumByUser]);
    }

    public function setUserNationality()
    {

        if (Auth::user()->nationality === null) {

            $location = new Location();
            $userLocation = $location->get();

            DB::table('users')->where('id', '=', Auth::user()->id)->update(['nationality' => $userLocation->countryCode]);
        }

    }

    public function getUsersNationalities()
    {

        return  DB::table('users')->select(['id','nationality'])->get();

    }



}





