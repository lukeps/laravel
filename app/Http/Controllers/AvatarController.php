<?php

namespace App\Http\Controllers;

use App\Audio;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Traits\UploadAvatarTrait;


class AvatarController extends Controller
{

    use UploadAvatarTrait;

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {

    }


    public function uploadAvatar(Request $request)
    {

        $input = $request->all();

        $validator = Validator::make($input, [
        ]);

        if ($validator->fails()) {

            return redirect()->back()->with('error', 'Wystąpił błąd! Spróbuj ponownie.');

        } else {

            if ($request->has('userImage')) {

                $fileName = 'users avatars/' . Auth::user()->name;
                $image = $this->handleUploadedAvatar($request->file('userImage'));
                Storage::disk('s3')->put($fileName, (string)($image));

            }

            if ($request->has('audio')) { //for uploaded audio - have to be moved somewhere else in the future

                $audio = new Audio();
                $audioFile = $request->file('audio');
                $folder = '/public/';
                $audio->name = $folder . $audioFile->getClientOriginalName();
                $audio->type = $audioFile->getClientOriginalExtension();
                $audio->size = $audioFile->getSize();
                Storage::disk('public')->put($audioFile->getClientOriginalName(), file_get_contents($audioFile));
                $audio->save();
            }
        }

        return redirect()->back();
    }


    public function deleteUploadedAvatar(Request $request)
    {

        $user = User::findOrFail(auth()->user()->id);

        if ($request->has('userImageDelete')) {

            if ($user->avatar = !null) {

                Storage::disk('public')->delete($user->avatar);
                $user->avatar = null;
                $user->save();

            }

        }

        return redirect()->back();
    }

}

