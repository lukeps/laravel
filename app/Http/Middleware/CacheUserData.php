<?php

namespace App\Http\Middleware;

use App\Http\Controllers\CacheUsersController;
use Closure;
use Illuminate\Support\Facades\Auth;

class CacheUserData
{
    public function handle($request, Closure $next)
    {

        $response = $next($request);

        if (Auth::user()) {
            $cachedUsers = new CacheUsersController();
            $result = $cachedUsers->trackUsers(); //double request bug <- sometimes while hit home page this method is called two times
                                                    // it was noticed because of double creation of cache files in data/storage
                                                // critical bug must be fixed asap
            if ($request->ajax()) {
                if ($request->get('cachedID')) {
                    return $cachedUsers->sendJsonCachedData($result);
                }

            }
            return $response;

        } else {

            return redirect('/login');
        }

    }


}
