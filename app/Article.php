<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{

    protected $table = 'articles';

    protected $id = 'name';

    protected $primaryKey = 'id';

    protected $fillable = [
        'name','alias','short_text','author_id','text','category_id','created_at','updated_at',
    ];
    protected $casts = [

        'category_id' => 'integer'

    ];

}
