<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Audio extends Model
{

    protected $table = 'audio';

    protected $primaryKey = 'id';

    protected $fillable = [
        'name','type','size'
    ];
    protected $timestamps = false;

}
