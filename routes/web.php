<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes();
Route::prefix('admin')->group(function () {
    Route::group(['namespace' => "App\\"], function () { //route group for classes which implements DatabaseInterface
        Route::post('/home/json/mysqlsize', 'MySQLDatabase@getDBData')->name('home.mysqlSize');
        Route::post('/home/json/postgresqlsize', 'PostgreSQLDatabase@getDBData')->name('home.postgresqlSize');

    });
    Route::group(['namespace' => 'App\Http\Controllers'], function () {

        Route::middleware(['auth', 'cache.users'])->group(function () {
            Route::get('/home', 'HomeController@index')->name('home.index');
            Route::post('/home', 'NotificationsController@sendGreetingNotification')->name('n.greet');
            Route::delete('/home', 'NotificationsController@deleteNotification')->name('home.deleteNotification');
            Route::get('/home/json/registeredUsersData', 'HomeController@getRegisteredUsersSum')->name('home.getRegisteredUsersSum');
            Route::get('/home/json/cronLogs', 'HomeController@parseCronLogsJson')->name('home.cronLogs');
            Route::post('/home/json/usersperday', 'HomeController@usersPerDayInSingleMonth')->name('home.usersPerDay');
            Route::post('/home/json/usersperweeks', 'HomeController@getUsersSumFromDatesPeriod')->name('home.getUsersSumFromDatesPeriod');
            Route::post('/home/json/userspernationalities', 'HomeController@getUsersByNationalities')->name('home.getUsersByNationalities');
            Route::post('/home/json/addcalendarevent', 'CalendarEventController@store')->name('home.storeCalendarEvent');
            Route::get('/home/json/eventsperuser', 'CalendarEventController@getUserEventsJson')->name('home.getUserEvents');
            Route::delete('/home/{id}', 'CalendarEventController@destroy')->name('home.destroy');


            Route::get('/articles', 'ArticlesController@index')->name('articles.index');
            Route::get('/articles/create', 'ArticlesController@create')->name('articles.create');
            Route::post('/articles', 'ArticlesController@store')->name('articles.store');
            Route::get('/articles/{id}', 'ArticlesController@show')->name('articles.show');
            Route::get('/articles/{id}/edit', 'ArticlesController@edit')->name('articles.edit');
            Route::patch('/articles/{id}', 'ArticlesController@update')->name('articles.update');
            Route::delete('/articles/{id}', 'ArticlesController@destroy')->name('articles.destroy');
            Route::post('/articles/json/table', 'ArticlesController@jsonTable')->name('articles.jsonTable');
            Route::post('/articles/json/articlesSum', 'ArticlesController@getArticlesByUser')->name('articles.sum');

            Route::get('/users', 'UsersController@index')->name('users.index');
            Route::get('/users/create', 'UsersController@create')->name('users.create');
            Route::post('/users', 'UsersController@store')->name('users.store');
            Route::get('/users/{id}', 'UsersController@show')->name('users.show');
            Route::get('/users/{id}/edit', 'UsersController@edit')->name('users.edit');
            Route::patch('/users/{id}', 'UsersController@update')->name('users.update');
            Route::delete('/users/{id}', 'UsersController@destroy')->name('users.destroy');
            Route::post('/users/json/table', 'UsersController@jsonTable')->name('users.jsonTable');
            Route::post('/users/{id}', 'UsersController@givePermissions')->name('users.givePermissions');
            Route::delete('/users1/{id}', 'UsersController@revokePermissions')->name('users.revokePermissions');
            Route::post('/users1', 'UsersController@banOrUnbanUser')->name('users.banOrUnbanUser');
            Route::post('/', 'AvatarController@uploadAvatar')->name('users.uploadAvatar');
            Route::delete('/users', 'AvatarController@deleteUploadedAvatar')->name('users.deleteUploadedAvatar');

            Route::get('/categories', 'CategoriesController@index')->name('categories.index');
            Route::get('/categories/create', 'CategoriesController@create')->name('categories.create');
            Route::post('/categories', 'CategoriesController@store')->name('categories.store');
            Route::get('/categories/{id}', 'CategoriesController@show')->name('categories.show');
            Route::get('/categories/{id}/edit', 'CategoriesController@edit')->name('categories.edit');
            Route::patch('/categories/{id}', 'CategoriesController@update')->name('categories.update');
            Route::delete('/categories/{id}', 'CategoriesController@destroy')->name('categories.destroy');
            Route::post('/categories/json/table', 'CategoriesController@jsonTable')->name('categories.jsonTable');


        });
    });

});
//routes without specific group
Route::get('/logout', 'App\Http\Controllers\Auth\LoginController@logout');
Route::post('/login', 'App\Http\Controllers\Auth\LoginController@authenticate');




