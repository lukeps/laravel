<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTotalPageViews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('total_page_views', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("user_id")->nullable();
            $table->string("username")->nullable();
            $table->string("email")->unique()->nullable();
            $table->string("nationality")->nullable();
            $table->string("session_id");
            $table->string("ip_address")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('total_page_views');
    }
}
