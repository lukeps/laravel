@component('mail::message')
<p style="margin-bottom: 10px;">
Twoje hasło znajduję się poniżej:</p>
<p style="margin-bottom: 10px;">{{$randomPassword}}</p>
<p>Wygenerowano: {{date("F j, Y, g:i a")}}</p><br>
{{ config('app.name') }}
@endcomponent
