@if(Auth::check())
    <div class="sidebar w3-bar-block w3-light-grey " style="font-size:1vw;width:12.5%;  min-height: 93vh;padding-left: 0 !important;padding-right: 0 !important;
 box-shadow: -10px 18px 20px 0 black">
        @if(Session::has('error'))
            <div class="alert alert-danger" style="display: flex; align-items: center; justify-content: center;">
                <h4>{{Session::get('error')}}</h4></div>
        @endif
        <div class="row" style="display: flex;align-items: center;margin-top: 5%">
            @if(Storage::disk('s3')->exists('users avatars/' . Auth::user()->name))
                <img src="{{Storage::disk('s3')->url('users avatars/' . Auth::user()->name)}}" alt="Avatar" class="avatar"
                     style="width: 35%;border-radius: 50%;margin-bottom: 1vh;margin-left: 7.5%;">
            @else
                <div class="customAvatar"
                     style="width: 35%; border-radius: 50%;margin-bottom: 1vh;margin-left: 7.5%;"></div>
            @endif
            <div style="width: 50%">
                <p style="padding-left: 25%;font-family: 'Poppins';font-weight:bold;font-size: 1vw;font-style: italic">
                    Witaj!</p>
                <p style="margin-left: 6%;margin-bottom: 10%;margin-top:0.5%;font-family: 'Poppins';font-weight:bold;font-size: 0.7vw;">{{Auth::user()->name}}</p>
                <div style="height: 1vh;">
                    <img src="{{asset("images/OnlineStatus.jpg")}}" id="expiryController"
                         style="width:0.5vw; height:inherit;border-radius: 50%;margin-left: 1%;position: absolute;">
                    <p id="statusDescription"
                       style="margin-left: 35%;font-family: 'Poppins';font-weight:bold; font-size: 0.5vw !important;"></p>
                </div>
            </div>
        </div>
        <div class="row col-6" style="display: flex;justify-content: center;padding: 0;">
            <form id="uploadAvatarForm" action="{{route('users.uploadAvatar')}}" method="POST" role="form"
                  enctype="multipart/form-data" style="display: inline-block;">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <label for="userImage">
                    <div class="w3-button w3-green" style="line-height: 2vh;padding:0;width: 1.5vw;height: 2vh;">+</div>
                </label>
                <input id="userImage" name="userImage" type="file" style="display: none">
            </form>
            <form action="{{route('users.deleteUploadedAvatar')}}" method="POST" style="display: inline-block;">
                {{ csrf_field() }}
                <label for="userImageDelete">
                    <input type="hidden" name="_method" value="delete"/>
                    <button onclick=" return confirm('Czy na pewno chcesz usunąć zdjęcie?')" type="submit"
                            class="w3-button w3-red" name="userImageDelete"
                            style="line-height: 2vh;padding:0;width: 1.5vw;height: 2vh;">-
                    </button>
                </label>
            </form>
        </div>
        <div class="row col-6" style="margin-bottom: 10%;">
            <button onclick=" return confirm('Czy na pewno chcesz dodać zdjęcie?')" form="uploadAvatarForm"
                    type="submit" class="btn btn-success btn-sm" style="width:inherit;font-size: 0.5vw">Update
            </button>
        </div>
        <div style="background-color: #e4e4e5 ; height: 4vh; display: flex; align-items:center;"><p
                style="padding-left: 20px; margin-bottom: 0px; font-family: 'Poppins'" ;>ADMINISTRATION</p></div>
        <ul class="list-unstyled" style="margin-right: 13.5%;">
            <li class="sidebar-menu-items1"
                style="width: inherit;transform: scale(1.3);padding-left: 20%;margin-right: 13%;margin-top: 4%;">
                <a href="{{ route('home.index') }}"
                   class="menu_item1" id="li_item1" style="color:black;"><i class="fas fa-home"
                                                                            style="margin-right: 15px;"></i>
                    <p>Home</p></a>
            </li>
            <li class="sidebar-menu-items2"
                style="width: inherit;transform: scale(1.3);padding-left: 20%;margin-right: 13%;margin-top: 4%;">
                <a href="{{ route('articles.index') }}"
                   class="menu_item2" id="li_item2" style="color:black;"><i class="far fa-address-card"
                                                                            style="margin-right: 15px;"></i>
                    <p>Articles</p></a>
            </li>
            <li class="sidebar-menu-items3"
                style="width: inherit;transform: scale(1.3);padding-left: 20%;margin-right: 13%;margin-top: 4%;">
                <a href="{{ route('users.index') }}"
                   class="menu_item3" id="li_item3" style="color:black;"><i class="fas fa-users"
                                                                            style="margin-right: 12px;"></i>
                    <p>Users</p></a>

            </li>
            <li class="sidebar-menu-items4"
                style="width: inherit;transform: scale(1.3);padding-left: 20%;margin-right: 13%;margin-top: 4%;">
                <a href="{{ route('categories.index') }}"
                   class="menu_item4" id="li_item4" style="color:black;"><i class="fab fa-elementor"
                                                                            style="margin-right: 15px;"></i>
                    <p>Categories</p></a>
            </li>
        </ul>
        <button class="dropdown-btn alertsBtn" style="font-size: 1.4vw"><i class="fas fa-bell"
                                                                           style="padding-right: 75%;"></i>
            <p style="position: absolute;left: 4%;display: inline;">Alerts
                <i class="fa fa-caret-down"></i>
        </button>
        <ul class="list-unstyled dropdown-list" style="margin-right: 5%;">
            <li class="sidebar-menu-items5"
                style="width: inherit;transform: scale(1.1);padding-left: 20%;margin-right: 13%;margin-top: 4%;">
                <a href="{{ route('articles.index') }}"
                   class="menu_item5" id="li_item5" style="color:black;"><i class="far fa-address-card"
                                                                            style="margin-right: 15px;"></i>
                    <p>Notifications</p></a>
            </li>
            <li class="sidebar-menu-items6"
                style="width: inherit;transform: scale(1.1);padding-left: 20%;margin-right: 13%;margin-top: 4%;">
                <a href="{{ route('articles.index') }}"
                   class="menu_item6" id="li_item6" style="color:black;"><i class="far fa-comment-dots"
                                                                            style="margin-right: 15px;"></i>
                    <p>Comments</p></a>
            </li>
            <li class="sidebar-menu-items7"
                style="width: inherit;transform: scale(1.1);padding-left: 20%;margin-right: 13%;margin-top: 4%;">
                <a href="{{ route('articles.index') }}"
                   class="menu_item7" id="li_item7" style="color:black;"><i class="far fa-envelope-open"
                                                                            style="margin-right: 15px;"></i>
                    <p>Messages</p></a>
            </li>
        </ul>
    </div>

    <style>
        .dropdown-list {
            display: none;
        }

        .alertsBtn {
            padding: 0 !important;
            width: 100%;
            border: 0;
            font-family: Poppins;
            font-size: 1.2rem;
        }

        .alertsBtn:focus {
            outline: none !important;
        }

        .list-unstyled a > i {
            margin-right: 0 !important;
        }

        .list-unstyled p {
            font-family: Poppins;
            bottom: -13%;
            left: 40%;
            position: absolute;
        }

        a, u {
            text-decoration: none !important;
        }

        ul.list-unstyled li:hover {

            background-color: #e6e6e6;
        }

        ul.list-unstyled li {
            margin-right: 0 !important;
        }

    </style>

    <script>
        $(document).ready(function () { //mark current li element save and retrieve its changed css style after reload page until browser is open
            const liElements = $('ul.list-unstyled')[0].children;
            const liElementsDropdown = $('ul.dropdown-list')[0].children;

            if (sessionStorage.getItem('selectedLI')) {
                markLiElement(liElements);
                markLiElement(liElementsDropdown);
            }
            $("ul.list-unstyled").on('click', 'li', function () {
                const childNodes = $(this)[0].parentNode.children;
                const currentNode = $(this);
                Array.from(childNodes).forEach(function (element) {

                    if ($(currentNode)[0].className === $(element)[0].className) {
                        $(element).children('div').hide();
                        element.style.backgroundColor = 'lightgrey';
                        element.style.fontWeight = 'bold';
                        let toHTML = element.outerHTML;
                        sessionStorage.setItem("selectedLI", toHTML);
                    } else {
                        $(element).children('div').show();
                        element.style.backgroundColor = '';
                        element.style.fontWeight = '';
                    }
                });

            });

            if (localStorage.getItem('avatar')) {

                const content = getLocalStorageContentArray('avatar');
                var currentLoggedUser = "{{Auth::user()->name}}";
                Array.from(content).forEach(function (el) {
                    if (currentLoggedUser === $(el)[0].dataset.id) {
                        $('.customAvatar').html($(el).css({
                            'width': "100%",
                            'height': "7.5vh",
                            "left": "0",
                            "top": "0",
                            "line-height": "7.5vh",
                            "font-size": "2.0rem"
                        }));

                    }
                })

            }
        });

        function getLocalStorageContentArray(localStorageKey) {
            if (localStorage.getItem(localStorageKey)) {
                const element = localStorage.getItem(localStorageKey);
                const elementsWithoutCommas = element.split(",").join("");
                const htmlElement = $.parseHTML(elementsWithoutCommas);
                var elementsArray = Array.from(htmlElement);
            }
            return elementsArray;
        }

        function markLiElement(liElement) {
            let selectedLI = sessionStorage.getItem('selectedLI');
            let parsedLiElement = $.parseHTML(selectedLI);
            Array.from(liElement).forEach(function (element, index) {

                if ($(parsedLiElement)[0].className === $(element)[0].className)
                    $(this[index]).replaceWith(parsedLiElement);

            }, Array.from(liElement));
        }
    </script>

    <script>
        /* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
        var dropdown = document.getElementsByClassName("dropdown-btn");
        var i;

        for (i = 0; i < dropdown.length; i++) {
            dropdown[i].addEventListener("click", function () {
                this.classList.toggle("active");
                var dropdownContent = this.nextElementSibling;
                if (dropdownContent.style.display === "block") {
                    dropdownContent.style.display = "none";
                } else {
                    dropdownContent.style.display = "block";
                }
            });
        }
    </script>

@endif
