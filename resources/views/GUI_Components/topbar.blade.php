<div class="panelName" style="display: flex; align-items: center; justify-content: center; width: 12.5%;">
    <div>
        <h2 style="font-family: Poppins;font-size: 1.5vw">Lara Panel</h2>
    </div>
</div>
<div class=" navbarTop d-flex flex-row-reverse  colNavbar   align-self-center w3-light-grey"
     style="align-items: center; height: 7vh;width: 87.5%;padding-right: 2%;">
    <ul class="barLogin navbar-nav ml-auto">
        <!-- Authentication Links -->
        @guest
            <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>
            @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
            @endif
        @else
            <div>
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        @if(isset(Auth::user()->avatar))
                            <img src="{{ asset(Auth::user()->avatar) }}" alt="Avatar" class="avatar"
                                 style="width: 3vh;margin-right: 0.5vh;border-radius: 50%;">
                        @else
                            <div class="customAvatarTopbar"
                                 style=" position:absolute;top:0;left:-23%;width: 3vh;border-radius: 50%;"></div>
                        @endif
                        {{ Auth::user()->name }}
                    </a>

                    <div class="row" id="timer"></div>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
                            Wyloguj
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                              style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            </div>
        @endguest
    </ul>
</div>
@if(Request::url() && Auth::user())
    <div class=" notificationsBar d-flex flex-row-reverse  colNavbar col-12"
         style="align-items: center; height: 4vh;background-color: #D3D3D3;padding-right: 17%;left: 12.5%;">
        {{--        <form id="uploadAudio" action="{{route('users.uploadAvatar')}}" method="POST" role="form" enctype="multipart/form-data" style="display: inline;">--}}
        {{--            {{ csrf_field() }}--}}
        {{--            {{ method_field('POST') }}--}}
        {{--            <label for="audio">--}}
        {{--                <div class="w3-button w3-green"style="padding:0;width: 2.5vh">+</div>--}}
        {{--            </label>--}}
        {{--            <input id="audio" name="audio" type="file" style="display: none">--}}
        {{--        </form>--}}
        {{--        <button onclick=" return confirm('Czy na pewno chcesz dodać plik?')" form="uploadAudio" type="submit" class="btn btn-success btn-sm" style="width:5%;">Upload</button>--}}

        <div class="dropdownNotifications3">
            <div style="display: inline">

                <a class="nav-link messageNotification" href="#" data-toggle="dropdown"><i class=" far fa-envelope fa-lg"
                                                                       style="color: white;%"></i></a>
                <span id="unreadNotifications3" class="badge badge-pill badge-danger"
                      style="top: 1.8vh;position: absolute;transform: translateX(200%)">{{$sumOfUnreadNotifications}}</span>
            </div>
            <ul class="dropdown-menu"
                style="max-height: 24vh;overflow-x: hidden;height: auto; -webkit-appearance: none;padding-top: 0;">
                <div class="card-header"><p
                        style="display:flex;justify-content:center;font-family:poppins;text-decoration: underline;font-size: 0.5vw !important;">
                        Messages:</p></div>
                @foreach(Auth::user()->notifications as $index=>$notification)
                    <li class="dropdown-header" id="dropdownNotification3" data-id="{{$notification->id}}">
                        <table class="tab">
                            <tr>
                                <td><img src="{{ asset('/images/System_Notification_icon.PNG') }}" alt="Avatar"
                                         class="systemNotificationIcon"></td>
                                <td><a class="alertID" style="word-break: break-all;width: 100%;white-space: normal;"
                                       data-id="{{$notification->id}}">{!! $notification->data !!} </a></td>
                            </tr>
                            <tr>
                                <td style="width: 40px;height: auto"></td>
                                <td style="padding-top: 2%;">
                                    <button class="markAsReadNotification" data-id="{{$notification->id}}"><i
                                            class="fas fa-check"></i></button>
                                    <button class="deleteNotification" data-id="{{$notification->id}}"
                                            style="margin-left: 2%;"><i
                                            class="fas fa-trash-alt"></i></button>
                                    <button class="hideNotification" data-id="{{$notification->id}}"
                                            style="margin-left: 2%;"><i class="far fa-eye"></i></button>
                                </td>
                            </tr>
                        </table>
                        <p id="timeSinceGotNotification" style="padding-top: 3%;">
                            {{$timeSinceGotNotification[$index]}}
                        </p>
                        <div class="bottomBorderGradient">
                        </div>
                    </li>
                    <div style="height: 0.3vh;width: 100%;background-color: white;"></div>@endforeach
            </ul>
        </div>
        <div class="dropdownNotifications2">
            <div style="display: inline">

                <a class="nav-link" href="#" data-toggle="dropdown"><i class="far fa-comments fa-lg"
                                                                       style="color: white;"></i></a>
                <span id="unreadNotifications2" class="badge badge-pill badge-danger"
                      style="top: 1.8vh;position: absolute;transform: translateX(210%)">{{$sumOfUnreadNotifications}}</span>
            </div>
            <ul class="dropdown-menu"
                style="max-height: 24vh;overflow-x: hidden;height: auto; -webkit-appearance: none;padding-top: 0;">
                <div class="card-header"><p
                        style="display:flex;justify-content:center;font-family:poppins;text-decoration: underline;">
                        Messages:</p></div>
                @foreach(Auth::user()->notifications as $index=>$notification)
                    <li class="dropdown-header" id="dropdownNotification2" data-id="{{$notification->id}}">
                        <table class="tab">
                            <tr>
                                <td><img src="{{ asset('/images/System_Notification_icon.PNG') }}" alt="Avatar"
                                         class="systemNotificationIcon"></td>
                                <td><a class="commentsID" style="word-break: break-all;width: 100%;white-space: normal;"
                                       data-id="{{$notification->id}}">{!! $notification->data !!} </a></td>
                            </tr>
                            <tr>
                                <td style="width: 40px;height: auto"></td>
                                <td style="padding-top: 2%;">
                                    <button class="markAsReadNotification" data-id="{{$notification->id}}"><i
                                            class="fas fa-check"></i></button>
                                    <button class="deleteNotification" data-id="{{$notification->id}}"
                                            style="margin-left: 2%;"><i
                                            class="fas fa-trash-alt"></i></button>
                                    <button class="hideNotification" data-id="{{$notification->id}}"
                                            style="margin-left: 2%;"><i class="far fa-eye"></i></button>
                                </td>
                            </tr>
                        </table>
                        <p id="timeSinceGotNotification" style="padding-top: 3%;">
                            {{$timeSinceGotNotification[$index]}}
                        </p>
                        <div class="bottomBorderGradient">
                        </div>
                    </li>
                    <div style="height: 0.3vh;width: 100%;background-color: white;"></div>@endforeach
            </ul>
        </div>

        <div class="dropdownNotifications" style="display:inline">
            <a class="nav-link" href="#" data-toggle="dropdown"><i class="far fa-bell fa-lg" style="color: white;"></i>
                <span id="unreadNotifications" class="badge badge-pill badge-danger"
                      style="top: 1.8vh;position: absolute;transform: translateX(10%)">{{$sumOfUnreadNotifications}}</span>
            </a>

            <ul class="dropdown-menu" id="dropdown-menu_notifications"
                style="max-height: 24vh;overflow-x: hidden;height: auto; -webkit-appearance: none;padding-top: 0px;">
                <div class="card-header"><p
                        style="display:flex;justify-content:center;font-family:poppins;text-decoration: underline;">
                        Messages:</p></div>
                @foreach(Auth::user()->notifications as $index=>$notification)
                    <li class="dropdown-header" id="dropdownNotification" data-id="{{$notification->id}}">
                        <table class="tab">
                            <tr>
                                <td><img src="{{ asset('/images/System_Notification_icon.PNG') }}" alt="Avatar"
                                         class="systemNotificationIcon"></td>
                                <td><p class="messageID" style="word-break: break-all;width: 100%;white-space: normal">{!!$notification->data!!}</p></td>
                            </tr>
                            <tr>
                                <td style="width: 40px;height: auto"></td>
                                <td style="padding-top: 2%;">
                                    <button class="markAsReadNotification" data-id="{{$notification->id}}"><i
                                            class="fas fa-check"></i></button>
                                    <button class="deleteNotification" data-id="{{$notification->id}}"
                                            style="margin-left: 2%;"><i
                                            class="fas fa-trash-alt"></i></button>
                                    <button class="hideNotification" data-id="{{$notification->id}}"
                                            style="margin-left: 2%;"><i class="far fa-eye"></i></button>
                                </td>
                            </tr>
                        </table>
                        <p id="timeSinceGotNotification" style="padding-top: 3%;">
                            {{$timeSinceGotNotification[$index]}}
                        </p>
                        <div class="bottomBorderGradient">
                        </div>
                    </li>
                    <div style="height: 0.3vh;width: 100%;background-color: white;"></div>@endforeach
            </ul>
        </div>
    </div>

    <span class="notificationAlert" style="height: 4vh;left: 100%;margin-top: 0.15%;"></span>
@endif

<style>

    #dropdownNotification {

        font-size: 0.8vw;
    }

    #dropdown-menu_notifications.show {
        transform: translate(335%,20%) !important;
        width: 20%; !important;
        display: unset !important;
    }

    .bottomBorderGradient {
        width: 150%;
        height: 1.5px;
        background-image: linear-gradient(to right, rgb(248, 248, 248), rgb(211, 211, 211), rgb(248, 248, 248));

    }


    .dropdown li:hover {
        background-color: lightgray;
    }

    .dropdown-header {
        width: 100%;
        padding: 3%;
    }

    #dropdown-menu_notifications.show p {
        margin-bottom: 0.5rem !important;
    }


    #dropdown-menu_notifications.show li {

        display: inline-block !important;
    }

    .markAsReadNotification {
        background-color: #3490dc;
        border-radius: 30%;
        border: none !important;
        color: white;
        padding: 3px 6px;
        cursor: pointer;
    }

    .systemNotificationIcon {

        width: 40px;
        height: auto;
        border-color: #1d68a7;
        border-radius: 25%;
        border: none !important;
        color: white;
        padding: 3px 6px;
        cursor: pointer;
    }


    .deleteNotification {
        background-color: red;
        border-radius: 30%;
        border: none !important;
        color: white;
        padding: 3px 6.7px;
        cursor: pointer;
    }

    .hideNotification {
        background-color: grey;
        border-radius: 30%;
        border: none !important;
        color: white;
        padding: 3px 5.5px;
        cursor: pointer;
    }

    .notificationsStyle p {
        -webkit-box-decoration-break: clone;
        box-decoration-break: clone;
    }

</style>

<script>

    $(document).ready(function () {
        var outOfDuplicatesVisibleElementsArray = [];
        var outOfDuplicatesHiddenElementsArray = [];
        var outOfDuplicatesLiElementsArray = [];
        var outOfDuplicatesLiElementsTemporaryArray = [];
        var liElementsArray = [];
        var arrayOfVisibleLiElements = [];
        var arrayOfHiddenLiElements = [];
        var liElementsCopy = $('li#dropdownNotification').clone(true, true);
        var liElements = $('li#dropdownNotification');
        var tempArr = [];

        if (localStorage.getItem('liElement')) {
            var parsedMarkedLiElements = getLocalStorageContentArray('liElement');
            const currentTimeNotification = JSON.stringify("@if(Auth::user()){{ json_encode(($timeSinceGotNotification))}} @endif").replace(/"/g, "").replace(/[[\]]/g, '').replace(/&quot;/g, "");
            const currentNotificationTimeArray = currentTimeNotification.split(',');

            for (var i = 0; i < liElements.length; i++) {

                const notificationID = $(liElements[i]).data('id');
                for (var j = 0; j < parsedMarkedLiElements.length; j++) {
                    if (notificationID === parsedMarkedLiElements[j].dataset.id) { //find marked elements and update their state
                        $(parsedMarkedLiElements[j].children[1])[0].textContent = currentNotificationTimeArray[i];
                        $(parsedMarkedLiElements[j]).height($(parsedMarkedLiElements[j]).parent().height());
                        $(parsedMarkedLiElements[j]).width($(parsedMarkedLiElements[j]).parent().width());
                        i === 0 ? $(liElements[i]).attr('style', 'padding: 0 !important;') : $(liElements[i]).attr('style', 'padding: 0 !important;');
                        $(liElements[i]).html(parsedMarkedLiElements[j]);
                        liElementsArray.push($(parsedMarkedLiElements[j])[0].outerHTML);    //push another element to existing storage prevent against overwriting existing element
                        localStorage.setItem('liElement', liElementsArray);
                    }
                }

            }
        }

        if (localStorage.getItem('hiddenLiElements')) {

            var parsedHiddenElements = getLocalStorageContentArray('hiddenLiElements');

            for (var k = 0; k < liElements.length; k++) {

                const notificationID = $(liElements[k]).data('id');
                for (var l = 0; l < parsedHiddenElements.length; l++) {
                    if (notificationID === $(parsedHiddenElements[l])[0].children[1].children[0].dataset.id) {
                        //find marked elements and update their state
                        k === 0 ? $(liElements[k]).attr('style', 'padding: 0 !important;') : $(liElements[k]).attr('style', 'padding: 0 !important;');
                        $(liElements[k]).html(parsedHiddenElements[l]);
                        arrayOfHiddenLiElements.push($(parsedHiddenElements[l])[0].outerHTML);    //push another element to existing storage prevent against overwriting existing element
                    }
                }
            }
        }

        if (localStorage.getItem('visibleLiElements')) {
            const data = getLocalStorageContentArray('visibleLiElements');

            for (var m = 0; m < liElements.length; m++) {
                for (var n = 0; n < data.length; n++) {
                    const notificationID = $(data[n])[0].dataset.id;

                    if ($(liElements[m])[0].dataset.id === notificationID) {

                        const visibleLiElement = $(data[n])[0];
                        const visibleLiElementHTML = visibleLiElement.outerHTML;
                        preventDuplicates(outOfDuplicatesVisibleElementsArray, 'visibleLiElements', notificationID, visibleLiElementHTML, arrayOfVisibleLiElements);
                    }
                }
            }
        }

        @if(Auth::check())
        var countDownDate = new Date();
        var add15MinutesForward = countDownDate.setMinutes(countDownDate.getMinutes() + 15);

        // Update the count down every 1 second
        var x = setInterval(function () {
            document.getElementById("statusDescription").innerHTML = "Online";
            var now = new Date().getTime();
            var distance = add15MinutesForward - now;
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            document.getElementById("timer").style.fontSize = "small";
            document.getElementById("timer").innerHTML = 'Sesja wygaśnie za :' + " " +
                +minutes + "m " + seconds + "s ";

            if (distance < 0) {
                document.getElementById("timer").innerHTML = "Sesja wygasła. Zaloguj się ponownie.";

                clearInterval(x);
            }

        }, 1000);

        $.ajax(
            {
                url: '{{route('n.greet')}}',
                type: 'post',
                data: {"_token": "{{csrf_token()}}"},
                success: function () {

                    @if(!empty($notificationAlert))

                    $('.notificationAlert').addClass("alert alert-success");
                    $('.notificationAlert').html('{{$notificationAlert}}');
                    $('.alert').animate({
                        "left": "-=12%",
                        "padding-right": "1%",
                        "padding-left": "1%"
                    }, 1500, function () {
                        var played = false;
                        const audio = new Audio('{{ asset('/swiftly.mp3') }}');
                        audio.muted = false;
                        document.body.addEventListener('mouseover', function () {
                            if (!played)
                                audio.play();
                            played = true;
                        });
                        countDownInSeconds($(this));
                    });
                    @endif
                }

            });


        @endif

        $('ul#dropdown-menu_notifications').on('click', '.markAsReadNotification', function () {
            const notificationID = $(this).attr('data-id');

            for (var i = 0; i < liElements.length; i++) {

                const liElement = $(liElements[i])[0];
                const copiedLiElement = $(liElementsCopy[i])[0];
                if (notificationID === $(liElements[i])[0].dataset.id) {
                    //reorganizacja drzewa dom po wywolaniu eventu ukrycia elementu a nastepnie oznaczeniu jako przeczytane
                    if (liElement.childNodes.length === 1) { //jesli byl wywolywany event ukrycia elementu
                        const currTab = liElement.children[0].children[0]; //odwolaj sie do biezacej tabeli
                        const unwrappedData = $(currTab).unwrap(); // wyciagnij tabele z jej rodzica
                        const unwrappedDataHTML = $.parseHTML(unwrappedData); //sparsuj do htmla
                        liElement.append(unwrappedDataHTML);
                        liElement.style.padding = "";

                        //wyciagnieta tabele dodaj do nadrzednego elementu root <li> tym samym przeorganizowujac drzewo do postaci <li><table></table></li> z postaci <li><li><table></table></li></li>
                    }
                    liElement.style.transition = "background 2.0s linear 0s";
                    liElement.style.background = '#E8E8E8';
                    $(liElement).css('background-color', '#E8E8E8');
                    copiedLiElement.style.transition = "background 2.0s linear 0s";
                    copiedLiElement.style.background = '#E8E8E8';
                    $(copiedLiElement).css('background-color', '#E8E8E8');
                    var parsedHTMLLiElement = liElement.outerHTML;

                }
            }

            if (!localStorage.getItem('liElement'))
                setLocalStorage(liElementsArray, parsedHTMLLiElement, outOfDuplicatesLiElementsArray, notificationID, 'liElement');
            else {
                setWithoutDuplicates(outOfDuplicatesLiElementsArray, 'liElement', notificationID, parsedHTMLLiElement, liElementsArray);
            }
            if (!localStorage.getItem('liElementTemporary'))
                setLocalStorage(tempArr, parsedHTMLLiElement, outOfDuplicatesLiElementsTemporaryArray, notificationID, 'liElementTemporary');
            else {
                setWithoutDuplicates(outOfDuplicatesLiElementsTemporaryArray, 'liElementTemporary', notificationID, parsedHTMLLiElement, tempArr);
            }

            $.ajax(
                {
                    url: '{{route('n.greet')}}',
                    type: 'post',
                    data: {
                        "notificationID": notificationID,
                        "_token": "{{csrf_token()}}"
                    },
                    success: function (response) {

                        var sumOfUnreadNotifications = response.SumOfUnreadNotifications;

                        $('#unreadNotifications').html(sumOfUnreadNotifications);

                    },

                });

        });

        $('li#dropdownNotification').on('click', '.deleteNotification', function () {

            const idx = $(this).attr('data-id');

            if (confirm("Na pewno chcesz usunąć ?")) {
                $.ajax(
                    {
                        url: '{{route('home.deleteNotification')}}',
                        type: 'DELETE',

                        data: {
                            "id": idx,
                            "_token": "{{csrf_token()}}"
                        },

                        success: function (response) {

                            const deletedID = response.deletedNotification;

                            for (var i = 0; i < liElements.length; i++) {

                                if ($(liElements[i])[0].dataset.id === deletedID) {

                                    $(liElements[i]).slideUp(1000, function () {
                                        $(this).remove();
                                    });
                                }
                                if (localStorage.getItem('liElement'))
                                    removeLocalStorageContent(deletedID);
                                if (localStorage.getItem('hiddenLiElements'))//do poprawy nie wiem czemu nie przekazuje obu kluczy tylko pojedynczo i niepotrzebnie wywoluje w przypadku gdy nie
                                    removeLocalStorageContent(deletedID);

                            }

                            var sumOfUnreadNotifications = response.sumOfUnreadNotifications;
                            $('#unreadNotifications').html(sumOfUnreadNotifications);

                        },

                    });
            }

            return false;
        });

        $('ul#dropdown-menu_notifications').on('click', '.hideNotification', function () {
            if ($(this).parent().get(0).tagName === "TD") { //preventing of double indiciating this event while ".hiddenNotification" event is clicked.
                const notificationID = $(this).attr('data-id');
                const parsedTemporaryLiElements = getLocalStorageContentArray('liElementTemporary');
                for (var i = 0; i < liElementsCopy.length; i++) {

                    if ($(liElementsCopy[i])[0].dataset.id === notificationID) {
                        const visibleLiElement = $(liElementsCopy[i])[0];
                        const visibleLiElementHTML = visibleLiElement.outerHTML;

                        if (!localStorage.getItem('visibleLiElements')) {
                            setLocalStorage(arrayOfVisibleLiElements, visibleLiElementHTML, outOfDuplicatesVisibleElementsArray, notificationID, 'visibleLiElements');
                        } else { //set items if in localstorage is at least 1 element
                            setWithoutDuplicates(outOfDuplicatesVisibleElementsArray, 'visibleLiElements', notificationID, visibleLiElementHTML, arrayOfVisibleLiElements);
                        }
                        $(liElements[i]).attr('style', 'padding:0 !important; ' + 'padding-top:0 !important;' + 'padding-bottom: 0 !important');
                        $(liElements[i]).html($(liElementsCopy[i]));
                        $(liElementsCopy[i]).animate({
                            'padding': '0',
                        }, 3000).empty().prepend(
                            "<div style='display:flex;justify-items: center;align-items: center;height:3.5vh;'>" + "<p id='new' style='position:absolute;margin-left: 10%;'>ROZWIŃ</p></div>");
                        const node = $(liElementsCopy[i])[0].childNodes;
                        $(node).append("<div class='hideIcon' style='margin-left: 80%;'></div>");
                        const data = $(node)[0].children[1];
                        $(data).attr('data-id', notificationID);
                        $(data).html(this).removeClass('hideNotification').addClass('hiddenNotification');

                        $(liElementsCopy[i])[0].children[0].style.transition = "background 1.0s linear 0s";

                        if (i % 2 === 0) {
                            $(liElementsCopy[i])[0].children[0].style.backgroundColor = '#E8E8E8';
                        } else {
                            $(liElementsCopy[i])[0].children[0].style.backgroundColor = '#F5F5F5';
                        }

                        const hiddenElementHTML = $(liElementsCopy[i])[0].children[0].outerHTML;

                        if (!localStorage.getItem('hiddenLiElements')) {
                            setLocalStorage(arrayOfHiddenLiElements, hiddenElementHTML, outOfDuplicatesHiddenElementsArray, notificationID, 'hiddenLiElements');
                        } else { //bug dodaje powielony element po zakryciu odkryciu i kolejnym zakryciu i odkryciu parzystego elementu - moze wyczyscic tablice duplikatow po ponownej zmianie stanu

                            setWithoutDuplicates(outOfDuplicatesHiddenElementsArray, 'hiddenLiElements', notificationID, hiddenElementHTML, arrayOfHiddenLiElements);
                        }
                    }

                    if (parsedTemporaryLiElements) {
                        for (var j = 0; j < parsedTemporaryLiElements.length; j++) {

                            if ($(parsedTemporaryLiElements[j])[0].dataset.id === $(liElementsCopy[i])[0].dataset.id) {
                                removeItem(parsedTemporaryLiElements, j, 'liElementTemporary');
                            }
                        }
                    }

                }
            }
        });


        $('ul#dropdown-menu_notifications').on('click', '.hiddenNotification', function () {
            const hideIconID = $(this).attr('data-id');
            const parsedLiElements = getLocalStorageContentArray('liElement');
            const parsedVisibleLiElements = getLocalStorageContentArray('visibleLiElements');
            const parsedHiddenLiElements = getLocalStorageContentArray('hiddenLiElements');

            for (var i = 0; i < liElements.length; i++) {

                if (hideIconID === $(liElements[i])[0].dataset.id) {

                    parsedVisibleLiElements.forEach(function (element, index) {
                        if (parsedLiElements) {
                            for (var k = 0; k < parsedLiElements.length; k++) {
                                if ($(parsedLiElements[k])[0].dataset.id === $(parsedVisibleLiElements[index])[0].dataset.id) {
                                    if (hideIconID === $(parsedLiElements[k])[0].dataset.id) {
                                        $(liElements[i]).attr('style', 'padding: 0 !important;', 'padding-bottom:0 !important;', 'padding-top:0 !important;');
                                        $(liElements[i]).html($(parsedVisibleLiElements[0]));
                                    }
                                }
                            }
                        }
                        if (hideIconID === $(parsedVisibleLiElements[index])[0].dataset.id) {
                            $(liElements[i]).attr('style', 'padding: 0 !important;', 'padding-bottom:0 !important;', 'padding-top:0 !important;');
                            $(liElements[i]).html($(parsedVisibleLiElements[index]));

                            removeItem(parsedHiddenLiElements, this[index], 'hiddenLiElements'); //'this' points to a current clicked object (current clicked <li> in <ul>) in array on which foreach method was indicated

                        }
                    }, parsedVisibleLiElements);

                }
            }
        });


        $('.commentID').on('click', function () {

            var idx = $(this).attr('data-id2');
            console.log(idx);
            {{--if (confirm("Na pewno chcesz usunąć ?")) {--}}
            {{--    $.ajax(--}}
            {{--        {--}}
            {{--            url: 'categories/' + idx,--}}
            {{--            type: 'DELETE',--}}

            {{--            data: {--}}

            {{--                "_token": "{{csrf_token()}}"--}}
            {{--            },--}}

            {{--            success: function () {--}}
            {{--                table.ajax.reload();--}}
            {{--                alert("Pomyślnie usunięto kategorie.")--}}
            {{--            },--}}

            {{--        });--}}
            {{--}--}}

            {{--return false;--}}
        });

        $('.alertID').on('click', function () {

            var idx = $(this).attr('data-id3');

            {{--if (confirm("Na pewno chcesz usunąć ?")) {--}}
            {{--    $.ajax(--}}
            {{--        {--}}
            {{--            url: 'categories/' + idx,--}}
            {{--            type: 'DELETE',--}}

            {{--            data: {--}}

            {{--                "_token": "{{csrf_token()}}"--}}
            {{--            },--}}

            {{--            success: function () {--}}
            {{--                table.ajax.reload();--}}
            {{--                alert("Pomyślnie usunięto kategorie.")--}}
            {{--            },--}}

            {{--        });--}}
            {{--}--}}

            {{--return false;--}}
        });

        $('#dropdown-menu_notifications').on('click',function(e){

            e.stopPropagation();
        })



        if (localStorage.getItem('avatar')) {

            const content = getLocalStorageContentArray('avatar');
            var currentLoggedUser = "{{Auth::user()->name}}";
            Array.from(content).forEach(function (el) {
                if (currentLoggedUser === $(el)[0].dataset.id) {
                    $('.customAvatarTopbar').html($(el).css({
                        'width': "120%",
                        'left': "0",
                        'top': "0",
                        'height': "3.7vh",
                        "line-height": "3.2vh",
                        "font-size": "1.0rem"
                    }));

                }
            })

        }


        function setWithoutDuplicates(outOfDuplicatesItemArray, localStorageKey, notificationID, htmlElement, localStorageArray) {

            if (!outOfDuplicatesItemArray.includes(notificationID)) {
                outOfDuplicatesItemArray.push(notificationID); //pushuj biezacy element jesli nie zostal juz wcesniej dodany
                localStorageArray.push(htmlElement);
                localStorage.setItem(localStorageKey, localStorageArray)
            }
        }

        function preventDuplicates(outOfDuplicatesItemArray, localStorageKey, notificationID, htmlElement, localStorageArray) { //id to ostatni dodany item

            if (!outOfDuplicatesItemArray.includes(notificationID)) {
                outOfDuplicatesItemArray.push(notificationID); //pushuj biezacy element jesli nie zostal juz wcesniej dodany
                localStorageArray.push(htmlElement);
            }
        }

        function setLocalStorage(localStorageArray, item, outOfDuplicatesItemArray, notificationID, localStorageKey) {

            localStorageArray.length = 0;
            outOfDuplicatesItemArray.length = 0; // prevent of getting previous element from array which was removed before set another one - clear array while last value is already removed
            localStorageArray.push(item);
            outOfDuplicatesItemArray.push(notificationID); //pomocnicza tablica do eliminacji duplikatow, przetrzymuje wszystkie klikniete id i porownuje je z kazdym nastepnym w celu eliminacji duplikatow
            localStorage.setItem(localStorageKey, localStorageArray);

        }

        function removeItem(itemArray, removeItemIndex, localStorageKey) {
            if (itemArray.length > 1) {
                const tempArray = [];

                itemArray.splice(removeItemIndex, 1); //usun element
                itemArray.slice(0, itemArray.length); //zaznacz wartosci z tablicy po usunieciu  elementu tworzac nowa tablice z nowymi wartosciami
                for (var i = 0; i < itemArray.length; i++) {

                    const item = itemArray[i].outerHTML;
                    tempArray.push(item);
                }
                tempArray.join(',');
                localStorage.setItem(localStorageKey, tempArray);

            } else {
                localStorage.removeItem(localStorageKey);
            }

        }

        function getLocalStorageContentArray(localStorageKey) {
            if (localStorage.getItem(localStorageKey)) {
                const element = localStorage.getItem(localStorageKey);
                const elementsWithoutCommas = element.split(",").join("");
                const htmlElement = $.parseHTML(elementsWithoutCommas);
                var elementsArray = Array.from(htmlElement);
            }
            return elementsArray;
        }

        function removeLocalStorageContent(deletedID) { //delete  content from selected local storage key
            for (var i = 0; i < localStorage.length; i++) {

                const key = localStorage.key(i);

                if (localStorage.getItem(key)) {

                    const el = getLocalStorageContentArray(key);

                    for (var j = 0; j < el.length; j++) {

                        if (el[j].dataset.id === deletedID) {

                            removeItem(el, j, key);

                        }
                    }
                }
            }
        }

        function countDownInSeconds(obj) {

            var countDownDate = new Date();
            var add15SecondsForward = countDownDate.setSeconds(countDownDate.getSeconds() + 15);

            // Update the count down every 1 second
            var x = setInterval(function () {
                var now = new Date().getTime();
                var distance = add15SecondsForward - now;
                $(obj).fadeToggle(1000);
                if (distance < 0) {
                    $(obj).remove();

                    clearInterval(x);
                }

            }, 1000);
        }

    });

</script>

