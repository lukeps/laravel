@extends('layouts.app')
@section('content')
    @role('admin')
    <link href="{{ URL::asset('/css/admin_panel_buttons_styles.css') }}" rel="stylesheet" type="text/css">
    <link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css" rel="stylesheet">
    @include('users.user_stats')
    <div class="container"style="height: 2vh;"></div>
    <div class="col-md-12"style="margin-top: 2%;">
        <div class="card">
            <div class="card-header">Lista użytkowników
                <a href="{{route('users.create')}}" class=" addUserLink btn btn-primary float-right"
                   style="background-color: mediumseagreen;margin-left: 5px; box-shadow: #2a9055">Dodaj Użytkownika</a>
            </div>
            @if(session('status_msg'))
                <div class="alert alert-success">
                    <p>{{session('status_msg')}}</p>
                </div>
            @endif
            <div class="card-body">
                <div class="table-responsive table-hover">
                    <table class="table table-striped table-bordered dt-responsive nowrap ajaxTableUsers"
                           style="margin: 0px auto">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Avatar</th>
                            <th>Imię</th>
                            <th>E-mail</th>
                            <th>Prawa administratora</th>
                            <th>Ban user</th>
                            <th style="width:10%">Wybierz</th>
                        </tr>
                        </thead>
                        <thead>
                        <td><input class="form-control" id="column0_search" type="text" placeholder="Szukaj...">
                        </td>
                        <td>
                        </td>
                        <td><input class="form-control" id="column1_search" type="text" placeholder="Szukaj...">
                        </td>
                        <td><input class="form-control" id="column2_search" type="text" placeholder="Szukaj...">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        </thead>
                    </table>
                    <div class="container">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <style>

        .mt-3 {
            margin-top: 0.2rem !important;
        }
    </style>
    @endrole
@endsection
@push('js')
    <script type="text/javascript" src="{{ URL::asset('/js/datatables.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('/js/dataTables.bootstrap4.js') }}"></script>
    <script type="text/javascript" src="{{URL::asset('/js/customAvatar.js')}}"></script>

    <script>

        $(document).ready(function () {

            var localStorageArr = [];
            var localStorageArrWithoutDuplicates = [];
            let parsedAvatarHTML = getLocalStorageContentArray('avatar');
            if (localStorage.getItem('avatar')) {
                getLocalStorageDataName('avatar');
                Array.from(parsedAvatarHTML).forEach(function (element) {
                    localStorageArr.push(element.outerHTML); //prevent against overwriting existing data in refresh
                    localStorage.setItem('avatar', localStorageArr);
                });

            }
            var table = $('.ajaxTableUsers').DataTable({

                "sDom": "ltipr",
                "fnDrawCallback": function () {
                    const data = ($('.name'));
                    setStyleForLastTableColumn($(this)[0].tBodies[0].children);
                    Array.from(data).forEach(function (element, index) {
                        const foundElementIndex = localStorageArrWithoutDuplicates.indexOf($(data)[index].dataset.id); //pozwala na odpowiednie rozmieszczenie danych elementow
                        if (!localStorage.getItem('avatar')) {
                            $(element).html($(data[index])[0].attributes[1].textContent);
                            $(element).nameBadge();
                            $(element).addClass('styled');
                            const toHTML = element.outerHTML;
                            const dataName = ($(element)[0].dataset.id);
                            setLocalStorage(localStorageArr, toHTML, localStorageArrWithoutDuplicates, dataName, 'avatar');

                        }

                        if (parsedAvatarHTML) {

                            if (!$(parsedAvatarHTML[foundElementIndex]).hasClass('styled')) {

                                $(element).promise().then(function () {//promise, queueing of function execution in right order
                                    $(this).addClass('styled');
                                    $(this).html($(data[index])[0].dataset.id);
                                    $(this).nameBadge();
                                    const toHTML = element.outerHTML;
                                    const dataName = ($(this)[0].dataset.id);
                                    setWithoutDuplicates(localStorageArrWithoutDuplicates, 'avatar', dataName, toHTML, localStorageArr);
                                });


                            }
                            if (parsedAvatarHTML.length === 1)
                                $(data[0]).html($(parsedAvatarHTML[0]));

                            if (parsedAvatarHTML.length > 1) {

                                $(data[index]).html($(parsedAvatarHTML[foundElementIndex]));

                                Array.from($('.customAvatar')).forEach(function (el) {
                                    if ($(el)[0].children[0].dataset.id === $(data[index])[0].dataset.id) {
                                        $('.customAvatar').html($(parsedAvatarHTML[foundElementIndex]).clone().css({
                                            'width': "100%",
                                            'height': "7.5vh",
                                            "left": "0",
                                            "top": "0",
                                            "line-height": "7.5vh",
                                            "font-size": "2.0rem"
                                        }));
                                    }
                                });
                                Array.from($('.customAvatarTopbar')).forEach(function (el) {
                                    if ($(el)[0].children[0].dataset.id === $(data[index])[0].dataset.id) {
                                        $('.customAvatarTopbar').html($(parsedAvatarHTML[foundElementIndex]).clone().css({
                                            'width': "120%",
                                            'left': "0",
                                            'top': "0",
                                            'height': "3.7vh",
                                            "line-height": "3.2vh",
                                            "font-size": "1.0rem"
                                        }));

                                    }
                                });
                            }
                        }
                    });
                },

                responsive: true,
                serverSide: true,
                select: true,
                paging: true,
                pageLength: 5,
                lengthMenu: [5],
                stateSave: false,
                searching: true,
                processing: true,
                ajax: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    url: "{{route('users.jsonTable')}}",

                },
                data: {

                    "_token": "{{ csrf_token() }}"
                },
                columns: [

                    {
                        data: 'u_id',
                        width: '6%'
                    },
                    {
                        data: 'u_avatar',
                        width: '6%',
                        sortable: false,
                        searchable: false,

                        render: function (data, type, row) {

                            if (row['u_avatar']) {
                                return '<img src= "' + "http://laravel.test/" + row['u_avatar'] + ' " alt="Avatar" class="avatar"style="position: relative;left: 12%;top: 0.2vh;width: 75%;border-radius: 50%;">';
                            } else {
                                return '<div class = "name" data-id = "' + row['u_name'] + '"></div>';

                            }
                        },
                    },
                    {data: 'u_name'},
                    {data: 'u_email'},

                    {
                        width: '10%',
                        sortable: false,
                        searchable: false,

                        render: function (data, type, row) {

                            return '<form id="adminPermissionsForm" method="POST" action="users/' + row['u_id'] + '"><input class="permissionInput"  data-id = "' + row['u_id'] + ' " type="checkbox" name="userPermissions" value="isAdmin" style="margin:auto;">' + '<div class="container mt-3 "></div></form>' +
                                '<div style="display: flex"><button type="button" data-id = "' + row['u_id'] + ' " + data-id1 = "' + row['u_name'] + ' " class="givePermissions btn btn-sm btn-success" style="margin-right: 0.5vw;flex:1 1 0px" >Dodaj</button>' +
                                '<button type="button" data-id = "' + row['u_id'] + ' "  data-id1 = "' + row['u_name'] + ' " class="revokePermissions btn btn-sm btn-danger" style="flex:1 1 0px"" >Usuń</button></div>';

                        },

                        error: () => {
                        },

                    },
                    {
                        width: '5%',
                        sortable: false,
                        searchable: false,

                        render: function (data, type, row) {

                            return '<form id="banUserForm" method="POST" action="{{route('users.banOrUnbanUser')}}" data-id = "' + row['u_id'] + '"> @csrf  <div class="form-check form-check-inline"><input class="banInput" type="radio"  name="banOrUnbanUserID" id="banUserID" value= ' + row['u_id'] + '  style="margin:auto;"/>' +
                                '<label class="form-check-label" for="banUser">Ban</label></div><div class="form-check form-check-inline mr-0"><input class="unbanInput" type="radio" name="banOrUnbanUserID" id="unbanUserID" value=' + row['u_id'] + ' style="margin:auto;"/>' +
                                '<label class="form-check-label" for="unbanUser">Unban</label></div><div class="container mt-3 "></div>' +
                                '<div style="display: flex;align-items: center;justify-content: center;"><button type="submit" data-id = "' + row['u_id'] + ' " +  class="banOrUnbanUser btn btn-sm btn-primary" style="color: white;" >Confirm</button></div></form>';

                        },

                        error: (error) => {
                            console.log(JSON.stringify(error));
                        },

                    },

                    {
                        width: '10%',
                        sortable: false,
                        searchable: false,

                        render: function (data, type, row) {

                            return '<a href="/admin/users/' + row['u_id'] + '/edit" class="editArticle btn btn-sm btn-primary mt-1" style="margin-top:0 !important;padding-left: 5%;padding-right: 5%;flex: 1 1 0px;margin-right: 5%;">Edycja</a>' +
                                '<button style= " flex: 1 1 0px;margin-top:0 !important;" name="deleteButton" id="DeleteButton" type="button" data-id = "' + row['u_id'] + '" data-id2 = ' + row['u_name'] + ' class="deleteUser btn btn-sm btn-danger mt-1">Usuń</button>';

                        },

                        error: (error) => {
                            console.log(JSON.stringify(error));
                        },

                    },

                ],
                order: [

                    [0, 'asc'],
                    [2, 'asc'],
                    [3, 'asc']

                ],

            });

            table.on('click', '.givePermissions', function () {
                let givePermissionButtonId = $(this).attr('data-id');
                let username = $(this).attr('data-id1');
                let checkbox = document.getElementsByClassName("permissionInput");
                const arr = Array.from(checkbox);
                for (let inputElement of arr) {
                    console.log($(inputElement)[0].checked);
                    if ($(inputElement)[0].dataset.id === givePermissionButtonId) {
                        if ($(inputElement)[0].checked) {

                            if (confirm("Na pewno chcesz nadać prawa administratora użytkownikowi" + " " + username + '?')) {

                                $.ajax(
                                    {
                                        url: 'users/' + givePermissionButtonId,
                                        type: 'POST',

                                        data: {

                                            "_token": "{{csrf_token()}}",
                                            "permissionsCheckbox": checkbox[0].checked,

                                        },

                                        success: function () {
                                            table.ajax.reload();
                                            alert("Pomyślnie nadano prawa administratora użytkownikowi" + " " + username + ".")
                                        },

                                    });
                            }

                            break;
                        } else {
                            alert("Aby przejść dalej zaznacz pole wyboru.");
                            break;
                        }

                    }

                }
            });

            table.on('click', '.revokePermissions', function () {

                let checkbox = document.getElementsByClassName("permissionInput");
                let deleteButtonId = $(this).attr('data-id');
                let username = $(this).attr('data-id1');
                const arr = Array.from(checkbox);
                for (let inputElement of arr) {
                    if ($(inputElement)[0].dataset.id === deleteButtonId) {
                        if ($(inputElement)[0].checked) {
                            if (confirm("Czy na pewno chcesz usunąć prawa administratora użytkownikowi" + " " + username + '?')) {

                                $.ajax(
                                    {
                                        url: 'users1/' + deleteButtonId,
                                        type: 'DELETE',

                                        data: {

                                            "_token": "{{csrf_token()}}",
                                            "permissionsCheckbox": checkbox[0].checked,

                                        },

                                        success: function () {
                                            table.ajax.reload();
                                            alert("Pomyślnie usunięto prawa administratora użytkownikowi" + " " + username + ".")
                                        },

                                        error: function (response) {

                                            alert(response.responseJSON.error);
                                        }

                                    });
                            }
                            break;
                        } else {
                            alert("Aby przejść dalej zaznacz pole wyboru.");
                            break;
                        }

                    }
                }
            });


            table.on('click', '#banUserForm', function (e) {

                console.log($(this));
                const formElements = Array.from($(this)[0]);
                const banInput = $(formElements[1]);
                const unbanInput = $(formElements[2]);
                const submitButton = $(formElements[3]);

                if (!$(banInput)[0].checked && !$(unbanInput)[0].checked) {
                    e.preventDefault();
                } else {

                    $(submitButton).on('click', function () {

                        if ($(banInput)[0].checked) {
                            const banInputKey = $(banInput)[0].attributes[2].value = "ban"; //have to change radio input name because of retrieve the specific value in serverside to detect and in the result to change user status
                            $(banInput).html(banInputKey);
                        } else if ($(unbanInput)[0].checked) {

                            const unbanInputKey = $(unbanInput)[0].attributes[2].value = "unban";
                            $(unbanInput).html(unbanInputKey);
                        }
                    });
                }
            });

            $('.ajaxTableUsers').on('click', '.deleteUser', function () {

                let idx = $(this).attr('data-id');
                let dataName = $(this).attr('data-id2');
                let parsedHTMLContent = getLocalStorageContentArray('avatar');


                if (confirm("Na pewno chcesz usunąć ?")) {
                    $.ajax(
                        {
                            url: 'users/' + idx,
                            type: 'DELETE',

                            data: {

                                "_token": "{{csrf_token()}}"
                            },

                            success: function () {

                                table.ajax.reload();

                                if (parsedHTMLContent) {
                                    parsedHTMLContent.forEach(function (element, index) {

                                        if ($(element)[0].dataset.id === dataName) {

                                            removeItem(parsedHTMLContent, index, 'avatar');
                                        }
                                    });
                                }
                                alert("Pomyślnie usunięto użytkownika.")
                            },

                        });
                }

                return false;
            });

            $('#column0_search').on('keyup', function () {

                let inputValue = $('#column0_search').val();
                table.columns(0).search(inputValue).draw();

            });

            $('#column1_search').on('keyup', function () {

                let inputValue = $('#column1_search').val();
                table.columns(2).search(inputValue).draw();

            });

            $('#column2_search').on('keyup', function () {

                let inputValue = $('#column2_search').val();
                table.columns(3).search(inputValue).draw();

            });

            $('.caret').remove();

            function setWithoutDuplicates(outOfDuplicatesItemArray, localStorageKey, dataName, htmlElement, localStorageArray) {

                if (!outOfDuplicatesItemArray.includes(dataName)) {
                    outOfDuplicatesItemArray.push(dataName);
                    localStorageArray.push(htmlElement);
                    localStorage.setItem(localStorageKey, localStorageArray)
                }
            }


            function setLocalStorage(localStorageArray, htmlElement, outOfDuplicatesItemArray, dataName, localStorageKey) {

                localStorageArray.length = 0;
                outOfDuplicatesItemArray.length = 0; // prevent of getting previous element from array which was removed before set another one - clear array while last value is already removed
                localStorageArray.push(htmlElement);
                outOfDuplicatesItemArray.push(dataName); //pomocnicza tablica do eliminacji duplikatow, przetrzymuje wszystkie klikniete id i porownuje je z kazdym nastepnym w celu eliminacji duplikatow
                localStorage.setItem(localStorageKey, localStorageArray);

            }

            function getLocalStorageContentArray(localStorageKey) {
                if (localStorage.getItem(localStorageKey)) {
                    const element = localStorage.getItem(localStorageKey);
                    const elementsWithoutCommas = element.split(",").join("");
                    const htmlElement = $.parseHTML(elementsWithoutCommas);
                    var elementsArray = Array.from(htmlElement);
                }
                return elementsArray;
            }


            function getLocalStorageDataName(localStorageKey) {
                if (localStorage.getItem(localStorageKey)) {
                    const element = localStorage.getItem(localStorageKey);
                    const elementsWithoutCommas = element.split(",").join("");
                    const htmlElement = $.parseHTML(elementsWithoutCommas);
                    var elementsArray = Array.from(htmlElement);

                    elementsArray.forEach(function (element) {

                        localStorageArrWithoutDuplicates.push($(element)[0].dataset.id);
                    });

                }

            }

            function removeItem(itemArray, removeItemIndex, localStorageKey) {
                if (itemArray.length > 1) {
                    const tempArray = [];

                    itemArray.splice(removeItemIndex, 1); //usun element
                    itemArray.slice(0, itemArray.length); //zaznacz wartosci z tablicy po usunieciu  elementu tworzac nowa tablice z nowymi wartosciami
                    for (var i = 0; i < itemArray.length; i++) {

                        const item = itemArray[i].outerHTML;
                        tempArray.push(item);
                    }
                    tempArray.join(',');
                    localStorage.setItem(localStorageKey, tempArray);

                } else {
                    localStorage.removeItem(localStorageKey);
                }

            }


            function setStyleForLastTableColumn(tableBodyChilds) {
                for (var i = 0; i < tableBodyChilds.length; i++) {
                    let childNodes = $(tableBodyChilds[i]);
                    let data = $(childNodes)[0].cells[6].children;
                    let lastTD = $(childNodes)[0].cells[5];
                    data = $(data).unwrap();
                    let parent = $('<div></div>').insertAfter(lastTD);

                    $(parent).css({
                        'min-height': "75.250px",
                        'display': "flex",
                        "align-items": "center",
                        "border-bottom": "0",
                        "border-left": "0",
                        "border-right": "0",
                        "padding-right": "10%",
                        "padding-left": "10%",
                    }).addClass('table-bordered');

                    $(data).prependTo(parent);

                }
            }

        });

    </script>
@endpush
