@extends('layouts.app')
@section('content')

    <div class="container">
        <link href="{{ URL::asset('/css/admin_panel_buttons_styles.css') }}" rel="stylesheet" type="text/css" >
        <form id="createUser" method="POST" action="{{route('users.store')}}">
            {{ csrf_field() }}
            {{ method_field('POST') }}
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-body">
                                <div class="form-group">
                                    <div id="errorNameField"></div>
                                    <label for="name">Imię</label>
                                    <input id="name" type="text" class="form-control" name="name">
                                </div>
                                <hr>
                                <div class="form-group">
                                    <div id="errorEmailField"></div>
                                    <label for="email">Adres e-mail</label>
                                    <input id="email" type="email" class="form-control" name="email">
                                </div>
                                <div class="form-group">
                                    <label>Hasło zostanie wysłane na adres email pracownika</label>
                                </div>
                                <button class=" addUserButton btn btn-primary float-right" type="submit" style="background-color: mediumseagreen;">Zapisz</button>
                                <a href="{{route('users.index')}}" class=" backAddUserButtonLink btn btn-secondary" style="background-color: #4aa0e6;">Wstecz</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@push('js')

    <script>
        $(document).ready(function () {

            $('#createUser').submit('.addUserButton', function (e) {

                e.preventDefault();

                var usernameInput = $('#name').val();
                var emailInput = $("#email").val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax(
                    {
                        url: "{{route('users.store')}}",
                        type: 'POST',
                        data: {

                            "_token": "{{ csrf_token() }}",
                            'name': usernameInput,
                            'email': emailInput,
                        },

                        success: function () {

                            window.location.replace('/admin/users'); //redirect to main page after success ajax call
                        },

                        error: function (response) {

                            const jsonStr = JSON.stringify(response);
                            const obj = JSON.parse(jsonStr);
                            const errorMessages = obj.responseJSON;

                            ("name" in errorMessages) ? $("#errorNameField").html("<div class='alert alert-danger'>" + errorMessages["name"] + "</div>").show()
                                : $("#errorNameField").hide();

                            ("email" in errorMessages) ? $("#errorEmailField").html("<div class='alert alert-danger'>" + errorMessages["email"] + "</div>").show()
                                : $("#errorEmailField").hide();

                        }

                    });
            });

        });

    </script>
@endpush


