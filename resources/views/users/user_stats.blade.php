
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"/>
<div class="grid-container">
    <div class="grid-item 1 rounded" id="drag1" style="background-color: deepskyblue;" draggable="true" ondragstart="drag(event)" ondrop="drop(event)" ondragover="allowDrop(event)">
        <span id="usersSum" style="margin-left: 2%;">0</span><i id="icon" class="fas fa-users"
                                                                style="float: right;padding: 5%;"></i>
        <p class="usersAmount" style="font-size: 1.5vh;margin-top: -1vh;margin-left: 2%;">Total users</p>
        <button class="btn btn-secondary dropdown-toggle"
                style="width: 100%;margin-bottom: 12%;background-color: #00abe5;border: none;" type="button"
                id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Select
        </button>
        <ul class="dropdown-menu" id="userStats" aria-labelledby="dropdownMenuButton">
            <li class="dropdown-item1">Users per day</li>
            <li class="dropdown-item2">Users per week</li>
            <li class="dropdown-item3">Users per month</li>
            <li class="dropdown-item4">Users per year</li>
        </ul>

    </div>
    <div class="grid-item 2 rounded" id="drag2" style="background-color: mediumseagreen"  draggable="true"  ondragstart="drag(event)" ondrop="drop(event)" ondragover="allowDrop(event)">
        <span id="activeUsersSum" style="margin-left: 2%;">0</span><i id="icon2" class="far fa-smile" style="float: right;padding: 5%;"></i>
        <p class="usersActivity" style="font-size: 1.5vh;margin-top: -1vh;margin-left: 2%;">Active users</p>
        <button class="btn btn-secondary dropdown-toggle usersActivity"
                style="width: 100%;margin-bottom: 12%;background-color: #36a266	;border: none;" type="button"
                aria-haspopup="true" aria-expanded="false">
            Select
        </button>
    </div>
    <div class="grid-item 3 rounded" id="drag3" style="background-color: #F5B041 " draggable="true" ondragstart="drag(event)" ondrop="drop(event)" ondragover="allowDrop(event)">
        <span id="userRoleSum" style="margin-left: 2%;">0</span><i id="icon3" class="fas fa-user-secret" style="float: right;padding: 5%;"></i>
        <p class="userRole" style="font-size: 1.5vh;margin-top: -1vh;margin-left: 2%;">User roles</p>
        <button class="btn btn-secondary dropdown-toggle userRole"
                style="width: 100%;margin-bottom: 12%;background-color: #f5a141	;border: none;" type="button" id="dropdownMenuButton2"
                aria-haspopup="true" aria-expanded="false">
            Select
        </button>
        <ul class="dropdown-menu" id="userRoles" aria-labelledby="dropdownMenuButton">
            @foreach($userRoles as $userRole)
            <li>{{$userRole->name}}</li>
                @endforeach
        </ul>
    </div>
    <div class="grid-item 4 rounded" id="drag4" style="background-color: red" draggable="true" ondragstart="drag(event)" ondrop="drop(event)" ondragover="allowDrop(event)">
        <span id="bannedUsers" style="margin-left: 2%;">0</span><i id="icon4" class="fas fa-user-times" style="float: right;padding: 5%;"></i>
        <p class="bannedUser" style="font-size: 1.5vh;margin-top: -1vh;margin-left: 2%;">Banned Users</p>
        <button class="btn btn-secondary bannedUsers"
                style="width: 100%;margin-bottom: 12%;background-color: #e60000 ;border: none;" type="button" id="dropdownMenuButton3"
                aria-haspopup="true" aria-expanded="false">
            Banned users
        </button>
    </div>
</div>

<style>
    .grid-container {
        display: grid;
        grid-column-gap: 5%;
        justify-content: center;
        grid-template-columns: 20% 20% 20% 20%;
    }

    .grid-container div {
        height: 12vh;
    }

    .grid-container > div {
        color: white;
        font-size: 4vh;
        font-family: Poppins;
        margin-left: 5%;
    }

    .grid-container > div:hover {
        box-shadow: inset 0 0 0 1000px rgba(0, 0, 0, .05);
        transition: 0.4s;
    }

    .grid-container .dropdown-menu {
        width: 18.4%;
    }

    .grid-container .dropdown-menu > li {
        padding-left: 2%;
        cursor: default;
        padding-top: 0.5% !important;
        padding-bottom: 0.5% !important;
        width: 100%;
    }

    .grid-container .dropdown-menu > li:hover {
        background-color: #F8F8FF;
    }

    .btn-secondary.dropdown-toggle:focus {

        box-shadow: none !important;
    }

    ul#userRoles{
        left : 0;
        position: relative;
        width: 100%;
        margin-top: -11%;
    }

</style>

<script src='https://cdnjs.cloudflare.com/ajax/libs/countup.js/1.8.2/countUp.min.js'></script>
<script>
    function swapElements(obj1, obj2) {
        // save the location of obj2
        var parent2 = obj2.parentNode;
        var next2 = obj2.nextSibling;

        // special case for obj1 is the next sibling of obj2
        if (next2 === obj1) {
            // just put obj1 before obj2
            parent2.insertBefore(obj1, obj2);
        } else {
            // insert obj2 right before obj1
            obj1.parentNode.insertBefore(obj2, obj1);

            // now insert obj1 where obj2 was
            if (next2) {
                // if there was an element after obj2, then insert obj1 right before that
                parent2.insertBefore(obj1, next2);
            } else {
                // otherwise, just append as last child
                parent2.appendChild(obj1);
            }
        }
    }

    function allowDrop(ev) {
        ev.preventDefault();
    }

    function drag(ev) {
        ev.dataTransfer.setData(this, ev.target.id);
    }

    function drop(ev) {
        const swappedElements = new Set();
        ev.preventDefault();
        var data = ev.dataTransfer.getData(this);

        if (ev.target.tagName === 'DIV') {
            swapElements(ev.target, document.getElementById(data));
            let toHTML = document.getElementsByClassName('grid-container')[0].children;
            let htmlArray = Array.from(toHTML);
            htmlArray.forEach((element) => {
                swappedElements.add(element.outerHTML);

            });
            let htmlToStr = Array.from(swappedElements).join("");
            localStorage.setItem('swappedUserStats',htmlToStr);
        }

    }
</script>
<script>

    $(document).ready(function () {

        if(localStorage.getItem('swappedUserStats')) {
           const swappedElements =  localStorage.getItem('swappedUserStats');
           $('.grid-container').html(swappedElements);
        }

        var usersSumCounter = new CountUp("usersSum", 0,{{$usersSum}});
        usersSumCounter.start();

        var activeUsersSumCounter = new CountUp("activeUsersSum", 0,{{$activeUsersSum}});
        activeUsersSumCounter.start();

        var userRoleSum = new CountUp("userRoleSum", 0,{{$userRoleSum}});
        userRoleSum.start();

        var bannedUsersSum = new CountUp("bannedUsers", 0,{{$bannedUsersSum}});
        bannedUsersSum.start();

        $('.grid-item1,#userStats').on('click', 'li', function () {
            const childName = $(this)[0].className;
            const usersPerTime = $(this)[0].innerHTML;
            const parent = $(this).parent();
            $(parent).removeClass('show');

            $.ajax(
                {
                    url: '{{route('users.index')}}',
                    type: 'get',
                    data: {
                        "totalUsers": childName,
                        "_token": "{{csrf_token()}}"
                    },
                    success: function (successResponse) {

                        var usersSumCounter = new CountUp("usersSum", 0, successResponse.usersSum);
                        usersSumCounter.start();
                        $('#dropdownMenuButton').html(usersPerTime);

                    },

                    error: function (errorResponse) {

                    }

                });

        });


        $('.usersActivity').on('click', function () {

            if ($(this)[0].getAttribute('id') === 'inactiveUsers') { //toggle between active and inactive users
                $(this).removeAttr('inactiveUsers');
                $(this).attr('id', 'activeUsers');
            } else {
                $(this).attr('id', 'inactiveUsers');
            }

            let getUserActivityStatus = $(this)[0].attributes[5].value;

            $.ajax(
                {
                    url: '{{route('users.index')}}',
                    type: 'get',
                    data: {
                        "userActivity": getUserActivityStatus,
                        "_token": "{{csrf_token()}}"
                    },
                    success: function (successResponse) {
                        if (successResponse.inactiveUsers) {
                            const inactiveUsersSum = new CountUp("activeUsersSum", 0, successResponse.inactiveUsers);
                            inactiveUsersSum.start();
                            $('.usersActivity').html("Inactive users");

                        } else if (successResponse.activeUsers) {
                            const activeUsersSum = new CountUp("activeUsersSum", 0, successResponse.activeUsers);
                            activeUsersSum.start();
                            $('.usersActivity').html("Active users");
                        }

                    },
                    error: function (errorResponse) {

                    }

                });

        });

        $('.userRole').on('click',function(){

            const childNode = $(this)[0].nextElementSibling;
            $(childNode).addClass("show");
        });


        $('.grid-item3,#userRoles').on('click', 'li', function () {

            const userRole = $(this)[0].textContent;
            const parent = $(this).parent();
            $(parent).removeClass('show');

            $.ajax(
                {
                    url: '{{route('users.index')}}',
                    type: 'get',
                    data: {
                        "userRole": userRole,
                        "_token": "{{csrf_token()}}"
                    },
                    success: function (successResponse) {
                            var userRoleSum = new CountUp("userRoleSum", 0, successResponse.userRole);
                            userRoleSum.start();
                            const userRoleString = userRole.toString();
                            const userRoleUppercase = userRoleString.charAt(0).toUpperCase() + userRoleString.slice(1);
                        $('.userRole').html(userRoleUppercase);
                        $('#dropdownMenuButton2').html(userRoleString);

                    },

                    error: function (errorResponse) {

                    }

                });

        });

    });
</script>


