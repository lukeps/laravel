<div class="grid-container">
    <div class="grid-item 1 rounded" style="background-color: white;">
        <div class="hubContainer">
            <p>Total views</p>
        </div>
        <div class="homeStatsHub" style="background-color: deepskyblue">
            <i class="fas fa-eye"></i>
        </div>
        <i class="far fa-clock" id="iconStat"></i>
        <p class="onlineUsers">Last 24 hours</p>
        <div class="bottomBorderGradient"></div>
    </div>
    <div class="grid-item 2 rounded" style="background-color: white">
        <div class="hubContainer">
            <p>Online users</p>
        </div>
        <div class="homeStatsHub" style="background-color: mediumseagreen">
            <i class="fas fa-plug"></i>
        </div>
        <i class="fas fa-user-check" id="iconStat"></i>
        <p class="onlineUsers">Last 5 minutes</p>
        <div class="bottomBorderGradient"></div>
    </div>
    <div class="grid-item 3 rounded" style="background-color: white">
        <div class="hubContainer">
            <p>Online users</p>
        </div>
        <div class="homeStatsHub" style="background-color: #F5B041">
            <i class="fas fa-plug"></i>
        </div>
        <i class="fas fa-user-check" id="iconStat"></i>
        <p class="onlineUsers">Last 5 minutes</p>
        <div class="bottomBorderGradient"></div>
    </div>
    <div class="grid-item 4 rounded" style="background-color: white">
        <div class="hubContainer">
            <p>Space usage</p>
        </div>
        <div class="homeStatsHub" style="background-color: red">
            <i class="fas fa-database"></i>
        </div>
        <i class="far fa-clock" id="iconStat"></i>
        <p class="onlineUsers">Last 5 minutes</p>
        <div class="bottomBorderGradient"></div>
    </div>
</div>
@push('home_stats_styles')
    <link href="{{ URL::asset('css/home_stats.css') }}" rel="stylesheet">
@endpush
@push('home_stats_js')
    <script src='https://cdnjs.cloudflare.com/ajax/libs/countup.js/1.8.2/countUp.min.js'></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>

    <script>

        $(document).ready(function () {

            $.ajax(
                {

                    url: '{{route('home.index')}}',
                    type: 'GET',
                    data: {
                        "cachedID": "cachedID",
                    },

                    success: function (successResponse) {

                        try {
                            const onlineUsersHub = $('.hubContainer')[1];
                            const appendParagraph = $(onlineUsersHub).append('<div>' + successResponse.cachedID.length + '</div>');
                            const children = $(appendParagraph)[0].children[1];
                            setParagraphStyle(children);

                            const onlineUsersCounter = new CountUp(children, 0, successResponse.cachedID.length);
                            onlineUsersCounter.start();
                        }

                        catch (e){
                            console.log('error')
                        }

                    },
                    error: function (response) {

                    },

                });


            (function getMYSQLSize() {
                $.ajax(
                    {

                        url: '{{route('home.mysqlSize')}}',
                        type: 'POST',
                        data: {
                            "_token": "{{csrf_token()}}"
                        },

                        success: function (successResponse) {

                            if (successResponse) {

                                const pageViewsHub = $('.hubContainer')[3];
                                const appendParagraphToPageViews = $(pageViewsHub).append('<div></div>');
                                const appendUnitMetricToPageView = $(appendParagraphToPageViews).append('<p></p>');
                                const childrenPageViews = $(appendParagraphToPageViews)[0].children[1];
                                const unitParagraphStyle = $(appendUnitMetricToPageView)[0].children[2];
                                if (successResponse.dbSize < 1000) {
                                    unitParagraphStyle.innerText = "MB"
                                } else {
                                    unitParagraphStyle.innerText = "GB"
                                }
                                setParagraphStyle(childrenPageViews);
                                setMetricUnitStyle(unitParagraphStyle)

                                const pageViewsCounter = new CountUp(childrenPageViews, 0, successResponse.dbSize, 2);
                                pageViewsCounter.start();

                            }
                        },
                        error: function (response) {
                        },

                    });
            }());

            (function getPOSTGRESQLSize() {
                $.ajax(
                    {

                        url: '{{route('home.postgresqlSize')}}',
                        type: 'POST',
                        data: {
                            "_token": "{{csrf_token()}}"
                        },

                        success: function (successResponse) {
                            if (successResponse) {
                                console.log(successResponse)
                                const pageViewsHub = $('.hubContainer')[3];
                                const appendParagraphToPageViews = $(pageViewsHub).append('<div></div>');
                                const appendUnitMetricToPageView = $(appendParagraphToPageViews).append('<p></p>');
                                const childrenPageViews = $(appendParagraphToPageViews)[0].children[1];
                                const unitParagraphStyle = $(appendUnitMetricToPageView)[0].children[2];
                                setParagraphStyle(childrenPageViews);
                                setMetricUnitStyle(unitParagraphStyle)
                                if (successResponse.dbSize < 1000) {
                                    appendUnitMetricToPageView[0].children[2].innerText = "MB"
                                } else {
                                    appendUnitMetricToPageView[0].children[2].innerText = "GB"
                                }

                                const pageViewsCounter = new CountUp(childrenPageViews, 0, successResponse.dbSize, 2);
                                pageViewsCounter.start();


                            }
                        },
                        error: function (response) {

                        },

                    });
            }());

            function setParagraphStyle(element) {

                element.style.marginRight = '3vw';
                element.style.position = 'absolute';
                element.style.color = 'black';
                element.style.fontSize = '2.5vw';
                element.style.marginTop = '2.5vh';

            }

            function setMetricUnitStyle(element) {


                element.style.position = 'absolute';
                element.style.color = 'black';
                element.style.fontSize = '0.7vw';
                element.style.bottom = '25%';

            }

        });
    </script>
@endpush
