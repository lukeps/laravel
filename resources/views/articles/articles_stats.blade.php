<div class="grid-container">
    <div class="grid-item 1 rounded" id="drag1" style="background-color: deepskyblue;" draggable="true"
         ondragstart="drag(event)" ondrop="drop(event)" ondragover="allowDrop(event)">
        <span id="articlesSum" style="margin-left: 2%;">0</span><i id="icon" class="fas fa-sticky-note"
                                                                   style="float: right;padding: 5%;"></i>
        <p class="usersAmount" style="font-size: 1.5vh;margin-top: -1vh;margin-left: 2%;">Total Articles</p>
        <button class="btn btn-secondary dropdown-toggle"
                style="width: 100%;margin-bottom: 12%;background-color: #00abe5;border: none;" type="button"
                id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Select
        </button>
        <ul class="dropdown-menu" id="articlesStats" aria-labelledby="dropdownMenuButton">
            <li class="dropdown-item1">Articles per day</li>
            <li class="dropdown-item2">Articles per week</li>
            <li class="dropdown-item3">Articles per month</li>
            <li class="dropdown-item4">Articles per year</li>
        </ul>
    </div>
    <div class="grid-item 2 rounded" id="drag2" style="background-color: mediumseagreen" draggable="true"
         ondragstart="drag(event)" ondrop="drop(event)" ondragover="allowDrop(event)">
        <span id="articlesByUserSum" style="margin-left: 2%;">0</span><i id="icon2" class="fas fa-calendar-check"
                                                                         style="float: right;padding: 5%;"></i>
        <p class="articlesByCategory" style="font-size: 1.5vh;margin-top: -1vh;margin-left: 2%;">Articles by User</p>
        <select class="articlesByUser js-state form-control"
                style="width: 100%;margin-bottom: 12%;background-color: white;border: none;font-size: 1.5vh !important;">
            <option></option>
            @foreach($usersNames as $userName)
                <option class="opt" id="{{$userName->id}}">{{$userName->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="grid-item 3 rounded" id="drag3" style="background-color: #F5B041 " draggable="true"
         ondragstart="drag(event)" ondrop="drop(event)" ondragover="allowDrop(event)">
        <span id="articlesByCategorySum" style="margin-left: 2%;">-</span><i id="icon3"
                                                                             class="fas fa-calendar-check"
                                                                             style="float: right;padding: 5%;"></i>
        <p class="articlesByCategory" style="font-size: 1.5vh;margin-top: -1vh;margin-left: 2%;white-space:nowrap;">
            Articles by
            Category</p>
        <button class="btn btn-secondary dropdown-toggle articlesByCategory"
                style="width: 100%;margin-bottom: 12%;background-color: #f5a141	;border: none;" type="button"
                id="dropdownMenuButton2"
                aria-haspopup="true" aria-expanded="false">
            Select
        </button>
        <ul class="dropdown-menu" id="articleByCategoryList" aria-labelledby="dropdownMenuButton">
            @foreach($articlesCategories as $articleCategory)
                <li>{{$articleCategory->name}}</li>
            @endforeach
        </ul>
    </div>
    <div class="grid-item 4 rounded" id="drag4" style="background-color: red" draggable="true"
         ondragstart="drag(event)" ondrop="drop(event)" ondragover="allowDrop(event)">
        <span id="bannedUsers" style="margin-left: 2%;">0</span><i id="icon4" class="fas fa-user-times"
                                                                   style="float: right;padding: 5%;"></i>
        <p class="bannedUser" style="font-size: 1.5vh;margin-top: -1vh;margin-left: 2%;">Banned Users</p>
        <button class="btn btn-secondary bannedUsers"
                style="width: 100%;margin-bottom: 12%;background-color: #e60000 ;border: none;" type="button"
                id="dropdownMenuButton3"
                aria-haspopup="true" aria-expanded="false">
            Banned users
        </button>
    </div>
</div>
<style>
    .grid-container {
        display: grid;
        grid-column-gap: 5%;
        justify-content: center;
        grid-template-columns: 20% 20% 20% 20%;
    }

    .grid-container div {
        height: 12vh;
    }

    .grid-container > div {
        color: white;
        font-size: 4vh;
        font-family: Poppins;

    }

    .grid-container > div:hover {
        box-shadow: inset 0 0 0 1000px rgba(0, 0, 0, .05);
        transition: 0.4s;
    }

    .grid-container .dropdown-menu {
        width: 18.4%;
    }

    .grid-container .dropdown-menu > li {
        padding-left: 2%;
        cursor: default;
        padding-top: 0.5% !important;
        padding-bottom: 0.5% !important;
        width: 100%;
    }

    .grid-container .dropdown-menu > li:hover {
        background-color: #F8F8FF;
    }

    .btn-secondary.dropdown-toggle:focus {

        box-shadow: none !important;
    }

    ul#articleByCategoryList {
        left: 0;
        position: relative;
        width: 100%;
        margin-top: -11%;
    }

    .select2-selection_placeholder {
        color: red;
    }

</style>

<script src='https://cdnjs.cloudflare.com/ajax/libs/countup.js/1.8.2/countUp.min.js'></script>

<script>
    function swapElements(obj1, obj2) {
        // save the location of obj2
        var parent2 = obj2.parentNode;
        var next2 = obj2.nextSibling;

        // special case for obj1 is the next sibling of obj2
        if (next2 === obj1) {
            // just put obj1 before obj2
            parent2.insertBefore(obj1, obj2);
        } else {
            // insert obj2 right before obj1
            obj1.parentNode.insertBefore(obj2, obj1);

            // now insert obj1 where obj2 was
            if (next2) {
                // if there was an element after obj2, then insert obj1 right before that
                parent2.insertBefore(obj1, next2);
            } else {
                // otherwise, just append as last child
                parent2.appendChild(obj1);
            }
        }
    }

    function allowDrop(ev) {
        ev.preventDefault();
    }

    function drag(ev) {
        ev.dataTransfer.setData(this, ev.target.id);
    }


    function drop(ev) {
        const swappedElements = new Set();
        ev.preventDefault();
        var data = ev.dataTransfer.getData(this);

        if (ev.target.tagName === 'DIV') {
            swapElements(ev.target, document.getElementById(data));
            let toHTML = document.getElementsByClassName('grid-container')[0].children;
            let htmlArray = Array.from(toHTML);
            htmlArray.forEach(function (element) {

                swappedElements.add($(element)[0].outerHTML);

            });
            let htmlToStr = Array.from(swappedElements).join("");
            localStorage.setItem('swappedArticlesStats', htmlToStr);
        }

    }
</script>

<script>

    $(window).on('load', function () {
        let counter = 0;
        let temp = [];
        let singleUserWithFlag;
        let iterate = true;
        let unfilteredList = true;

        $('.articlesByUser').select2({
            templateSelection: function (data) {
                if (data.id === '') {
                    return 'Select an option...';
                } else {
                    return $(data)[0].element.textContent;
                }
            },
            templateResult: function (data) {
                if (iterate) { //loop through  all users only once and set them correct nationalities
                        let users = Array.from($('.opt'));
                        users.unshift('<div></div>'); //this is first element in optgroup which is disabled/hidden default, move correct options under hidden element to make them all visible
                        for (let i = 0; i < users.length; i++) {
                            let user = users[i];
                            let userFlag = new CountryFlag($(user)[0]);
                            @foreach($usersNationalities as $userNationality)
                            if ('{{isset($userNationality->nationality)}}')
                                if ('{{$userNationality->id}}' === $(user)[0].id)
                                    userFlag.selectByAlpha2("{{$userNationality->nationality}}");
                                else {
                                    userFlag.selectByAlpha2('NL');
                                }
                                @endforeach

                            let nationality = $(userFlag)[0].element;
                            $(nationality)[0].style.marginLeft = '80%';
                            $(nationality)[0].style.position = 'relative';
                            $(nationality).html('<p style="left: -10vw; position: absolute">' + $(user)[0].textContent + '</p>');
                            temp.push(nationality);

                        }
                        iterate = false;
                }
                if(unfilteredList) { //nie wchodz tu jesli cos zostalo wpisane w search... poprawic warunek

                    singleUserWithFlag = temp[counter];
                    counter = counter + 1;
                    return singleUserWithFlag;
                }
                else if(!unfilteredList) {

                    for(let i=0;i<temp.length;i++){
                        if(data.element.innerHTML === temp[i].textContent){
                            unfilteredList=true;
                            counter=0;
                            return temp[i];
                        }
                    }
                }
            },
            matcher: function (params, data) {

                if ($.trim(params.term) === '') {
                    return data;
                }

                else if (data.text.indexOf(params.term) !== -1) {
                    var modifiedData = $.extend({}, data, true);
                    unfilteredList=false;
                    return modifiedData
                }
               else{
                   return null;
                }

            },
            placeholder: ' ',
            allowClear: true,
        }).on('select2:select', function (data) {

            counter = 0; //restart counter after selecting value in optgroup
            {
                $.ajax({
                    url: '{{route('articles.sum')}}',
                    dataType: "json",
                    type: 'post',
                    data: {
                        "id": data.params.data.element.id,
                        "_token": "{{csrf_token()}}"
                    },
                    success: function (data) {
                        var articlesSum = new CountUp('articlesByUserSum', 0, data.articlesSum);
                        articlesSum.start();
                        localStorage.setItem('articlesSum', data.articlesSum);

                    },
                });
                $('.select2-selection__clear').css('font-size', '0.5em');

            }

            const textValue = $(data.target).val();
            localStorage.setItem('select2Input', textValue);

        }).on('select2:close',function(){
            counter=0;
        });

        $('.select2-container').css('margin-bottom', '12%');
        $('.select2-selection, .select2-selection--single').css('height', '3.5vh');
        if (!localStorage.getItem('select2Input')) {
            $('.select2-selection__rendered').html('Select an option...').css({
                'font-size': '1.5vh',
                'color': 'gray'
            });
        }


    });
</script>
<script>

    $(document).ready(function () {

        function getLocalStorageContentArray(localStorageKey) {
            if (localStorage.getItem(localStorageKey)) {
                const element = localStorage.getItem(localStorageKey);
                const elementsWithoutCommas = element.split(",").join("");
                const htmlElement = $.parseHTML(elementsWithoutCommas);
                var elementsArray = Array.from(htmlElement);
            }
            return elementsArray;
        }

        if (localStorage.getItem('swappedArticlesStats')) {
            let swappedElements = getLocalStorageContentArray('swappedArticlesStats');
            swappedElements.forEach(function (element) {

                if ($(element)[0].attributes[0].value === 'grid-item 2 rounded') {
                    $(element)[0].children[4].remove(); //prevent duplicate of select2-container, default is loaded after select2() init
                }
            });

            $('.grid-container').html(swappedElements);

        }

        if (localStorage.getItem('select2Input')) {
            var input = localStorage.getItem('select2Input');
            window.onload = function () {
                $('.select2-selection__rendered').html(input).css({
                    'font-size': '1.5vh',
                    'color': 'gray'
                });
            }

        }


        if (localStorage.getItem('articlesSum')) {
            const articlesSum = localStorage.getItem('articlesSum');
            $('#articlesByUserSum').html(articlesSum);
        }

        var articlesSumCounter = new CountUp("articlesSum", 0,{{$articlesSum}});
        articlesSumCounter.start();


        var bannedUsersSum = new CountUp("bannedUsers", 0,{{$bannedUsersSum}});
        bannedUsersSum.start();


        $('.grid-item1,#articlesStats').on('click', 'li', function () {
            const childName = $(this)[0].className;
            const articlesPerTime = $(this)[0].innerHTML;
            const parent = $(this).parent();
            $(parent).removeClass('show');

            $.ajax(
                {
                    url: '{{route('articles.index')}}',
                    type: 'get',
                    data: {
                        "totalArticles": childName,
                        "_token": "{{csrf_token()}}"
                    },
                    success: function (successResponse) {

                        var articlesSumCounter = new CountUp("articlesSum", 0, successResponse.articlesSum);
                        articlesSumCounter.start();
                        $('#dropdownMenuButton').html(articlesPerTime);

                    },

                    error: function (errorResponse) {

                    }

                });

        });

        $('.usersActivity').on('click', function () {

            if ($(this)[0].getAttribute('id') === 'inactiveUsers') { //toggle between active and inactive users
                $(this).removeAttr('inactiveUsers');
                $(this).attr('id', 'activeUsers');
            } else {
                $(this).attr('id', 'inactiveUsers');
            }

            let getUserActivityStatus = $(this)[0].attributes[5].value;

            $.ajax(
                {
                    url: '{{route('users.index')}}',
                    type: 'get',
                    data: {
                        "userActivity": getUserActivityStatus,
                        "_token": "{{csrf_token()}}"
                    },
                    success: function (successResponse) {
                        if (successResponse.inactiveUsers) {
                            const inactiveUsersSum = new CountUp("activeUsersSum", 0, successResponse.inactiveUsers);
                            inactiveUsersSum.start();
                            $('.usersActivity').html("Inactive users");

                        } else if (successResponse.activeUsers) {
                            const activeUsersSum = new CountUp("activeUsersSum", 0, successResponse.activeUsers);
                            activeUsersSum.start();
                            $('.usersActivity').html("Active users");
                        }

                    },
                    error: function (errorResponse) {

                    }

                });

        });

        $('.articlesByCategory').on('click', function () {

            const childNode = $(this)[0].nextElementSibling;
            $(childNode).addClass("show");
        });


        $('.grid-item3,#articleByCategoryList').on('click', 'li', function () {

            const articleByCategory = $(this)[0].textContent;
            const parent = $(this).parent();
            $(parent).removeClass('show');

            $.ajax(
                {
                    url: '{{route('articles.index')}}',
                    type: 'get',
                    data: {
                        "articleByCategory": articleByCategory,
                        "_token": "{{csrf_token()}}"
                    },
                    success: function (successResponse) {
                        var articlesByCategory = new CountUp("articlesByCategorySum", 0, successResponse.articlesByCategorySum);
                        articlesByCategory.start();

                        $('#dropdownMenuButton2').html(articleByCategory);

                    },

                    error: function (errorResponse) {

                    }

                });

        });

    });
</script>

