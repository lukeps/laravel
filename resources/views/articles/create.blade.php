@extends('layouts.app')
@section('content')
    <div class="container">
        <form id="createArticle" method="POST">
            {{ csrf_field() }}
            {{method_field("GET")}}
            <link href="{{ URL::asset('/css/admin_panel_buttons_styles.css') }}" rel="stylesheet" type="text/css">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-body">
                                <div class="form-group">
                                    <div id="errorNameField"></div>
                                    <label for="name">Podaj tytuł</label>
                                    <input type="text" class="form-control" id="name" name="name">
                                </div>
                                <hr>
                                <div class="form-group">
                                    <div id="errorShortTextField"></div>
                                    <label for="short_text">Krótki opis:</label>
                                    <textarea class="form-control" id="short_text" rows="6"
                                              name="short_text"></textarea>
                                </div>
                                <div class="form-group">
                                    <div id=errorTextField></div>
                                    <label for="text">Opis szczegółowy:</label>
                                    <textarea class="form-control" id="text" rows="9" name="text"></textarea>
                                </div>
                                <div id=errorCategoryGroup></div>
                                <div class="form-group col-sm-4">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Wybierz kategorie:</span>
                                        </div>
                                        <select name="categoryGroup" id="categoryGroup"
                                                class="selectCategory form-control">
                                            <option>Wybierz kategorie</option>
                                            @foreach($categories as $category)
                                                <option value="{{$category->id}}"
                                                        @if($category->id == old('categoryGroup')) selected @endif>
                                                    {{$category->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <br>
                                <button id="addArticle" class=" addArticleButtons btn btn-primary float-right"
                                        style="background-color: mediumseagreen;" type="submit">Dodaj artykuł
                                </button>
                                <a href="{{route('articles.index')}}" class=" backButton btn btn-primary float-left"
                                   style="background-color: #4aa0e6;">Cofnij</a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
@push('js')
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

    <script>tinymce.init({

            selector: 'textarea#text',
            menubar: false,

        });
    </script>

    <script>
        $(document).ready(function () {

            $('#addArticle').click(function (e) {

                e.preventDefault();
                var articleTitle = $('#name').val();
                var articleShortText = $('#short_text').val();
                var articleText = tinymce.get('text').getContent();
                var articleCategory = $('#categoryGroup').val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax(
                    {

                        url: "{{route('articles.store')}}",
                        type: 'POST',
                        data: {
                            "_token": "{{csrf_token()}}",
                            'name': articleTitle,
                            'short_text': articleShortText,
                            'text': articleText,
                            'categoryGroup': articleCategory
                        },

                        success: function () {

                            window.location.replace('/admin/articles');

                        },
                        error: function (response) {

                            const jsonStr = JSON.stringify(response);
                            const obj = JSON.parse(jsonStr);
                            const errorMessages = obj.responseJSON;

                            ("name" in errorMessages) ? $("#errorNameField").html("<div class='alert alert-danger'>" + errorMessages["name"] + "</div>").show()
                                : $("#errorNameField").hide();

                            ("short_text" in errorMessages) ? $("#errorShortTextField").html("<div class='alert alert-danger'>" + errorMessages["short_text"] + "</div>").show()
                                : $("#errorShortTextField").hide();

                            ("text" in errorMessages) ? $("#errorTextField").html("<div class='alert alert-danger'>" + errorMessages["text"] + "</div>").show()
                                : $("#errorTextField").hide();

                            ("categoryGroup" in errorMessages) ? $("#errorCategoryGroup").html("<div class='alert alert-danger'>" + errorMessages["categoryGroup"] + "</div>").show()
                                : $("#errorCategoryGroup").hide();

                        }

                    });

            });

        });

    </script>

@endpush




