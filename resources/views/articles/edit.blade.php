@extends('layouts.app')

@section('content')

    <div class="container">
        <link href="{{ URL::asset('/css/admin_panel_buttons_styles.css') }}" rel="stylesheet" type="text/css">
        <form id="updateArticle" method="POST" action="{{ route('articles.update', $article->id)}}">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-body">
                                <div class="form-group">
                                    <div id="errorNameField"></div>
                                    <label for="name">Podaj tytuł</label>
                                    <input type="text" class=" articleTitle form-control" id="name"
                                           value="{{$article->name}}">
                                </div>
                                <hr>
                                <div class="form-group">
                                    <div id="errorShortTextField"></div>
                                    <label for="short_text">Krótki opis:</label>
                                    <textarea class=" articleShortText form-control" rows="6" id="short_text"
                                              name="short_text">{{$article->short_text}}</textarea>
                                </div>
                                <div class="form-group">
                                    <div id=errorTextField></div>
                                    <label for="text">Opis szczegółowy:</label>
                                    <textarea class=" articleText form-control" rows="9" id="text"
                                              name="text">{{$article->text}}</textarea>
                                </div>
                                <div id=errorCategoryGroup></div>
                                <div class="form-group col-sm-4">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Wybierz kategorie:</span>
                                        </div>
                                        <select name="categoryGroup" id="categoryGroup"
                                                class="selectCategory form-control">
                                            <option>Wybierz kategorie</option>
                                            @foreach($categories as $category)
                                                <option value="{{$category->id}}"
                                                        @if($category->id == old('categoryGroup')) selected @endif>
                                                    {{$category->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <button data-id="{{$article->id}}" class=" editArticleButton btn btn-primary float-right"
                                        id="editArticleButton" type="submit" style="background-color: mediumseagreen;">
                                    Wyślij
                                </button>
                                <a href="{{route('articles.index')}}" class=" backEditArticleButton btn btn-secondary"
                                   style="background-color: #4aa0e6;">Cofnij</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
@push('js')

    <script
        src="https://cdn.tiny.cloud/1/a0ly4cxb0i32yq0e6vi78lejki4r83evr344vj7bkn7kvx17/tinymce/5/tinymce.min.js"></script>

    <script>tinymce.init({

            selector: 'textarea#text',
            menubar: false,

        });

    </script>

    <script>
        $(document).ready(function () {

            $('#updateArticle').submit('#editArticleButton', function (e) {

                e.preventDefault();

                var articleTitleInput = $('#name').val();
                var articleShortTextInput = $("#short_text").val();
                var articleTextInput = tinymce.get('text').getContent();
                var articleCategorySelect = $("#categoryGroup").val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax(
                    {
                        url: '{{route('articles.update',$article->id)}}',
                        type: 'PATCH',
                        data: {
                            "_token": "{{ csrf_token() }}",
                            'name': articleTitleInput,
                            'short_text': articleShortTextInput,
                            'text': articleTextInput,
                            'category_id': articleCategorySelect
                        },

                        success: function () {

                            window.location.replace('/admin/articles'); //redirect to main page after success ajax call
                        },

                        error: function (response) {

                            const jsonStr = JSON.stringify(response);
                            const obj = JSON.parse(jsonStr);
                            const errorMessages = obj.responseJSON;

                            ("name" in errorMessages) ? $("#errorNameField").html("<div class='alert alert-danger'>" + errorMessages["name"] + "</div>").show()
                                : $("#errorNameField").hide();

                            ("short_text" in errorMessages) ? $("#errorShortTextField").html("<div class='alert alert-danger'>" + errorMessages["short_text"] + "</div>").show()
                                : $("#errorShortTextField").hide();

                            ("text" in errorMessages) ? $("#errorTextField").html("<div class='alert alert-danger'>" + errorMessages["text"] + "</div>").show()
                                : $("#errorTextField").hide();

                            ("category_id" in errorMessages) ? $("#errorCategoryGroup").html("<div class='alert alert-danger'>" + errorMessages["category_id"] + "</div>").show()
                                : $("#errorCategoryGroup").hide();

                        }

                    });
            });

        });

    </script>
@endpush

