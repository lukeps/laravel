@extends('layouts.app')
@section('content')
    @role('admin')
    <link href="{{ URL::asset('/css/admin_panel_buttons_styles.css') }}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="{{URL::asset('/css/country-flag.css')}}">
    @include('articles.articles_stats')
    <div class="container"style="height: 2vh;"></div>
    <div class="col-md-12" style="margin-top: 2%;">
        <div class="card">
            <div class="card-header">Lista artykułów
                <a id="addArticleButton" href="{{route('articles.create')}}" class=" addArticleButtons btn btn-primary float-right">Dodaj artykuł</a>
            </div>
            <div class="card-body">
                @if(session('status_msg'))
                    <div class="alert alert-success">
                        <p class="text-center"> {{session('status_msg')}}</p>
                    </div>
                @endif
                <div class="table-responsive table-hover">
                    <table class="table table-striped table-bordered dt-responsive nowrap ajaxTable"
                           style="margin: 0px auto">
                        <thead>
                        <tr>
                            <th class="thID" width="10%">ID
                            </th>
                            <th class="thName">Tytuł
                            </th>
                            <th class="thCreatedAt">Data dodania
                            </th>
                            <th class="thCategory"> Kategoria
                            </th>
                            <th class="thAutor">Autor
                            </th>
                            <th class=buttons>Akcje</th>
                        </tr>
                        </thead>
                        <thead>
                        <td><input class="form-control" id="column0_search" type="text" placeholder="Szukaj...">
                        </td>
                        <td><input class="form-control" id="column1_search" type="text" placeholder="Szukaj...">
                        </td>
                        <td><input class="form-control" id="column2_search" type="text" placeholder="Szukaj...">
                        </td>
                        <td><input class="form-control" id="column3_search" type="text" placeholder="Szukaj...">
                        </td>
                        <td><input class="form-control" id="column4_search" type="text" placeholder="Szukaj...">
                        </td>
                        <td>
                        </td>
                        </thead>
                    </table>
                </div>
                <div class="container">
                </div>
            </div>
        </div>
    </div>
@endrole
@endsection
@push('js')
    <script src='https://cdnjs.cloudflare.com/ajax/libs/countup.js/1.8.2/countUp.min.js'></script>
    <script type="text/javascript" src="{{ URL::asset('/js/datatables.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('/js/dataTables.bootstrap4.js') }}"></script>
    <script src="{{URL::asset('/js/country-flag.js')}}"></script>
    <script>

        $(document).ready(function () {

            var table = $('.ajaxTable').DataTable({
                "sDom": "ltipr",
                serverSide: true,
                select: true,
                paging: true,
                pageLength: 5,
                lengthMenu: [5, 10, 15, 20, 25],
                stateSave: false,
                searching: true,
                processing: true,
                ajax: {

                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    url: "{{route('articles.jsonTable')}}",

                },
                data: {

                    "_token": "{{ csrf_token() }}"
                },

                columns: [
                    {data: 'a_id'},
                    {data: 'a_name'},
                    {data: 'a_created_at'},
                    {data: 'c_name'},
                    {data: 'u_name'},
                    {
                        width: '20%',
                        sortable: false,
                        searchable: false,

                        render: function (data, type, row) {

                            return ' <a href="/admin/articles/' + row['a_id'] + '  "class="showArticle btn btn-sm btn-primary d-block mt-1" style="background: mediumseagreen" >Zobacz</a>' +
                                '<a href="/admin/articles/' + row['a_id'] + '/edit" class="editArticle btn btn-sm btn-primary d-block mt-1">Edycja</a>' +
                                '<button style= "width: 100%" name="deleteButton" id="idDeleteButton" type="button" data-id = "' + row['a_id'] + ' "class="deleteArticle btn btn-sm btn-danger d-block mt-1" >Usuń</button>';

                        },

                        error: (error) => {
                            console.log(JSON.stringify(error));
                        },

                    },
                ],
                order: [
                    [0, 'desc'],
                    [1, 'desc'],
                    [2, 'desc'],
                    [3, 'desc'],
                    [4, 'desc'],
                ],
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            $('.ajaxTable').on('click', '.deleteArticle', function () {

                var idx = $('#idDeleteButton').attr('data-id');

                if (confirm("Na pewno chcesz usunąć ?")) {
                    $.ajax(
                        {
                            url: 'articles/' + idx,
                            type: 'DELETE',

                            data: {

                                "_token": "{{csrf_token()}}"
                            },

                            success: function () {
                                table.ajax.reload();
                                alert("Pomyślnie usunięto artykuł.")
                            },

                        });
                }

                return false;
            });

            $('#column0_search').on('keyup', function () {

                var inputValue = $('#column0_search').val();
                table.columns(0).search(inputValue).draw();

            });

            $('#column1_search').on('keyup', function () {

                var inputValue = $('#column1_search').val();
                table.columns(1).search(inputValue).draw();

            });

            $('#column2_search').on('keyup', function () {

                var inputValue = $('#column2_search').val();
                table.columns(2).search(inputValue).draw();

            });

            $('#column3_search').on('keyup', function () {

                var inputValue = $('#column3_search').val();
                table.columns(3).search(inputValue).draw();

            });

            $('#column4_search').on('keyup', function () {

                var inputValue = $('#column4_search').val();
                table.columns(4).search(inputValue).draw();
            });

            $('.caret').remove();


            let articlesSumCounter = new CountUp("articlesSum", 0,{{$articlesSum}});
            articlesSumCounter.start();


            let bannedUsersSum = new CountUp("bannedUsers", 0,{{$bannedUsersSum}});
            bannedUsersSum.start();


            $('.grid-item1,#articlesStats').on('click', 'li', function () {
                const childName = $(this)[0].className;
                const articlesPerTime = $(this)[0].innerHTML;
                const parent = $(this).parent();
                $(parent).removeClass('show');

                $.ajax(
                    {
                        url: '{{route('articles.index')}}',
                        type: 'get',
                        data: {
                            "totalArticles": childName,
                            "_token": "{{csrf_token()}}"
                        },
                        success: function (successResponse) {

                            var articlesSumCounter = new CountUp("articlesSum", 0, successResponse.articlesSum);
                            articlesSumCounter.start();
                            $('#dropdownMenuButton').html(articlesPerTime);

                        },

                        error: function (errorResponse) {

                        }

                    });

            });

            $('.usersActivity').on('click', function () {

                if ($(this)[0].getAttribute('id') === 'inactiveUsers') { //toggle between active and inactive users
                    $(this).removeAttr('inactiveUsers');
                    $(this).attr('id', 'activeUsers');
                } else {
                    $(this).attr('id', 'inactiveUsers');
                }

                let getUserActivityStatus = $(this)[0].attributes[5].value;

                $.ajax(
                    {
                        url: '{{route('users.index')}}',
                        type: 'get',
                        data: {
                            "userActivity": getUserActivityStatus,
                            "_token": "{{csrf_token()}}"
                        },
                        success: function (successResponse) {
                            if (successResponse.inactiveUsers) {
                                const inactiveUsersSum = new CountUp("activeUsersSum", 0, successResponse.inactiveUsers);
                                inactiveUsersSum.start();
                                $('.usersActivity').html("Inactive users");

                            } else if (successResponse.activeUsers) {
                                const activeUsersSum = new CountUp("activeUsersSum", 0, successResponse.activeUsers);
                                activeUsersSum.start();
                                $('.usersActivity').html("Active users");
                            }

                        },
                        error: function (errorResponse) {

                        }

                    });

            });

            $('.articlesByCategory').on('click', function () {

                const childNode = $(this)[0].nextElementSibling;
                $(childNode).addClass("show");
            });


            $('.grid-item3,#articleByCategoryList').on('click', 'li', function () {

                const articleByCategory = $(this)[0].textContent;
                const parent = $(this).parent();
                $(parent).removeClass('show');

                $.ajax(
                    {
                        url: '{{route('articles.index')}}',
                        type: 'get',
                        data: {
                            "articleByCategory": articleByCategory,
                            "_token": "{{csrf_token()}}"
                        },
                        success: function (successResponse) {
                            var articlesByCategory = new CountUp("articlesByCategorySum", 0, successResponse.articlesByCategorySum);
                            articlesByCategory.start();

                            $('#dropdownMenuButton2').html(articleByCategory);

                        },

                        error: function (errorResponse) {

                        }

                    });
            });
        });
    </script>
@endpush

