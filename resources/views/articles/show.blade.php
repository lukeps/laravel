@extends('layouts.app')
@section('content')
    <div class="container">
        <form method="GET" action="">
            {{ csrf_field() }}
            {{ method_field('POST') }}
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{route('articles.index')}}" class="btn btn-primary float-right" style="background-color: #4aa0e6;">Cofnij</a>
                            <h1>{{$article->name}}</h1>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <p class="lead"> {!! $article->short_text !!}</p>
                            </div>
                            <br>
                            <hr>
                            <div class="form-group">
                                <p class="lead"> {!! $article->text !!}</p>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-6">
                                    <p>Utworzono:</p>
                                    <p style="font-size: 10px;"> {{$article->created_at}}</p>
                                </div>
                                <div class="col-md-6 text-right">
                                    <p>Edytowano:</p>
                                    <p style="font-size: 10px;">{{$article->updated_at}}</p>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
        </form>
    </div>
@endsection