<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet"/>
    <link href="{{ URL::asset('/css/admin_sidebar.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

{{--    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">--}}

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}" type="text/css">


</head>
<body>
<script src="{!! mix('js/app.js') !!}"></script>
<script
    integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30="
    crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>

{{--<div id="app">--}}
{{--    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">--}}
{{--        <div class="container">--}}
{{--            <a class="navbar-brand" href="{{ route('articles.home') }}">--}}
{{--                {{ config('app.name', 'Laravel') }}--}}
{{--            </a>--}}
{{--            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"--}}
{{--                    aria-controls="navbarSupportedContent" aria-expanded="false"--}}
{{--                    aria-label="{{ __('Toggle navigation') }}">--}}
{{--                <span class="navbar-toggler-icon"></span>--}}
{{--            </button>--}}

{{--            <div class="collapse navbar-collapse" id="navbarSupportedContent">--}}
{{--                <!-- Left Side Of Navbar -->--}}
{{--                <ul class=" barContent navbar-nav mr-auto">--}}

{{--                    @if(Auth::check())--}}
{{--                        <li class="nav-item">--}}
{{--                            <a class="nav-link" href="{{ route('articles.index') }}">{{ __('Artykuły') }}</a>--}}
{{--                        </li>--}}

{{--                        <li class="nav-item">--}}
{{--                            <a class="nav-link" href="{{ route('users.index') }}">{{ __('Użytkownicy') }}</a>--}}
{{--                        </li>--}}


{{--                        <li class="nav-item">--}}
{{--                            <a class="nav-link" href="{{ route('categories.index') }}">{{ __('Kategorie') }}</a>--}}
{{--                        </li>--}}
{{--                    @endif--}}
{{--                </ul>--}}

<!-- Right Side Of Navbar -->

<div class=" row" id="headerRow">
    @if(\Illuminate\Http\Request::capture()->getRequestUri() !== '/login')
        @include('GUI_Components.topbar')
</div>
<div>
<div class="row" id="contentRow">
    @include('GUI_Components.sidebarAvatarComponent')
    <div class="dataTable col-10 mt-5" style="margin-left:2%;margin-top: 5.5rem !important;">
        @yield('content')
    </div>
    @else
        <div class="dataTable col-12" style="transform: translateY(20vh)">
            @yield('loginContent')
            @endif

        </div>
</div>
</div>
<!-- Scripts -->
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<!-- overflow-y scroll -  prevents moving page content horizontally by setting scrollbar visible always -->
<style>
    html {
        overflow-y: scroll;
    }

    .card-header {
        font-size: 1.2vw;
    }

</style>

<script>

        @if(Auth::check()){
        var countDownDate = new Date();
        var add15MinutesForward = countDownDate.setMinutes(countDownDate.getMinutes() + 15);

        var x = setInterval(function () {
            document.getElementById("statusDescription").innerHTML = "Online";
            var now = new Date().getTime();
            var distance = add15MinutesForward - now;

            if (distance < 0) {
                document.getElementById("expiryController").src = '{{asset('images/OfflineStatus.png')}}';
                document.getElementById("statusDescription").innerHTML = "Offline";

                clearInterval(x);
            }

        }, 1000);
    }
    @endif

</script>
@stack('js')

</body>
</html>

