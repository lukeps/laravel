@extends('layouts.app')

@section('content')
    <head>
        <link rel="stylesheet" href="{{URL::asset('/css/country-flag.css')}}">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.7.0/main.min.css">
        <link rel="stylesheet" href="https://cdn.anychart.com/releases/8.7.1/css/anychart-ui.min.css"/>
        <link rel="stylesheet"
              href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

    </head>

    <body>
    <div>
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCphhIsQdqYIYgWU2sUQHAGyL_e0On7eXM&libraries=places"
                type="text/javascript"></script>
        <script src="https://cdn.anychart.com/releases/8.7.1/js/anychart-core.min.js" type="text/javascript"></script>
        <script src="https://cdn.anychart.com/releases/8.7.1/js/anychart-map.min.js" type="text/javascript"></script>
        <script src="https://cdn.anychart.com/releases/8.8.0/geodata/custom/world/world.js"></script>


        @include('home_stats')
        <div class="container" style="height: 2vh;"></div>
        <div class=" container col-12" style="padding: 0;margin-top: 2%">
            <div class="row justify-content-center">
                <div class="card col-2" style="right: 0.5vw;max-height: 35vh">
                    <div class="card-header" style="padding-bottom: 0 !important;"><i class="fas fa-smile"
                                                                                      style="margin-right: 5%"></i>User
                        Profile
                    </div>
                    <div class="card-body">
                        <div class="row justify-content-center">
                            <div class="col-6" style="padding: 0">
                                @if(isset(Auth::user()->avatar))
                                    <img src="{{ asset(Auth::user()->avatar) }}" alt="Avatar" class="avatar"
                                         style="width: 100%;border-radius: 50%;">
                                @else
                                    <div class="customAvatar" style="width: 100%; border-radius: 50%;"></div>
                                @endif
                            </div>
                            <div class="col-auto">
                                <h3 class="userRole"
                                    style="font-family: Poppins;text-align: center;margin-bottom:0;margin-top: 5%;font-size: 1.3vw">{{$userRoles[0]}}</h3>
                                <ul class="menu" style="font-family: Poppins;font-size: 0.6vw;margin:0 !important;">
                                    <li id="children1"
                                        style="padding-bottom: 1% !important;padding-top: 10% !important;"><i
                                            class="fas fa-envelope" style="margin-right: 2%"></i>{{$userEmail}}
                                    </li>
                                    <li id="children2"
                                        style="padding-top: 5% !important;padding-bottom: 5% !important;">{{$registeredDate}}</li>
                                    <li id="children3" style="padding-top: 0.5% !important;"></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card col-5 usersChartContainer" style="max-height: 35vh">
                    <div class="card-header usersHub" style="padding-bottom: 0"><i class="fas fa-user-check"
                                                                                   style="margin-right: 2%;"></i>Registered
                        Users
                    </div>
                    <div class="chart-container"
                         style="width: 80%;height: 75%;display: flex;align-items: center;align-self: center;">
                        <canvas id="myChart" width="inherit" height="inherit"></canvas>
                    </div>
                    <div class="dropdown registeredUsersDropdown"
                         style="display: flex;align-self: center;height: 2.5vh">
                        <button class="btn btn-secondary dropdown-toggle registeredUsersButton" type="button"
                                id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="display:flex;
                            align-items: center">
                            Select
                        </button>
                        <ul class="dropdown-menu registeredUsers" aria-labelledby="dropdownMenuButton">
                            <li class="usersPerDay" href="#">Users per Day</li>
                            <li class="usersPerWeek" href="#">Users per Week</li>
                            <li class="usersPerMonth" href="#">Users per Month</li>
                            <li class="usersPerYear" href="#">Users per Year</li>
                        </ul>
                    </div>
                </div>
                <div class="card col-5 userWorldMap" id="worldMap" style="left: 0.5vw;max-height: 35vh">
                    <div class="card-header" style="padding-bottom:0;">
                        <i class="fas fa-globe-africa" style="margin-right: 2%;"></i>Users map
                    </div>
                    <div class="mapContainer">
                    </div>
                </div>
            </div>
            <div class="container" style="margin-top:1.5vh"></div>
            <div class=" container col-12" style="padding:0">
                <div class="row justify-content-center" style="padding-bottom:1vh">
                    <div class="card col-6" style="right: 0.5vw">
                        <div class="card-header"><i class="fas fa-calendar-alt" style="margin-right: 2%"></i>Notebook
                        </div>
                        <div class="card-body" id="calendarCardBody">
                            <div id="dialog"></div>
                            <div id='calendar'>
                            </div>
                        </div>
                    </div>
                    <div class="card col-6" style="left: 0.5vw;padding-bottom: 1%;">
                        <div class="card-header" style="padding-bottom: 0;"><i class="fas fa-cogs"
                                                                               style="margin-right: 2%;"></i>System Logs
                        </div>
                        <div class="card-body" id="logsCardBody" style="margin-top: 4vh">
                            <input type="text" class="form-control" id="logsSearch" name="sysLogsInputSearch"
                                   placeholder="Search...">
                            <div class="table-responsive">
                                <table class="table systemLogs dt-responsive nowrap justify-content-center table-hover">
                                    <thead style="text-align: center;">
                                    <tr>
                                        <th>Date</th>
                                        <th>Time</th>
                                        <th>Message</th>
                                    </tr>
                                    </thead>
                                    <tbody style="text-align: center">
                                    <tr>
                                        <td id="date"></td>
                                        <td id="time"></td>
                                        <td id="message"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <style>

            .systemLogs tr:nth-child(even) {
                background-color: #F8F8F8;
            }

            .table td, table th {
                border-left: 0;
                border-right: 0;
                border-top: 0;
            }

            .card {
                padding-right: 0;
                padding-left: 0;
            }

            .card-header {
                background-color: transparent;
                border: none;
                font-size: 1.1rem;
                font-family: Poppins;
            }

            .registeredUsers.show {
                top: 100% !important;
                transform: unset !important;
            }

            .registeredUsers > li {
                padding-top: unset !important;
                padding-bottom: unset !important;
            }


            #selectpickerDropdown li {
                display: block !important;
                padding-top: 0 !important;
                padding-bottom: 0 !important;

            }

            .menu {
                padding: 0;
            }

            .menu li {
                display: flex;
                align-items: center;
            }

            .menu > li {
                justify-content: center;
            }

            #logsSearch {
                width: 25%;
                float: right;
                height: 3.2vh;
                margin-bottom: 2%;
                margin-top: 2%;
                margin-right: 2%;
            }

            #logsCardBody {
                padding: 0;
            }

            .registeredUsers > li {
                cursor: default;
            }

            input[type="datetime-local"]::-webkit-calendar-picker-indicator {

                margin-left: 0;
            }

            input[type='datetime-local'] {
                font-size: 1vw;
            }

            #selectpickerDropdown {
                position: relative !important;
                transform: unset !important;
                top: unset !important;

            }

            .distinctYears > li {
                padding-top: unset !important;
                padding-bottom: unset !important;
            }

            .monthsList > li {
                padding-top: unset !important;
                padding-bottom: unset !important;

            }

            .quartersList > li {
                padding-top: unset !important;
                padding-bottom: unset !important;

            }

            .deleteEventButton {
                border: 0;
                background-color: transparent;

            }

            #deleteEvents tr {
                border-bottom: 1px solid black !important;
            }
        </style>

        @stack('home_stats_styles')
    </div>
    </body>

@endsection

@push('js')
    <script type="text/javascript" charset="utf8"
            src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script type="text/javascript" charset="utf8"
            src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>
    <script type="text/javascript" charset="utf8"
            src="https://cdn.datatables.net/plug-ins/1.10.21/sorting/datetime-moment.js"></script>
    <script src="{{URL::asset('/js/country-flag.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.7.0/main.min.js"></script>
    <script>


        $(document).ready(function () {

            let chart;

            getCharoplethMapWithData();

            $.fn.dataTable.moment('Do MMMM YYYY');//system logs sorting cron logs by datetime in right order

            //user info dashboard
            const role = $('.userRole')[0].textContent;
            setUserRole(role);

            const userRegisteredDate = '{{$registeredDate}}';
            $('#children2').html('<i class="fas fa-calendar-check" style="margin-right: 2%"></i>' + "Joined at" + " " + moment(userRegisteredDate).format('MMMM Do YYYY'));

            const parent = document.getElementById("children3");
            const userFlag = new CountryFlag(parent);
            userFlag.selectByAlpha2("{{$userNationality}}");
            let flag = document.getElementsByClassName("flag");
            flag[0].style.transform = "scale(0.7)";
            flag[0].style.position = "absolute";
            flag[0].style.marginTop = '10%';


            const ctx = document.getElementById('myChart').getContext('2d');

            //registered users per month - default view after page load
            defaultChart();


            $('.registeredUsers').on('click', 'li', function () {
                const optionName = $(this)[0].innerHTML;
                const trimmedOptionName = optionName.substr(optionName.indexOf(" ") + 1);
                $('.registeredUsersButton').html(trimmedOptionName);
                const dynamicallyButton = $('.usersChartContainer')[0].children[0].children[1];
                if (dynamicallyButton !== undefined) {
                    $(dynamicallyButton).remove();
                }

                if (optionName === 'Users per Day') {

                    const parent = $(this).parent();
                    const usersHub = $('.usersHub');
                    const usersPerDayInMonthButton = createHTMLButtonWithListContent("usersBySingleDayInSingleMonthDropdown", "distinctYears", "monthsList", "Month");
                    $(parent).removeClass('show');

                    if ($('.usersBySingleDayInSingleMonthDropdown')[0] === undefined)
                        $(usersPerDayInMonthButton).insertAfter(usersHub);

                    usersPerDay(promiseUsersPerDay());

                } else if (optionName === 'Users per Week') {

                    const parent = $(this).parent();
                    const usersHub = $('.usersHub');
                    const usersPerWeekInMonthButton = createHTMLButtonWithListContent("usersPerWeek", "distinctYears", "quartersList", "Quarter");
                    $(parent).removeClass('show');

                    if ($('.usersPerWeek')[0] === undefined)
                        $(usersPerWeekInMonthButton).insertAfter(usersHub);

                    usersPerWeek(promiseUsersPerWeek());


                } else if (optionName === 'Users per Month') {
                    defaultChart();
                }

            });


            $('body').on('click', '.usersBySingleDayInSingleMonthDropdown', function () {
                usersPerDay(promiseUsersPerDay())
            });


            $('body').on('click', '.usersPerWeek', function () {
                usersPerWeek(promiseUsersPerWeek())
            });


            const datatable = $('.systemLogs').DataTable({

                "sDom": "ltipr",
                responsive: true,
                pageLength: 5,
                "bLengthChange": false,
                ajax: {
                    type: "GET",
                    url: "{{route('home.cronLogs')}}",
                },
                columns: [
                    {
                        "data": "date",
                        sortable: true
                    },
                    {
                        "data": "time",
                        sortable: false
                    },
                    {
                        "data": "message",
                        sortable: false
                    },
                ],
                data: {},

                order: [0, "desc"],

            });

            $("#logsSearch").on('keyup', function () {

                datatable.search(this.value).draw();
            });


            function setUserRole(role) {
                const userRoleUppercase = role.charAt(0).toUpperCase();
                if (role === 'admin')
                    $('.userRole').html(userRoleUppercase + role.slice(1) + 'istrator');
                else {
                    $('.userRole').html(userRoleUppercase + role.slice(1));
                }
            }

            function createHTMLButtonWithListContent(buttonClassName, ulParent, ulChild, buttonTextContent) {
                const div = document.createElement("div");
                div.style.display = 'inline';
                div.setAttribute('class', 'dropdown' + " " + buttonClassName);
                const button = document.createElement("button");
                button.setAttribute('class', 'btn btn-secondary dropdown-toggle' + " " + buttonClassName);
                button.type = "button";
                button.id = "dropdownMonthButton";
                button.setAttribute('data-toggle', 'dropdown');
                button.setAttribute('aria-haspopup', 'true');
                button.setAttribute('aria-expanded', 'false');
                button.style.lineHeight = '1.25vh';
                button.style.height = '2.5vh';
                button.style.width = 'auto';
                button.style.cssFloat = 'right';
                button.style.marginRight = '2%';
                button.style.marginTop = '1%';
                button.textContent = buttonTextContent;
                button.style.paddingLeft = "1%";

                div.appendChild(button);

                const ulListParent = document.createElement("ul");
                ulListParent.setAttribute('class', 'dropdown-menu' + " " + ulParent);
                ulListParent.setAttribute('aria-labelledby', 'dropdown' + ulParent);
                ulListParent.style.position = 'absolute';
                ulListParent.style.marginLeft = '85%';
                ulListParent.style.minWidth = "0";
                ulListParent.style.width = "7vw";
                ulListParent.style.top = "0";

                const ulListChild = document.createElement("ul");
                ulListChild.setAttribute('class', 'dropdown-menu' + " " + ulChild);
                ulListChild.setAttribute('aria-labelledby', 'dropdown' + ulChild);
                ulListChild.style.position = 'absolute';
                ulListChild.style.minWidth = '0';
                ulListChild.style.marginLeft = '100%';
                ulListChild.style.width = "10vw";
                ulListChild.style.top = "0";
                ulListChild.style.left = "0.2vw";
                ulListChild.style.marginTop = "-0.1vh";

                const distinctYears = {!! $registeredDateYears !!};
                const yearsData = createDataArray(distinctYears);
                for (let i = 0; i < yearsData.length; i++) {
                    ulListParent.appendChild(yearsData[i]);
                }

                if (buttonClassName === "usersBySingleDayInSingleMonthDropdown") {
                    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
                    const data = createDataArray(months);
                    ulListParent.appendChild(ulListChild);
                    for (let i = 0; i < months.length; i++)
                        ulListChild.appendChild(data[i]);
                    div.appendChild(ulListParent);

                    $('.usersHub').append(div);
                    return div.append();
                } else {
                    const quarters = ["Q1", "Q2", "Q3", "Q4"];
                    const data = createDataArray(quarters);
                    ulListParent.appendChild(ulListChild);
                    for (var i = 0; i < quarters.length; i++)
                        ulListChild.appendChild(data[i]);
                    div.appendChild(ulListParent);

                    $('.usersHub').append(div);
                    return div.append();
                }
            }

            function createDataArray(dataArr) {
                const tempArray = [];

                for (let i = 0; i < dataArr.length; i++) {
                    const dataName = dataArr[i];
                    var data = document.createElement("li");
                    data.textContent = dataArr[i];
                    data.style.cursor = 'default';
                    data.setAttribute('class', dataName);
                    tempArray.push(data);
                }

                return tempArray;
            }

            function defaultChart() {
                $.ajax(
                    {
                        url: '{{route('home.getRegisteredUsersSum')}}',
                        type: 'get',
                        data: {},
                        success: function (response) {

                            if (chart) {
                                chart.destroy();
                            }
                            chart = new Chart(ctx, {
                                type: 'line',
                                data: {
                                    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                                    datasets: [{
                                        label: 'Registered Users',
                                        data: response.registeredUsersData,
                                        borderColor: 'rgba(255, 99, 132, 1)',
                                        pointBorderColor: 'rgba(0, 0, 0, 1)',
                                        pointBackgroundColor: 'rgba(0, 0, 0, 1)',
                                        pointRadius: 2.5,
                                        borderWidth: 2.5,
                                        lineTension: 0,
                                        fill: false,

                                    }]
                                },
                                options: {
                                    spanGaps: true,
                                    maintainAspectRatio: false,
                                    scales: {
                                        xAxes: [{
                                            gridLines: {
                                                display: false
                                            },
                                            ticks: {
                                                fontSize: 10,
                                            }
                                        }],
                                        yAxes: [{
                                            ticks: {
                                                stepSize: 1,
                                                beginAtZero: true,

                                            }
                                        }]
                                    }
                                }
                            });
                        },


                        error: function (errorResponse) {

                        }

                    });
            }


            function usersPerDay(callback) {

                callback.then(function (resolve) {

                    const year = resolve[0]
                    const month = resolve[1]

                    $.ajax(
                        {
                            url: '{{route('home.usersPerDay')}}',
                            type: 'POST',
                            data: {

                                "year": year,
                                "month": month,
                                "_token": "{{csrf_token()}}"

                            },

                            success: function (successResponse) {

                                if (chart) {
                                    chart.destroy();
                                }
                                chart = new Chart(ctx, {
                                    type: 'line',
                                    data: {
                                        labels: successResponse.monthDays,
                                        datasets: [{
                                            label: successResponse.monthName,
                                            data: successResponse.usersSum,
                                            borderColor: 'rgba(255, 99, 132, 1)',
                                            pointBorderColor: 'rgba(0, 0, 0, 1)',
                                            pointBackgroundColor: 'rgba(0, 0, 0, 1)',
                                            pointRadius: 2.5,
                                            borderWidth: 2.5,
                                            lineTension: 0,
                                            fill: false,
                                        }]
                                    },
                                    options: {
                                        maintainAspectRatio: false,
                                        tooltips: {
                                            callbacks: {
                                                title: function (tooltipItem) {

                                                    const day = function () {
                                                        if (tooltipItem[0].label < 10)
                                                            return "0" + tooltipItem[0].label;
                                                        else {
                                                            return tooltipItem[0].label;
                                                        }
                                                    }(); // () immediate execution to get result for further retrieve  in that callback

                                                    const date = day + " " + month + " " + year;
                                                    return tooltipItem[0].label = moment(date).format('MMMM Do YYYY');

                                                },

                                                label: function (tooltipItem) {
                                                    return 'Registered users:' + " " + tooltipItem.yLabel;

                                                },
                                            },
                                        },
                                        spanGaps: true,

                                        scales: {
                                            xAxes: [{
                                                ticks: {
                                                    beginAtZero: true,
                                                    autoSkip: true,
                                                    maxTicksLimit: successResponse.monthDays.length,
                                                    fontSize: 10,

                                                },
                                                gridLines: {
                                                    display: false,
                                                }
                                            }],
                                            yAxes: [{
                                                ticks: {
                                                    stepSize: 1,
                                                    beginAtZero: true,
                                                }
                                            }]
                                        }
                                    }
                                });

                            },
                            error: function (errorResponse) {

                            }

                        });
                });

            }

            function usersPerWeek(callback) {

                callback.then(function (resolve) {

                    const year = resolve[0];
                    const quarter = resolve[1];
                    const quarterMonths = [];
                    const quarterPeriod = getQuarterRange(quarter, year);
                    const strChar = quarter.split("");
                    const quarterNumber = parseInt(strChar[1])

                    for (var i = 0; i < 3; i++) {
                        let month = moment().quarter(quarterNumber).startOf('quarter').add(i, 'months').format('MMMM');
                        quarterMonths.push(month);
                    }

                    $.ajax(
                        {
                            url: '{{route('home.getUsersSumFromDatesPeriod')}}',
                            type: 'POST',
                            data: {
                                "quarterPeriod": quarterPeriod,
                                "_token": "{{csrf_token()}}"
                            },

                            success: function (successResponse) {
                                const tempArray = [];
                                const jsonData = [];
                                const midMonth = quarterMonths[1];

                                for (let i = 0; i < successResponse.data.length; i++) {

                                    jsonData.push(successResponse.data[i].usersSum);
                                    if (i === 0) {
                                        tempArray.push(quarterMonths[0])
                                    } else if (i === successResponse.data.length - 1) {
                                        tempArray.push(quarterMonths[2])
                                    } else {

                                        tempArray.push(" ");
                                    }
                                }

                                if (chart) {

                                    chart.destroy();
                                }

                                chart = new Chart(ctx, {
                                    type: 'line',
                                    data: {
                                        labels: tempArray,
                                        datasets: [{
                                            label: quarter,
                                            data: jsonData,
                                            borderColor: 'rgba(255, 99, 132, 1)',
                                            pointBorderColor: 'rgba(0, 0, 0, 1)',
                                            pointBackgroundColor: 'rgba(0, 0, 0, 1)',
                                            pointRadius: 2.5,
                                            borderWidth: 2.5,
                                            lineTension: 0,
                                            fill: false,
                                        }]
                                    },
                                    options: {
                                        layout: {
                                            padding: 5
                                        },
                                        maintainAspectRatio: false,
                                        animation: {
                                            duration: 0,
                                            onComplete: function () {
                                                const controller = this.chart.controller;
                                                const chart = controller.chart;
                                                const xAxis = controller.scales['x-axis-0'];

                                                xAxis.ticks.forEach(function (value, index) {

                                                    if (index === Math.floor(successResponse.data.length / 2 - 1)) {
                                                        const x = chart.width / 2;
                                                        const y = xAxis._labelItems[0].y;
                                                        ctx.save(); //save default canvas context styling
                                                        ctx.translate(x, y); //then do some transformations with text
                                                        ctx.rotate(-0.8387251712631526);
                                                        ctx.font = "10px Helvetica Neue, Helvetica, Arial, sans-serif";
                                                        ctx.fillStyle = '#666';
                                                        ctx.textAlign = 'right';
                                                        ctx.fillText(midMonth, 0, 0); //apply changes to default chart ctx
                                                        ctx.restore(); //at the end get back to default ctx state to prevent of redrawing chart on changed context

                                                    }

                                                });
                                            }
                                        },
                                        hover: {
                                            animationDuration: 0 // duration of animations when hovering an item
                                        },
                                        responsiveAnimationDuration: 0, // animation duration after a resize
                                        tooltips: {
                                            callbacks: {
                                                title: function (tooltipTitle) {
                                                    const currentData = successResponse.data[tooltipTitle[0].index];

                                                    return "Week period:" + " " + '\n' + '\n' + 'from:' + " " + currentData.startWeek + '\n' +
                                                        "to:" + " " + currentData.endWeek

                                                },

                                                label: function (description) {
                                                    return "Registered users:" + " " + description.yLabel;
                                                }
                                            },
                                        },
                                        spanGaps: true,

                                        scales: {
                                            xAxes: [{
                                                ticks: {
                                                    padding: 5,
                                                    beginAtZero: true,
                                                    autoSkip: true,
                                                    fontSize: 10
                                                },
                                                gridLines: {
                                                    display: false,
                                                }
                                            }],
                                            yAxes: [{
                                                ticks: {
                                                    stepSize: 1,
                                                    beginAtZero: true,
                                                }
                                            }]
                                        }
                                    }
                                });

                                quarterMonths.length = 0;


                            },
                            error: function (errorResponse) {

                            }

                        });

                });

            }


            function promiseUsersPerDay() {

                return new Promise(function (resolve) { // using promise ensures retrieving dynamically created button

                    $('body').on('mouseenter mouseleave', '.distinctYears > li', function (e) {

                        $('.monthsList').show();
                        if (e.type === 'mouseleave')
                            var validDate = e.target.innerHTML;

                        $('body').on('mouseenter mouseleave', '.monthsList > li', function (e) {
                            if (e.type === 'mouseenter') {
                                $('.monthsList').show();

                                this.currentDate = validDate

                                let validMonth = e.target.innerHTML;

                                $(this).on('click', function () {
                                    const currentDate = this.currentDate
                                    return resolve([currentDate, validMonth]);

                                })
                            }
                            if (e.type === 'mouseleave') {
                                $('.monthsList').hide();
                            }
                        })
                    });

                });


            }


            function promiseUsersPerWeek() {

                return new Promise(function (resolve) { // using promise ensures retrieving dynamically created button

                    $('body').on('mouseenter mouseleave', '.distinctYears > li', function (e) {

                        $('.quartersList').show();
                        if (e.type === 'mouseleave')
                            var validDate = e.target.innerHTML;

                        $('body').on('mouseenter mouseleave', '.quartersList > li', function (e) {
                            if (e.type === 'mouseenter') {
                                $('.quartersList').show();

                                this.currentDate = validDate

                                let validMonth = e.target.innerHTML;

                                $(this).on('click', function () {

                                    const currentDate = this.currentDate
                                    return resolve([currentDate, validMonth]);

                                })
                            }
                            if (e.type === 'mouseleave') {
                                $('.quartersList').hide();
                            }
                        })
                    });

                });


            }

            function getQuarterRange(quarter, year) {
                const strChar = quarter.split("");

                let start = moment().quarter(parseInt(strChar[1])).startOf('quarter');
                let end = moment().quarter(parseInt(strChar[1])).endOf('quarter');
                start.set('year', year);
                end.set('year', year);
                start = moment(start).format('YYYY-MM-DD');
                end = moment(end).format('YYYY-MM-DD');

                return {start, end};
            }


            function getCharoplethMapWithData() {

                const promise = new Promise(function (resolve) {

                    $.ajax({

                        url: '{{route('home.getUsersByNationalities')}}',
                        type: 'POST',
                        data: {"_token": "{{csrf_token()}}"},

                        success: function (successResponse) {

                            resolve(successResponse);

                        },
                    });

                });

                promise.then(function (result) {

                    var map = anychart.map();

                    // create data set
                    var dataSet = anychart.data.set(result);
                    // create choropleth series

                    series = map.choropleth(dataSet);

                    series.tooltip().format(function (e) {

                        return "Code:" + " " + e.getData("id") + "\n" +
                            "Registered users: " + e.getData("value")
                    })
                    map.background().fill('#00000000');
                    // set geoIdField to 'id', this field contains in geo data meta properties
                    series.geoIdField('id');

                    // set map color settings
                    // series.colorScale(anychart.scales.linearColor('#deebf7', '#ff6384'));
                    // series.colorScale(anychart.scales.ordinalColor([{less:1},{from:1, to:2},
                    //     {from:3, to:4}, {from:5, to:6},{from:7, to:8},{greater:9}]));
                    // ocs.colors(['#ffeff2', '#ffd0da', '#ffb1c1','#ff91a8','#ff7290','#e55976']);
                    // color scale ranges

                    series.colorScale(anychart.scales.ordinalColor([{less: 0, color: '#ffeff2'}, {
                        from: 1,
                        to: 2,
                        color: '#ffd0da'
                    }
                        , {from: 3, to: 4, color: '#ffb1c1'}, {from: 5, to: 6, color: '#ff91a8'}, {
                            from: 7,
                            to: 8,
                            color: '#ff7290'
                        }, {greater: 9, color: '#e55976'}]));
                    series.hovered().fill('#662734');
                    // set geo data, you can find this map in our geo maps collection
                    // https://cdn.anychart.com/#maps-collection
                    map.geoData(anychart.maps['world']);

                    //set map container id (div)
                    const stage = anychart.graphics.create("worldMap");
                    const parent = stage.Gd.parentNode;
                    parent.style.transform = "scale(0.8)";
                    parent.style.position = "absolute";
                    parent.style.marginTop = "3%";
                    //initiate map drawing
                    map.colorRange(true);
                    map.container(stage).draw();

                });
            }


            const calendarEl = document.getElementById('calendar'); //full calendar plugin
            let isCalled = {called: false};
            let tempEventsArr = [];
            const isCalledInAjaxResponse = true;
            const calendar = new FullCalendar.Calendar(calendarEl,
                {

                    timeZone: 'UTC',
                    initialView: 'dayGridMonth',
                    aspectRatio: 2,
                    themeSystem: 'bootstrap',
                    height: '100%',
                    customButtons: { //add event
                        myCustomButton: {
                            bootstrapFontAwesome: 'fas fa-plus',
                            click: function () {
                                let dialog = $('#dialog').dialog({
                                    modal: true,
                                    appendTo: $('#calendar'),
                                    width: '100%',
                                    position: {my: "top center", at: "top top ", of: 'body'},
                                    open: function (event) {
                                        if ($('#deleteEvents').length) { //remove delete event dialog box in this case from the DOM
                                            $('#deleteEvents').remove();
                                            $('#logsSearch').remove();
                                            $('#logsCardBody').remove();
                                            isCalled.called = false;
                                        }
                                        $('.ui-dialog-titlebar-close').removeClass().addClass('ui-icon ui-icon-closethick').css({
                                            'outline': 'none',
                                            'border': 'none',
                                            'background-color': 'inherit'
                                        }); //styling dialog header
                                        $('.ui-dialog-titlebar').css({
                                            'display': 'flex',
                                            'align-items': 'center',
                                            'justify-content': 'flex-end'
                                        })
                                        $('.ui-dialog').css({'top': '-60vh'})
                                        $(this).prepend('<div id="errorBag"></div>')

                                        let dialogBox = $(event)[0].target;
                                        if (!$('#calendarEvent').length) {
                                            $(dialogBox).append('<form id="calendarEvent" method="post">\n' +
                                                '@csrf\n' +
                                                '  <div class="form-group">\n' +
                                                '    <label for="inputEventTitle"></label>\n' +
                                                '    <input type="text" class="form-control shadow-none" id="inputEventTitle" placeholder="Add title" style="border-left: 0;border-right: 0;border-top: 0">\n' +
                                                '  </div>\n' +
                                                '  <div class="form-row">\n' +
                                                '    <div class="form-group col-md-6">\n' +
                                                '      <label for="inputStartEvent">Start</label>\n' +
                                                '      <input type="datetime-local" class="form-control" id="inputStartEvent">\n' +
                                                '    </div>\n' +
                                                '    <div class="form-group col-md-6">\n' +
                                                '      <label for="inputEndEvent">End</label>\n' +
                                                '      <input type="datetime-local" class="form-control" id="inputEndEvent">\n' +
                                                '    </div>\n' +
                                                '  </div>\n' +
                                                '  <div class=" form-group"style= "display: grid">\n' +
                                                '    <label for="selectUsersNames">Add member</label>\n' +
                                                '    <select class="selectpicker col-md-6" multiple data-actions-box="true" >\n' +
                                                '@foreach ($users as $user)<option>{{$user->name}}</option> @endforeach \n' +
                                                '  </select></div>\n' +
                                                '  <div class=" form-group"style= "display: grid">\n' +
                                                '    <label for="selectEventType">Event type</label>\n' +
                                                '    <select class="selectpicker2 col-md-6" title="Nothing selected" multiple data-max-options="1">\n' +
                                                '@foreach ($events as $event)<option>{{$event->type}}</option> @endforeach \n' +
                                                '  </select></div>\n' +
                                                '    <div class="form-group col-md-6" style ="display : grid;padding : 0">\n' +
                                                '      <label for="inputLocation">Set location</label>\n' +
                                                '      <input type="text" id="inputAutocomplete" placeholder="Choose..." style="outline: none;border-left: 0;border-right: 0;border-top: 0;">\n' +
                                                '    </div>\n' +
                                                '  </div>\n' +
                                                '  <button type="submit" class="btn btn-primary" id="addEvent" style=" background-color : rgba(255, 99, 132, 1);border:none;float : right">Sign in</button>\n' +
                                                '</form>');
                                        }

                                        $('.selectpicker').selectpicker();
                                        $('.selectpicker2').selectpicker();

                                        let dropdown = $('.selectpicker')[0].offsetParent.children[2];
                                        let dropdown2 = $('.selectpicker2')[0].offsetParent.children[2];
                                        $(dropdown).attr('id', 'selectpickerDropdown');
                                        $(dropdown2).attr('id', 'selectpickerDropdown');

                                        $('.dropdown-menu.inner').css({'max-height': '18vh', 'font-size': '1vw'});

                                        $('.bootstrap-select').css({'padding': '0'});

                                        $('.bootstrap-select').on('click', function () {

                                            if ($(this)[0].children[0].className === 'selectpicker col-md-6') {
                                                $('.selectpicker').selectpicker('toggle');
                                            } else {
                                                $('.selectpicker2').selectpicker('toggle');

                                            }
                                        })

                                        new google.maps.places.Autocomplete(document.getElementById('inputAutocomplete'));

                                        $('#addEvent').on('click', function (e) {

                                            const token = $('#calendarEvent')[0][0].defaultValue;
                                            const eventTitle = $('#inputEventTitle').val();
                                            const startEvent = $('#inputStartEvent').val();
                                            const endEvent = $('#inputEndEvent').val();
                                            const members = $('.selectpicker').val();
                                            const events = $('.selectpicker2').val();
                                            const location = $('#inputAutocomplete').val()
                                            e.preventDefault();

                                            $.ajax(
                                                {

                                                    url: "{{route('home.storeCalendarEvent')}}",
                                                    type: 'POST',
                                                    data: {
                                                        "_token": token,
                                                        'eventTitle': eventTitle,
                                                        'startEvent': startEvent,
                                                        'endEvent': endEvent,
                                                        'members': members,
                                                        'events': events,
                                                        'location': location
                                                    },

                                                    success: function (response) {
                                                        $(dialog).empty()
                                                        $('.ui-widget-overlay').remove()
                                                        $('.ui-front').remove()
                                                        calendar.refetchEvents()
                                                        openDeleteEventDialogBox(isCalled, dialogBox,isCalledInAjaxResponse);
                                                        const rows = $('#deleteEvents')[0].children[1].children.length;
                                                        let lastRow = $('#deleteEvents')[0].children[1].children[rows - 1];
                                                        let clonedRow = lastRow.cloneNode(true);
                                                        let freshEvent = response.userEvents[response.userEvents.length - 1];
                                                        const tableRow = document.createElement("tr");
                                                        const id = freshEvent.id;
                                                        for (let cell of clonedRow.cells) {
                                                            let attrName = cell.attributes[0].value;
                                                            for (const [key, value] of Object.entries(freshEvent)) {
                                                                if (attrName === key) {
                                                                    const tableCell = document.createElement('td');

                                                                        const text = document.createTextNode(value);
                                                                        tableCell.setAttribute('id', key.toString())
                                                                        tableCell.appendChild(text);
                                                                        tableRow.appendChild(tableCell);
                                                                        if(key.toString() === 'end_event'){
                                                                            const tableCell = document.createElement('td');
                                                                            const button = document.createElement('button');
                                                                            const span = document.createElement('span');
                                                                            button.setAttribute('style',' flex: 1 1 0px;margin-top:0 !important;');
                                                                            button.setAttribute('name',' deleteButton');
                                                                            button.setAttribute('style',' flex: 1 1 0px;margin-top:0 !important;');
                                                                            button.setAttribute('class',' deleteEventButton');
                                                                            button.setAttribute('type','button');
                                                                            button.setAttribute('data-id',id);
                                                                            span.setAttribute('class',' fas fa-trash alt');
                                                                            span.setAttribute('aria-hidden','true');
                                                                            button.appendChild(span);
                                                                            tableCell.appendChild(button);
                                                                            tableRow.appendChild(tableCell);
                                                                        }
                                                                }
                                                            }
                                                        }
                                                        $(dialogBox).hide();
                                                        let tbody = $('#deleteEvents').find('tbody');
                                                        $(tbody)[0].appendChild(tableRow);

                                                    },


                                                    error: function (response) {

                                                        const jsonStr = JSON.stringify(response);
                                                        const obj = JSON.parse(jsonStr);
                                                        const errorMessages = obj.responseJSON;
                                                        if ($('#errorBag')[0].children.length > 0)
                                                            $('#errorBag').empty()
                                                        if (("eventTitle" in errorMessages)) $("#errorBag").append("<div class='alert alert-danger'>" + errorMessages["eventTitle"] + "</div>")
                                                        if (("startEvent" in errorMessages)) $("#errorBag").append("<div class='alert alert-danger'>" + errorMessages["startEvent"] + "</div>")
                                                        if (("endEvent" in errorMessages)) $("#errorBag").append("<div class='alert alert-danger'>" + errorMessages["endEvent"] + "</div>")
                                                        if (("members" in errorMessages)) $("#errorBag").append("<div class='alert alert-danger'>" + errorMessages["members"] + "</div>")
                                                        if (("events" in errorMessages)) $("#errorBag").append("<div class='alert alert-danger'>" + errorMessages["events"] + "</div>")
                                                        if (("location" in errorMessages)) $("#errorBag").append("<div class='alert alert-danger'>" + errorMessages["location"] + "</div>")
                                                    }

                                                });
                                        })
                                    },

                                    close: (event) => {
                                        let dialogBox = $(event)[0].target;
                                        $(dialogBox).hide();

                                    },
                                });
                            },
                        },
                        myCustomButton2: { //delete event
                            bootstrapFontAwesome: 'fas fa-trash alt',
                            click: function () {
                                $('#dialog').dialog({
                                    modal: true,
                                    appendTo: $('#calendar'),
                                    width: '200%',
                                    position: {my: "top center", at: "top top ", of: 'body'},
                                    open: function (event) { //dodac flage jesli poszlo cos z response add event to delete events show...
                                        //if firstly success response from add event then isCalled is set to true.
                                        if ($('#calendarEvent')) { //remove add event dialog box from DOM <- at this moment is hided
                                            $('#calendarEvent').remove()
                                        }

                                        $('.ui-dialog-titlebar-close').removeClass().addClass('ui-icon ui-icon-closethick').css({
                                            'outline': 'none',
                                            'border': 'none',
                                            'background-color': 'inherit'
                                        }); //styling dialog header
                                        $('.ui-dialog-titlebar').css({
                                            'display': 'flex',
                                            'align-items': 'center',
                                            'justify-content': 'flex-end'
                                        })
                                        $('.ui-dialog').css({'top': '-60vh'})
                                        $(this).prepend('<div id="errorBag"></div>')

                                        let dialogBox = $(event)[0].target;

                                        openDeleteEventDialogBox(isCalled, dialogBox) //jesli

                                        $('.deleteEventButton').on('click', function (e) {

                                            const id = $(this).attr('data-id');
                                            const tableRow = $(this)[0].parentNode.parentNode
                                            e.preventDefault();
                                            if (confirm("Are you sure to delete this event ?")) {
                                                $.ajax({
                                                    url: 'home/' + id,
                                                    type: 'DELETE',

                                                    data: {
                                                        "_token": "{{csrf_token()}}",

                                                    },
                                                    success: function () {
                                                        tableRow.remove();
                                                        alert("Successfully deleted event.")
                                                    },

                                                    error: function () {
                                                        alert("Something went wrong. Let's try again later.");
                                                    },
                                                });
                                            }
                                        })
                                    },

                                    close: (event) => {
                                        const dialogBox = $(event)[0].target;
                                        const actualValue = $(dialogBox)[0].innerHTML;
                                        tempEventsArr.push(actualValue);
                                        $(dialogBox).hide(); //hide this dialog box, for further processing in "add event" ajax call
                                    },
                                });
                            },

                        }
                    },
                    default: {
                        close: 'fa-times',
                        prev: 'fa-chevron-left',
                        next: 'fa-chevron-right',
                        center: 'title',
                        prevYear: 'fa-angle-double-left',
                        nextYear: 'fa-angle-double-right',
                    },
                    headerToolbar: {
                        left: 'prevYear,nextYear,prev,next',
                        center: 'title',
                        right: 'dayGridMonth,timeGridWeek,listWeek,myCustomButton,myCustomButton2'
                    },

                    events: function (fetchInfo, successCallback) {
                        const events = [];
                        let events2 = [];
                        const all = [];
                        $.ajax({
                            url: 'https://s3.eu-central-1.amazonaws.com/admin.panel.content/cron+logs/logs',
                            type: 'GET',
                            success: function (res) {
                                const jsonData = cutFromString(res)
                                jsonData.forEach(function (el) {
                                    const offsetDate = moment(el.date).utcOffset(120) //offset date +2h to original time
                                    el.date = offsetDate._d;
                                    fetchInfo.start = moment(el.date).toISOString()
                                    fetchInfo.end = moment(el.date).toISOString()
                                    events.push({
                                        start: fetchInfo.start,
                                        end: fetchInfo.end,
                                        title: "System check"
                                    });

                                })
                            },
                            complete: function () {
                            }
                        });

                        $.ajax({
                            url: '{{route('home.getUserEvents')}}',
                            type: 'GET',
                            success: function (res) {
                                res.events.forEach(function (el) {

                                    fetchInfo.start = moment(el.start_event).toISOString()
                                    fetchInfo.end = moment(el.end_event).toISOString()
                                    fetchInfo.title = el.event_type

                                    events2.push({

                                        start: fetchInfo.start,
                                        end: fetchInfo.end,
                                        title: fetchInfo.title,
                                        id: el.id

                                    });
                                });


                            },
                            complete: function () {
                                return successCallback(all.concat(events, events2));
                            }

                        });


                    },

                    eventTimeFormat: { // like '14:30:00'
                        hour: '2-digit',
                        minute: '2-digit',
                        meridiem: false,
                        hour12: false,
                    },
                    eventColor: 'rgba(255, 99, 132, 1)',

                    eventDidMount: function (el) {
                        let viewType = $(el)[0].view.type
                        switch (viewType) {
                            case('dayGridMonth'):
                                $(el)[0].el.style.display = 'block';
                                break;
                            case('timeGridWeek'):
                                $(el)[0].el.style.fontSize = 'x-small';
                                break;
                        }


                    },

                    eventClick: function (el) {
                        if (confirm("Are you sure to delete this event ?")) {
                            const eventID = el.event._def.publicId
                            $.ajax(
                                {
                                    url: 'home/' + eventID,
                                    type: 'DELETE',

                                    data: {
                                        "_token": "{{csrf_token()}}",
                                    },

                                    success: function () {
                                        calendar.refetchEvents()
                                        alert("Event was deleted successfully.")
                                    },

                                    error: function (response) {
                                    }

                                });
                        }
                    },

                    loading: function (bool) {

                        if (!bool) {
                            const leftButtonsSum = this.el.children[0].children[0].children[0].children.length;
                            const rightButtonsSum = this.el.children[0].children[2].children[0].children.length;
                            setLeftButtonConfig(this, leftButtonsSum);
                            setRightButtonConfig(this, rightButtonsSum);
                            setCenterPartOfCalendarHeader(this)


                        }

                    }

                });
            calendar.render();


            function cutFromString(str) {
                const arr = [];
                let jsonObj = {};
                const numberOfLineBreaks = (str.match(/\n/g) || []).length;
                const num = /\d/;
                let lastIndexInText = str.lastIndexOf("\n") + 1;

                for (let i = 0; i < numberOfLineBreaks; i++) {

                    let endPos = str.indexOf('.') + 1
                    let firstNumberInStr = str.match(num);
                    firstNumberInStr = str.indexOf(firstNumberInStr)
                    let lastNumberInStr = str.indexOf("\n")
                    let sliceMessage = str.slice(0, endPos);
                    let date = str.slice(firstNumberInStr, lastNumberInStr);
                    arr.push(jsonObj = {
                        message: sliceMessage,
                        date: date
                    })
                    str = str.slice(lastNumberInStr + 1, lastIndexInText)
                }
                return arr
            }

            function setLeftButtonConfig(obj, childrenSum) { //set style of left buttons at calendar header
                const min = window.matchMedia('(min-width: 768px)')
                const max = window.matchMedia('(max-width: 1024px)')
                for (let i = 0; i < childrenSum; i++) {
                    obj.el.children[0].children[0].children[0].children[i].style.backgroundColor = 'unset'
                    obj.el.children[0].children[0].children[0].children[i].style.borderColor = 'transparent'
                    obj.el.children[0].children[0].children[0].children[i].style.color = 'rgba(255, 99, 132, 1)'
                    obj.el.children[0].children[0].children[0].children[i].style.transform = 'scale3d(0.8,0.8,0.8)'
                    if (min.matches && max.matches) //if width is between 768px and 1024px
                        obj.el.children[0].children[0].children[0].children[i].style.padding = '0.5vw'

                }

            }

            function setRightButtonConfig(obj, childrenSum) { //set style of left buttons at calendar header

                const min = window.matchMedia('(min-width: 768px)')
                const max = window.matchMedia('(max-width: 1024px)')
                for (let i = 0; i < childrenSum; i++) {
                    obj.el.children[0].children[2].children[0].children[i].style.backgroundColor = 'unset'
                    obj.el.children[0].children[2].children[0].children[i].style.borderColor = 'transparent'
                    obj.el.children[0].children[2].children[0].children[i].style.fontFamily = 'Poppins'
                    if (min.matches && max.matches) {

                        obj.el.children[0].children[2].children[0].children[i].style.padding = '0'
                    } else {
                        obj.el.children[0].children[2].children[0].children[i].style.padding = '0.5vw'

                    }

                    if (i < childrenSum - 2) {
                        obj.el.children[0].children[2].children[0].children[i].style.color = 'black'
                        obj.el.children[0].children[2].children[0].children[i].style.fontWeight = '700'
                        obj.el.children[0].children[2].children[0].children[i].style.fontSize = '1vw'

                    } else { //set add button "+"
                        obj.el.children[0].children[2].children[0].children[i].style.color = 'rgba(255, 99, 132, 1)'
                        obj.el.children[0].children[2].children[0].children[i].style.transform = 'scale3d(0.8,0.8,0.8)'
                    }
                }

            }

            function setCenterPartOfCalendarHeader(obj) {

                if (window.matchMedia('(min-width: 1024px)')) {
                    obj.el.children[0].children[1].children[0].style.display = 'flex'
                    obj.el.children[0].children[1].children[0].style.justifyContent = 'center'
                    obj.el.children[0].children[1].style.fontFamily = 'Poppins'
                    obj.el.children[0].children[1].style.fontSize = '1vw'
                    obj.el.children[0].children[1].style.width = '100%'


                }

            }

            function openDeleteEventDialogBox(isCalled, dialogBox,isCalledOptional = undefined) { //isCalledOptional refers to called function in ajax response, is helper in case when firstly delete events is open, then add event and then response
                if (!isCalled.called && isCalledOptional === undefined) {
                    if(!$('#deleteEvents').length) {
                        $(dialogBox).append('<div class="card-body" id="logsCardBody" style="margin-top: 4vh">\n' +
                            '<input type="text" class="form-control" id="logsSearch" name="sysLogsInputSearch" placeholder = "Search..." >\n' +
                            '<div class="table-responsive">\n' +
                            '<table class=" dt-responsive wrap justify-content-center table-hover dataTable no-footer" id="deleteEvents" role="grid" aria-describedby="DataTables_Table_0_info">\n' +
                            '<thead style="text-align: center;">\n' +
                            '<tr>\n' +
                            '<th>ID</th>\n' +
                            '<th>Title</th>\n' +
                            '<th>Member</th>\n' +
                            '<th>Type</th>\n' +
                            '<th>Location</th>\n' +
                            '<th>Start</th>\n' +
                            '<th>End</th>\n' +
                            '<th></th>\n' +
                            '</tr>\n' +
                            '</thead>\n' +
                            '<tbody style="text-align: center">\n' +
                            '@foreach ($userEvents as $event)\n' +
                            '<tr>\n' +
                            '<td id="id">{{$event->id}}</td>\n' +
                            '<td id="title">{{$event->title}}</td>\n' +
                            '<td id="member">{{$event->member}}</td>\n' +
                            '<td id="event_type">{{$event->event_type}}</td>\n' +
                            '<td id="location">{{$event->location}}</td>\n' +
                            '<td id="start_event">{{$event->start_event}}</td>\n' +
                            '<td id="end_event">{{$event->end_event}}</td>\n' +
                            '<td id="deleteEvent"><button style= " flex: 1 1 0px;margin-top:0 !important;" name="deleteButton" class="deleteEventButton" type="button" data-id = {{$event->id}}  class="btn btn-default"><span class="fas fa-trash alt"aria-hidden="true"></span></button></td>\n' +
                            '</tr>\n' +
                            '@endforeach\n' +
                            '</tbody>\n' +
                            '</table>\n' +
                            '</div>\n' +
                            '</div>');
                        isCalled.called = true;
                    }
                    else{
                        $(dialogBox).show()
                    }
                } else {

                    if($('#deleteEvents').length || isCalledOptional === true){
                        $(dialogBox).show();
                    }

                }

                if (tempEventsArr.length !== 0) {
                    $(dialogBox).html(tempEventsArr[0])
                    tempEventsArr = [];
                }
            }

        });

    </script>

    @stack('home_stats_js')
@endpush
