@extends('layouts.app')

@section('content')

    <div class="container">
        <link href="{{ URL::asset('/css/admin_panel_buttons_styles.css') }}" rel="stylesheet" type="text/css">
        <form method="POST" id="updateCategory" action="{{ route('categories.update', $category->id)}}">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-body">
                                <div class="form-group">
                                    <div id="errorNameField"></div>
                                    <label for="name">Podaj tytuł</label>
                                    <input type="text" class="form-control" id="name"  name="name" value="{{$category->name}}">
                                </div>
                                <hr>
                                <button id="editCategoryButton" class="btn btn-primary float-right" type="submit" style="background-color: mediumseagreen;">Wyślij</button>
                                <a href="{{route('categories.index')}}" class=" editBackButton btn btn-secondary"  style="background-color: #4aa0e6;">Cofnij</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection

@push('js')
<script>
    $(document).ready(function () {

        $('#updateCategory').submit('#editCategoryButton', function (e) {

            e.preventDefault();

            let categoryTitleInput = $('#name').val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax(
                {
                    url: '{{route('categories.update',$category->id)}}',
                    type: 'PATCH',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        'name': categoryTitleInput,
                    },

                    success: function () {

                        window.location.replace('/admin/categories'); //redirect to main page after success ajax call
                    },

                    error: function (response) {

                        const jsonStr = JSON.stringify(response);
                        const obj = JSON.parse(jsonStr);
                        const errorMessages = obj.responseJSON;

                        ("name" in errorMessages) ? $("#errorNameField").html("<div class='alert alert-danger'>" + errorMessages["name"] + "</div>").show()
                            : $("#errorNameField").hide();

                    }

                });
        });

    });

</script>
@endpush

