@extends('layouts.app')
@section('content')
    <div class="container">
        <form method="GET" action="">
            {{ csrf_field() }}
            {{ method_field('POST') }}
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{route('categories.index')}}" class="btn btn-primary float-right" style="background-color: #4aa0e6;">Cofnij</a>
                            <h1>{{$category->name}}</h1>
                        </div>
                        </div>
                    </div>
                </div>
        </form>
    </div>
@endsection