@extends('layouts.app')
@section('content')
        <link href="{{ URL::asset('/css/admin_panel_buttons_styles.css') }}" rel="stylesheet" type="text/css" >
        <link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"/>
            <div class="col-md-12">
                <div class="card ">
                    <div class="card-header">Lista kategorii
                        <a href="{{route('categories.create')}}" class=" addCategoryButtonLink btn btn-primary float-right" style="background-color: mediumseagreen">Dodaj kategorie</a>
                    </div>
                    @if(session('status_msg'))
                        <div class="alert alert-success">
                            <p class="text-center"> {{session('status_msg')}}</p>
                        </div>
                    @endif
                    <div class="card-body">
                        <div class="table-responsive table-hover">
                        <table class="table table-striped table-bordered dt-responsive nowrap ajaxTableCategories" style= "margin: 0px auto">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Tytuł kategorii</th>
                                <th style="width:10%">Wybierz</th>
                            </tr>
                            </thead>
                            <thead>
                            <td><input class="form-control" id="column0_search" type="text" placeholder="Szukaj...">
                            </td>
                            <td><input class="form-control" id="column1_search" type="text" placeholder="Szukaj...">
                            </td>
                            <td>
                            </td>
                            </thead>
                        </table>
                        </div>
                        <div class="container">
                            <br>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
@endsection

@push('js')
    <script type="text/javascript" src="{{ URL::asset('/js/datatables.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('/js/dataTables.bootstrap4.js') }}"></script>
    <script>

        $(document).ready(function () {

            var table = $('.ajaxTableCategories').DataTable({
                "sDom": "ltipr",
                serverSide: true,
                select: true,
                paging: true,
                pageLength: 5,
                lengthMenu: [5, 10, 15, 20, 25],
                stateSave: false,
                searching: true,
                processing: true,
                ajax: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    url: "{{route('categories.jsonTable')}}",

                },
                data: {

                    "_token": "{{ csrf_token() }}"
                },

                columns: [
                    {data: 'c_id'},
                    {data: 'c_name'},

                    {
                        width: '20%',
                        sortable: false,
                        searchable: false,

                        render: function (data, type, row) {

                            return '<a href="/admin/categories/' + row['c_id'] + '/edit" class="editArticle btn btn-sm btn-primary d-block mt-1">Edycja</a>' +
                                '<button style= "width: 100%" name="deleteButton" id="deleteButton" type="button" data-id = "' + row['c_id'] + ' "class="deleteCategory btn btn-sm btn-danger d-block mt-1" >Usuń</button>';


                        },

                    },
                ],
                order: [

                    [0, 'desc'],
                    [1, 'desc'],

                ],
            });


            $('.ajaxTableCategories').on('click', '.deleteCategory', function () {

                var idx = $(this).attr('data-id');
                if (confirm("Na pewno chcesz usunąć ?")) {
                    $.ajax(
                        {
                            url: 'categories/' + idx,
                            type: 'DELETE',

                            data: {

                                "_token": "{{csrf_token()}}"
                            },

                            success: function () {
                                table.ajax.reload();
                                alert("Pomyślnie usunięto kategorie.")
                            },

                        });
                }

                return false;
            });

            $('#column0_search').on('keyup', function () {

                let inputValue = $('#column0_search').val();
                table.columns(0).search(inputValue).draw();

            });

            $('#column1_search').on('keyup', function () {

                let inputValue = $('#column1_search').val();
                table.columns(1).search(inputValue).draw();

            });


            $('.caret').remove();

        });

    </script>

@endpush

