@extends('layouts.app')
@section('content')

    <div class="container">
        <link href="{{ URL::asset('/css/admin_panel_buttons_styles.css') }}" rel="stylesheet" type="text/css" >
        <form method="POST" action="{{route('categories.store')}}">
            {{ csrf_field() }}
            {{ method_field('POST') }}
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-body">
                                <div class="form-group">
                                    <div id="errorNameField"></div>
                                    <label for="name">Podaj kategorie artykułu</label>
                                    <input id="name" type="text" class="form-control" name="name">
                                </div>
                                <button class=" addCategoryButton btn btn-primary float-right" type="submit" style="background-color: mediumseagreen;">Zapisz</button>
                                <a href="{{route('categories.index')}}" class=" backCategoryLinkButton btn btn-secondary" style="background-color: #4aa0e6;">Wstecz</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection

@push('js')

    <script>
        $(document).ready(function () {

            $('.addCategoryButton').click(function (e) {

                e.preventDefault();
                var categoryTitle = $('#name').val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax(
                    {

                        url: "{{route('categories.store')}}",
                        type: 'POST',
                        data: {

                            "_token": "{{csrf_token()}}",
                            'name': categoryTitle,
                        },

                        success: function () {

                            window.location.replace('/admin/categories');

                        },
                        error: function (response) {

                            const jsonStr = JSON.stringify(response);
                            const obj = JSON.parse(jsonStr);
                            const errorMessages = obj.responseJSON;

                            ("name" in errorMessages) ? $("#errorNameField").html("<div class='alert alert-danger'>" + errorMessages["name"] + "</div>").show()
                                : $("#errorNameField").hide();

                        }

                    });

            });

        });

    </script>

@endpush







