(function ($) {
    $.fn.nameBadge = function (options) {
        var settings = $.extend({
            border: {
                color: '#ddd',
                width: 3
            },
            colors: ['#C0C0C0', '#808080', 	'#000000', '#FF0000', '#800000','#FFFF00','#808000','#00FF00','#008000','#00FFFF','#008080','#0000FF','#000080','#FF00FF','#800080'],
            text: '#fff',
            position: 'relative',
            width: 75,
            left: 12,
            top: 0.2,
            lineHeight: 4.5,
            height: 5,
            fontSize: 1.5,
            borderRadius:50,
            middlename:true,
            uppercase: false
        }, options);
        return this.each(function () {
            var elementText = $(this).text();
            var initialLetters = elementText.match(settings.middlename ? /\b(\w)/g : /^\w|\b\w(?=\S+$)/g);
            var initials = initialLetters.join('');
            $(this).text(initials);
            $(this).css({
                'position': settings.position,
                'color': settings.text,
                'background-color': settings.colors[Math.floor(Math.random() * settings.colors.length)],
                'border': settings.border.width + 'px solid ' + settings.border.color,
                'font-family': 'Poppins',
                'font-size': settings.fontSize  + 'rem',
                'border-radius': settings.borderRadius + '%',
                'width': settings.width + '%',
                'height': settings.height + 'vh',
                'left': settings.left + '%',
                'top': settings.top + 'vh',
                'line-height': settings.lineHeight + 'vh',
                'text-align': 'center',
                'text-transform' : settings.uppercase ? 'uppercase' : ''
            });
        });
    };
}(jQuery));
